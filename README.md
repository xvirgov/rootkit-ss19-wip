# RootKit SS19 wip

## Requied packages
* Necessary packages can be found [here](https://wiki.debian.org/KVM)

## Installing Debian vm using Qemu
**Note:** Complete tutorial how to install Debian using cmd line can be found [here](https://linuxwebdevelopment.com/run-debian-qemu-kvm-virtual-machine-using-ubuntu-debian/).
1. Create image file: `qemu-img create -f qcow2 virtualdebian.img 20G`
2. Install debian: `kvm -hda qemudebian.img -cdrom debian-9.9.0-amd64-netinst.iso -m 4096 -net nic -net user,hostfwd=tcp::2222-:22 -enable-kvm -cpu host -soundhw all`
3. Create folder for sharing on host: `mkdir /home/xvirgov/iso/debian/pom`
4. Run Debian VM with [9p filesharing](http://www.linux-kvm.org/page/9p_virtio): `kvm -hda qemudebian.img -m 4096 -name n15 -fsdev local,security_model=passthrough,id=fsdev0,path=/home/xvirgov/iso/debian/pom -device virtio-9p-pci,id=fs0,fsdev=fsdev0,mount_tag=hshare -net nic -net user,hostfwd=tcp::2222-:22 -enable-kvm -cpu host -soundhw all`
5. Create folder for sharing on guest: `mkdir /hostshare`
6. Run this in guest: `mount -t 9p -o trans=virtio,version=9p2000.L hshare /hostshare`
**Note:** There is a bug in this sharing - guest cannot create new files but can only view/modify already created - host can create/modify/view changes of guest. I wasn't able to find a reason why this happens and loss of this functionality doesn't bother me..


## Connect with SSH to Guest
`ssh -p 2222 localhost` 

## Issues
* Reasons why compliation from multiple sources did not work and how to fix it is [here](https://paguilar.org/?p=7)