#!/usr/bin/env python3

import os
import gdb
import re
from elftools.elf import elffile

# My tutorial on elftools https://xvirgov.github.io/elftools-ipython3

def get_kernel_addr():
    print("Get kernel addr")
    vmlinux = "vmlinux-4.9.0-9-amd64"
    f = open(vmlinux,"rb")
    e = elffile.ELFFile(f)
    s =  e.get_section_by_name(".text")
    print(hex(s.header['sh_addr']))
    return hex(s.header['sh_addr'])


def get_rodata_addr():
    print("Get rodata addr")
    vmlinux = "vmlinux-4.9.0-9-amd64"
    f = open(vmlinux,"rb")
    e = elffile.ELFFile(f)
    s =  e.get_section_by_name(".rodata")
    print(hex(s.header['sh_addr']))
    return hex(s.header['sh_addr'])

def main():
    kernel_addr = str(get_kernel_addr())
    print("Kernel start before KASLR: " + kernel_addr)

    mem = gdb.execute('monitor info mem', to_string=True)
    for line in mem.splitlines():
        # get start address
        line = line.split("-")
        start_addr = "0x" + line[0]

        if ("0000ffff" in start_addr):
            # set the first four zeros to f
            start_addr = hex((int(start_addr, 16) + int("0xffff000000000000", 16)))
            print("Kernel start after KASLR: " + start_addr)
            kaslr_offset = hex((int(start_addr, 16) - int(kernel_addr, 16)))
            print("KASLR offset: " + kaslr_offset)
            rodata_position = get_rodata_addr()
            rodata_offset = hex((int(rodata_position, 16) + int(kaslr_offset, 16)))
            gdb.execute('add-symbol-file vmlinux-4.9.0-9-amd64 %s -s .rodata %s' %(start_addr, rodata_offset))
            gdb.execute('x/s linux_banner')
            break

main()