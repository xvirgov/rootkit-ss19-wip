#ifndef _HOOKING_UTILS_H
#define _HOOKING_UTILS_H

#include <linux/ftrace.h>
#include <linux/kallsyms.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/types.h>
#include <linux/unistd.h>
#include <linux/ctype.h>

void enable_page_protection(void);
void disable_page_protection(void);

#endif  //_HOOKING_UTILS_H
