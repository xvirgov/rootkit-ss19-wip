#ifndef _LOCK_UTILS_H
#define _LOCK_UTILS_H

#include <linux/ftrace.h>
#include <linux/kallsyms.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/types.h>
#include <linux/unistd.h>
#include <linux/ctype.h>

/* increment counter of a critical section */
void inc_critical(struct mutex *lock, int *counter);
/* decrement counter of a critical section */
void dec_critical(struct mutex *lock, int *counter);

#endif  //_LOCK_UTILS_H