#ifndef _UTILS_H
#define _UTILS_H

#include <linux/ftrace.h>
#include <linux/kallsyms.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/types.h>
#include <linux/unistd.h>
#include <linux/ctype.h>

#define NIPQUAD(addr) \
    ((unsigned char *)&addr)[0], \
    ((unsigned char *)&addr)[1], \
    ((unsigned char *)&addr)[2], \
    ((unsigned char *)&addr)[3]

#define NIP6(addr) \
    ntohs(addr[0]) >> 8, \
    ntohs(addr[1]) >> 8, \
    ntohs(addr[2]) >> 8, \
    ntohs(addr[3]) >> 8, \
    ntohs(addr[4]) >> 8, \
    ntohs(addr[5]) >> 8, \
    ntohs(addr[6]) >> 8, \
    ntohs(addr[7]) >> 8, \
    ntohs(addr[8]) >> 8, \
    ntohs(addr[9]) >> 8, \
    ntohs(addr[10]) >> 8, \
    ntohs(addr[11]) >> 8, \
    ntohs(addr[12]) >> 8, \
    ntohs(addr[13]) >> 8, \
    ntohs(addr[14]) >> 8, \
    ntohs(addr[15]) >> 8
    
int isIP(char *str);
int isPort(char *str);

int strtoint(char *str);

#endif  //_UTILS_H