#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x5d1699c6, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x5ecd35c8, __VMLINUX_SYMBOL_STR(d_path) },
	{ 0x99fccef1, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x79e8f5f0, __VMLINUX_SYMBOL_STR(sockfd_lookup) },
	{ 0x349cba85, __VMLINUX_SYMBOL_STR(strchr) },
	{ 0x754d539c, __VMLINUX_SYMBOL_STR(strlen) },
	{ 0x1b17e06c, __VMLINUX_SYMBOL_STR(kstrtoll) },
	{ 0x20c1a8ba, __VMLINUX_SYMBOL_STR(prepare_kernel_cred) },
	{ 0x6501515f, __VMLINUX_SYMBOL_STR(get_current_tty) },
	{ 0x1b6314fd, __VMLINUX_SYMBOL_STR(in_aton) },
	{ 0x94c828c7, __VMLINUX_SYMBOL_STR(commit_creds) },
	{ 0xe43eb7d8, __VMLINUX_SYMBOL_STR(remove_proc_entry) },
	{ 0x8efb1ee3, __VMLINUX_SYMBOL_STR(__dynamic_pr_debug) },
	{ 0x73820c41, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xf097ea92, __VMLINUX_SYMBOL_STR(nf_register_hook) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x343a1a8, __VMLINUX_SYMBOL_STR(__list_add) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0xece784c2, __VMLINUX_SYMBOL_STR(rb_first) },
	{ 0xc671e369, __VMLINUX_SYMBOL_STR(_copy_to_user) },
	{ 0x904ff872, __VMLINUX_SYMBOL_STR(PDE_DATA) },
	{ 0xa407d170, __VMLINUX_SYMBOL_STR(kern_path) },
	{ 0x11089ac7, __VMLINUX_SYMBOL_STR(_ctype) },
	{ 0x132f79b6, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0x49693a44, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x449ad0a7, __VMLINUX_SYMBOL_STR(memcmp) },
	{ 0x9166fada, __VMLINUX_SYMBOL_STR(strncpy) },
	{ 0x33c90865, __VMLINUX_SYMBOL_STR(netpoll_send_udp) },
	{ 0x5792f848, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0xdf04c6a9, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x3f3dd36, __VMLINUX_SYMBOL_STR(_raw_write_lock) },
	{ 0x521445b, __VMLINUX_SYMBOL_STR(list_del) },
	{ 0x1e6d26a8, __VMLINUX_SYMBOL_STR(strstr) },
	{ 0x1d8d7bc6, __VMLINUX_SYMBOL_STR(unregister_ftrace_function) },
	{ 0x5fdaa583, __VMLINUX_SYMBOL_STR(netpoll_print_options) },
	{ 0xa9b2229e, __VMLINUX_SYMBOL_STR(pid_task) },
	{ 0xe007de41, __VMLINUX_SYMBOL_STR(kallsyms_lookup_name) },
	{ 0xf1b5c4f5, __VMLINUX_SYMBOL_STR(init_net) },
	{ 0x3ee6be67, __VMLINUX_SYMBOL_STR(netpoll_setup) },
	{ 0xfac64b89, __VMLINUX_SYMBOL_STR(ftrace_set_filter_ip) },
	{ 0xb7ffcdca, __VMLINUX_SYMBOL_STR(init_task) },
	{ 0xdb7305a1, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x2ea2c95c, __VMLINUX_SYMBOL_STR(__x86_indirect_thunk_rax) },
	{ 0xbdfb6dbb, __VMLINUX_SYMBOL_STR(__fentry__) },
	{ 0x84d6e258, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xc6b68515, __VMLINUX_SYMBOL_STR(proc_create_data) },
	{ 0xc87e1eb2, __VMLINUX_SYMBOL_STR(nf_unregister_hook) },
	{ 0x6ed66d0c, __VMLINUX_SYMBOL_STR(find_get_pid) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x69acdf38, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0x1b8ae392, __VMLINUX_SYMBOL_STR(register_ftrace_function) },
	{ 0xca9360b5, __VMLINUX_SYMBOL_STR(rb_next) },
	{ 0xb0e602eb, __VMLINUX_SYMBOL_STR(memmove) },
	{ 0x77bc13a0, __VMLINUX_SYMBOL_STR(strim) },
	{ 0xdf2c2742, __VMLINUX_SYMBOL_STR(rb_last) },
	{ 0xb5419b40, __VMLINUX_SYMBOL_STR(_copy_from_user) },
	{ 0x88db9f48, __VMLINUX_SYMBOL_STR(__check_object_size) },
	{ 0x387336d5, __VMLINUX_SYMBOL_STR(filp_open) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "17ADDD8F7003F4D6203FD5D");
