#!/bin/bash
#
# config
image="/home/xvirgov/iso/debian/qemudebian.img"
shared_path="/home/xvirgov/iso/debian/pom"
mount_tag="rk_mount"

echo "Boot virtual machine for rootkit programming..."
echo "-----------------------------------------------"
echo "image: $image"
echo "shared path: $shared_path"
echo "mount tag: $mount_tag"
echo "-----------------------------------------------"

qemu-system-x86_64 $image $1 $2 -m 512 -enable-kvm -cpu host -net user,hostfwd=tcp::2222-:22 -net nic -vga std -virtfs local,id=test_dev,path=$shared_path,security_model=none,mount_tag=$mount_tag