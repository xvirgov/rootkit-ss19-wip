#!/bin/sh
#  see https://wiki.osdev.org/QEMU_and_GDB_in_long_mode
echo "Use Ctrl+C to interrupt gdb and get into its shell; 'continue' to
resume.\n\n"
gdb \
 -ex "add-auto-load-safe-path $(pwd)" \
 -ex 'set arch i386:x86-64:intel' \
 -ex 'target remote localhost:1234' \
 -ex 'continue' \
 -ex 'disconnect' \
 -ex 'set arch i386:x86-64' \
 -ex 'target remote localhost:1234'
