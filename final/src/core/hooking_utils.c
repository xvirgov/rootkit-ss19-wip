#include "hooking_utils.h"

void disable_page_protection(void) {
  unsigned long value;
  asm volatile("mov %%cr0, %0" : "=r"(value));

  if (!(value & 0x00010000)) return;

  asm volatile("mov %0, %%cr0" : : "r"(value & ~0x00010000));
}

void enable_page_protection(void) {
  unsigned long value;
  asm volatile("mov %%cr0, %0" : "=r"(value));

  if ((value & 0x00010000)) return;

  asm volatile("mov %0, %%cr0" : : "r"(value | 0x00010000));
}