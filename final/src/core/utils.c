#include "utils.h"

int strtoint(char *str) {
  long res;

  /* using kstrtol with base 10 (decimal) */
  if (kstrtol(str, 10, &res) == 0) return (int)res;

  return -1;
}

int isIP(char *str) {
  int i;
  for (i = 0; i < strlen(str); i++)
    if ((!isdigit(str[i]) && (str[i] < 'a' && str[i] > 'f')) && (str[i] != '.') && (str[i] != ':')) return 0;
  return 1;
}

int isPort(char *str) {
  int i;
  for (i = 0; i < strlen(str); i++)
    if (!isdigit(str[i])) return 0;
  return 1;
}

