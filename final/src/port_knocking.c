#include "port_knocking.h"

#define KNOCKING_LEN 3

static int knock_ports[KNOCKING_LEN] = {
    1111, 2222, 3333};  // Knock these ports before accepting
static int pos = 0;

/* counter for accesses */
static int nf_hook_access = 0;
/* mutex for counting */
struct mutex lock_nf_hook_access;

/* List of hidden ports */
struct list_head pk_ports;
int pk_ports_init = 0;
rwlock_t pk_list_lock;

/* global netfilter hook */
struct nf_hook_ops netf_hook;

int is_position(int port) {
  if (knock_ports[pos] == port) {
    pos++;

    if (pos == KNOCKING_LEN) return 1;

    return 0;
  }

  return 0;
}

int is_pk_port_hidden(int port) {
  struct list_head *ptr;
  struct pk_hidden_ports *entry;

  if (list_empty(&pk_ports)) return 0;

  list_for_each(ptr, &pk_ports) {
    entry = list_entry(ptr, struct pk_hidden_ports, list);
    if (entry->port == port) return 1;
  }

  return 0;
}

int pk_port_hide(char *port) {
  int port_i = strtoint(port);
  struct list_head *ptr;
  struct pk_hidden_ports *entry;
  struct pk_hidden_ports *temp_node = NULL;

  pr_info("udp_socket_hide: %s", port);

  // Check if port is already hidden
  list_for_each(ptr, &pk_ports) {
    entry = list_entry(ptr, struct pk_hidden_ports, list);
    if (entry->port == port_i) {
      pr_info("Port %d is already hidden!", port_i);
      return -1;
    }
  }

  // Creating node
  temp_node = kmalloc(sizeof(struct pk_hidden_ports), GFP_KERNEL);
  temp_node->port = port_i;

  // Adding node
  write_lock(&pk_list_lock);
  list_add(&temp_node->list, &pk_ports);
  write_unlock(&pk_list_lock);

  // Print all ports after add
  pr_info("---- Current list of TCP sockets to hide: ");
  list_for_each(ptr, &pk_ports) {
    entry = list_entry(ptr, struct pk_hidden_ports, list);
    pr_info("PORT::%d", entry->port);
  }
  pr_info("------------------------------------------");

  return 0;
}

int pk_port_unhide(char *port) {
  int port_i = strtoint(port);
  struct list_head *ptr;
  struct pk_hidden_ports *entry;

  pr_info("udp_socket_unhide: %s", port);

  write_lock(&pk_list_lock);
  // Remove the port from udp list
  list_for_each(ptr, &pk_ports) {
    entry = list_entry(ptr, struct pk_hidden_ports, list);
    if (entry->port == port_i) {
      list_del(ptr);
      kfree(entry);
      write_unlock(&pk_list_lock);
      pr_info("Port %d was found and deleted.", port_i);
      return 0;
    }
  }
  write_unlock(&pk_list_lock);

  pr_info("Port %d is not in list of udp sockets.", port_i);

  return -1;
}

unsigned int knock_port(void *priv, struct sk_buff *skb,
                        const struct nf_hook_state *state) {
  inc_critical(&lock_nf_hook_access, &nf_hook_access);
  // pr_info("--- knock_port --- begin");
  struct iphdr *header_ipv4;
  struct ipv6hdr *header_ipv6;
  struct tcphdr *header_tcp;
  struct udphdr *header_udp;

  header_ipv4 = ip_hdr(skb);
  header_ipv6 = ipv6_hdr(skb);

  if (skb->protocol == htons(ETH_P_IP)) {
    if (header_ipv4->protocol != IPPROTO_TCP) {
      pr_info("IPv4 UDP packet, accept");
      dec_critical(&lock_nf_hook_access, &nf_hook_access);
      return NF_ACCEPT;
    }
  }

  if (skb->protocol == htons(ETH_P_IPV6)) {
    if (header_ipv6->nexthdr != IPPROTO_TCP) {
      pr_info("IPv6 udp packet, accept");
      dec_critical(&lock_nf_hook_access, &nf_hook_access);
      return NF_ACCEPT;
    }
  }

  header_tcp = tcp_hdr(skb);

  if (!is_position(ntohs(header_tcp->dest)) &&
      !is_pk_port_hidden(ntohs(header_tcp->dest)))
    return NF_ACCEPT;

  if (skb->protocol == htons(ETH_P_IP)) {
    pr_info("IPv4 packet --- ");
    if (header_ipv4->protocol == IPPROTO_TCP) {
      pr_info("--- TCP protocol. -- ports:: source[%d] dest[%d]",
              ntohs(header_tcp->source), ntohs(header_tcp->dest));
      if (is_pk_port_hidden(ntohs(header_tcp->source)) ||
          is_pk_port_hidden(ntohs(header_tcp->dest))) {
        pr_info("FILTERED PORT");
        dec_critical(&lock_nf_hook_access, &nf_hook_access);

        if (is_position(header_tcp->dest)) return NF_ACCEPT;

        if (skb->protocol == htons(ETH_P_IP)) {
          pr_info("IPV4 PACKET REJECTED, SEND REJECT MESSAGE");
          // nf_send_reset(state->net, skb, state->hook); // Unknown symbol?
        } else if (skb->protocol == htons(ETH_P_IPV6)) {
          pr_info("IPV6 PACKET REJECTED, SEND REJECT MESSAGE");
          // nf_send_reset6(state->net, skb, state->hook);
        }

        return NF_DROP;
      }
    }
    // else if (header_ipv4->protocol == IPPROTO_UDP) {
    // 	header_udp = udp_hdr(skb);
    // 	pr_info("--- UDP protocol. -- ports:: source[%d] dest[%d]",
    // ntohs(header_udp->source),  ntohs(header_udp->dest));
    // }
  } /*else if (skb->protocol == htons(ETH_P_IPV6)) {
        pr_info("IPv6 packet --- ");
    if (header_ipv6->protocol == IPPROTO_TCP) {
        header_tcp = tcp_hdr(skb);
        pr_info("--- TCP protocol. -- ports:: source[%d] dest[%d]",
  header_tcp->source,  header_tcp->dest);
    }
    else if (header_ipv6->protocol == IPPROTO_UDP) {
        header_udp = udp_hdr(skb);
        pr_info("--- UDP protocol. -- ports:: source[%d] dest[%d]",
  header_udp->source,  header_udp->dest);
    }
  }*/

  // if(is_empty_data_node(&ports))
  // 	return NF_ACCEPT;

  // debug("KNOCK KNOCK");

  // /* get tcp header */
  // header_tcp = tcp_hdr(skb);

  //  fix for host -> guest scp file transfer when module loaded
  // if(!is_knock_port(ntohs(header_tcp->dest))
  // 	&& !find_port(ntohs(header_tcp->dest)))
  // 	return NF_ACCEPT;

  // if(sender_check(skb, ntohs(header_tcp->dest))) {
  // 	if(skb->protocol == htons(ETH_P_IP)) {
  // 		debug("IPV4 PACKET REJECTED, SEND REJECT MESSAGE");
  // 		nf_send_reset(state->net, skb, state->hook);
  // 	}else if(skb->protocol == htons(ETH_P_IPV6)) {
  // 		debug("IPV6 PACKET REJECTED, SEND REJECT MESSAGE");
  // 		nf_send_reset6(state->net, skb, state->hook);
  // 	}

  // 	debug("UNKNOWN PROTOCOL, DROP");

  // 	return NF_DROP;
  // }

  // debug("PACKET ACCEPTED");
  dec_critical(&lock_nf_hook_access, &nf_hook_access);
  return NF_ACCEPT;
}

int init_port_knocking(void) {
  pr_info("port_knocking_init");
  if (pk_ports_init) {
    pr_info("Port knocking is already initialized.");
    return -1;
  }

  int ret;

  // debug("INITIALIZING PORT KNOCKING");

  /* set flags and function for netfilter */
  netf_hook.hook = knock_port;
  netf_hook.hooknum = NF_INET_LOCAL_IN;
  netf_hook.pf = PF_INET;
  netf_hook.priority = NF_IP_PRI_FIRST;

  /* register our netfilter hook */
  ret = nf_register_hook(&netf_hook);

  if (ret < 0) return 1;

  INIT_LIST_HEAD(&pk_ports);
  mutex_init(&lock_nf_hook_access);

  pk_ports_init = 1;
  return 0;
}

void exit_port_knocking(void) {
  pr_info("port_knocking_exit");

  if (pk_ports_init) {
    struct pk_hidden_ports *entry, *next;
    write_lock(&pk_list_lock);
    if (!list_empty(&pk_ports)) {
      list_for_each_entry_safe(entry, next, &pk_ports, list) {
        pr_info("Deleting port :: %d", entry->port);
        list_del(&entry->list);
        kfree(entry);
      }
    }
    write_unlock(&pk_list_lock);
    pk_ports_init = 0;
  }

  /* clear lists */
  // free_data_node_list(&senders);
  // free_data_node_list(&ports);
  nf_unregister_hook(&netf_hook);
}