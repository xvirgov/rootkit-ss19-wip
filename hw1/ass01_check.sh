#!/bin/bash

SHELLS="bash sh"

# This script should be run as unpriviledged user
id | grep root &> /dev/null
if [[ $? == 0 ]]; then
	echo "Do not run me as root!"
	exit -1;
fi

# Test priviledge escalation on some common shells
for s in $SHELLS; do
	echo -n "Checking $s..."
	echo "make_me_root; id" | $s 2> /dev/null | grep root &> /dev/null
	if [[ $? == 0 ]]; then
		echo "OK"
	else
		echo "FAIL"
	fi

	id | grep root &> /dev/null
	if [[ $? == 0 ]]; then
		echo "This process shouldn't be root!"
		exit -1;
	fi
done

# Create some load on stdin and stdout to test if the kernel is still functional
echo -n "Performing load test..."
cat /dev/zero | cat > /dev/null &
sleep 5
kill $!
echo "OK"

# Bonus: Can we priviledge escalate in this scenario?
# echo "Bonus: Priviledge escalation under load..."
# cat /dev/zero | cat > /dev/null &
# PID=$!
# sleep 1

# for s in $SHELLS; do
# 	echo -n "Checking $s..."
# 	echo "make_me_root; id" | $s 2> /dev/null | grep root &> /dev/null
# 	if [[ $? == 0 ]]; then
# 		echo "OK"
# 	else
# 		echo "FAIL"
# 	fi

# 	id | grep root &> /dev/null
# 	if [[ $? == 0 ]]; then
# 		echo "This process shouldn't be root!"
# 		exit -1;
# 	fi
# done
# kill $PID
