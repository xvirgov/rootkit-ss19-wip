#!/usr/bin/env python3

# Example rootkit control programm, created for the purporse
# of the Rootkit Programming Lab at TUM
# Copyright 2019 by Fabian Franzen (franzen@sec.in.tum.de)

import fcntl
import ctypes
import sys
import traceback

if len(sys.argv) != 2:
    print("Usage: ./r00tkit_cli.py <your_ioctl_file>")
    exit(0)

IOCTL_PING = 0

pong_buf = ctypes.create_string_buffer(b"PING")

f = open(sys.argv[1], "r")
fcntl.ioctl(f, IOCTL_PING, pong_buf, True)

if pong_buf.value == b"PONG":
    print("Got pong!")
else:
    print("No Pong!!!")
