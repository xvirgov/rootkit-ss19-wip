/**
 * @file    print_ps.c
 * @author  Michal Virgovic
 * @date    9. February 2019
 * @version 1.0
 * @brief  A loadable kernel module (LKM) that can display list of processes (PIDs, IDs)
 * in the /var/log/kern.log file when the module is loaded and removed.
*/

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/namei.h>
#include <linux/sched.h>
#include <linux/security.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Michal Virgovic");
MODULE_DESCRIPTION("This module prints on module load a list of processes with their respecive properties: PID(root NS), PID(own NS), Comm, ID of PID NS, ID of user NS, ID of network NS");
MODULE_VERSION("1.0");

#define PATH_BUFF_SIZE 256

int procs_info_print(void)
{
	int r = 0;
        struct task_struct* task;
	size_t process_counter = 0;
        for_each_process(task) {
		// Structures used for getting PID in own ns - not completely necessary
		struct pid* pid_task;		
		struct pid_namespace* pid_ns;

		// Buffers used to store paths - /proc/<PID>/ns/<pid|usr|net>
		char pid_ns_path[PATH_BUFF_SIZE];
		char user_ns_path[PATH_BUFF_SIZE];
		char net_ns_path[PATH_BUFF_SIZE];

		// Structs where could be found inode numbers of files
		struct path path_PID, path_user, path_net;

		// Diffrent types of PIDs
		pid_t pid_root_ns, pid_own_ns, pid_parent;

		// Remeber number of processes
                ++process_counter;

		// Get PID of process in root ns
		// You can also access PID directrly from task_struct but there is also this special function
		pid_root_ns = task_pid_nr(task);

		// First way how to get pid in own namespace
		pid_task = task_pid(task);		
		pid_ns = ns_of_pid(pid_task);
		pid_own_ns = task_pid_nr_ns(task, pid_ns);
			
		// Second and easier way
		// pid_own_ns = task_pid_vnr(task);

		// Get PID of parent process
		pid_parent = task_ppid_nr(task);

		// Get inode numbers from files
		sprintf(pid_ns_path, "/proc/%d/ns/pid", (int) pid_root_ns);
		r = kern_path(pid_ns_path, LOOKUP_FOLLOW, &path_PID);
	   	if (r) {
	    		pr_err("Could not get path for %s\n", pid_ns_path);
    			return r;
    		}

		sprintf(user_ns_path, "/proc/%d/ns/user", (int) pid_root_ns);
		r = kern_path(user_ns_path, LOOKUP_FOLLOW, &path_user);
		if(r) {
			pr_err("Could not get path for %s\n", user_ns_path);
			return r;
		}

		sprintf(net_ns_path, "/proc/%d/ns/net", (int) pid_root_ns);
		r = kern_path(net_ns_path, LOOKUP_FOLLOW, &path_net);
		if(r) {			
	    		pr_err("Could not get path for %s\n", net_ns_path);
			return r;
		}

		// Print info
		pr_info("Process \"%s\" - PIDs: root-ns[%d], own-ns[%d], parent[%d]\n", task->comm, pid_root_ns, pid_own_ns, pid_parent);
        	pr_info("------- IDs: PID-namespace [%ld], User-namespace [%ld], Net-namespace [%ld]\n",
				path_PID.dentry->d_inode->i_ino, path_user.dentry->d_inode->i_ino, path_net.dentry->d_inode->i_ino);
	}
        pr_info("== Number of processes: %zu]\n", process_counter);
	return 0;
}

static int __init print_init(void){

	pr_info("[ INIT ==\n");

	return procs_info_print();
}

static void __exit print_exit(void){
   pr_info("Module \"print_ps\" removed.\n");
}

module_init(print_init);
module_exit(print_exit);
