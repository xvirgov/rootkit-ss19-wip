#!/usr/bin/env bash

[ $# -eq 0 ] &&  echo "Specify port!!" && exit -1

socat tcp-listen:$1,fork,reuseaddr exec:/bin/bash,pty,stderr,setsid