#!/usr/bin/env python3

import os
import gdb
import re # regular expression module



def get_kernel_position_without_kaslr():
    cmd = "readelf -S vmlinux-4.9.0-9-amd64"
    output = os.popen(cmd).read()
    for line in output.splitlines():
        if ("[ 1]" in line):
            # get kernel address
            #print(line.split()[4])
            return line.split()[4]

def get_rodata_position():
    cmd = "readelf -S vmlinux-4.9.0-9-amd64"
    output = os.popen(cmd).read()
    for line in output.splitlines():
        if ("[ 7]" in line):
            # get kernel address
            #print(line.split()[4])
            return line.split()[4]

def main():
    kernel_addr = "0x" + get_kernel_position_without_kaslr()
    print("Kernel start before KASLR: " + kernel_addr)

    mem = gdb.execute('monitor info mem', to_string=True)
    for line in mem.splitlines():
        # get start address
        line = line.split("-")
        start_addr = "0x" + line[0]

        if ("0000ffff" in start_addr):
            # set the first four zeros to f
            start_addr = hex((int(start_addr, 16) + int("0xffff000000000000", 16)))
            print("Kernel start after KASLR: " + start_addr)
            kaslr_offset = hex((int(start_addr, 16) - int(kernel_addr, 16)))
            print("KASLR offset: " + kaslr_offset)
            rodata_position = get_rodata_position()
            rodata_offset = hex((int(rodata_position, 16) + int(kaslr_offset, 16)))
            gdb.execute('add-symbol-file vmlinux-4.9.0-9-amd64 %s -s .rodata %s' %(start_addr, rodata_offset))
            gdb.execute('x/s linux_banner')
            break


main()