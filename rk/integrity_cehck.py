#!/usr/bin/env python3

import struct
from elftools.elf import elffile
import sys

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("usage: ./foo <file>")

    f = open(sys.argv[1], "rb")
    e = elffile.ELFFile(f)
    s = e.get_section_by_name(".symtab")
    sym = s.get_symbol_by_name("sys_call_table")[0]
    ls = e.get_section(sym.entry['st_shndx'])
    offset = sym.entry['st_value'] - ls.header['sh_addr']
    base = ls.header['sh_offset']
    f.seek(base + offset)
    first_entry = f.read(0x8)

    print("the first entry of sys_call_table is at {:#x} "
          "and points to {:#x}".format(sym.entry['st_value'],
                                       struct.unpack("<Q", first_entry)[0]))