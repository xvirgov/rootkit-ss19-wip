#!/usr/bin/env python3

# ffffffff9e400000 (replace leading 0000 with ffff) - kernel after kaslr
# .text == ffffffff81000000
# .rodata == ffffffff81800000
# offset = 0x1d400000
# new_rodata = 0xffffffff9ec00000

# (gdb) p/x 0xffffffff9e400000 - 0xffffffff81000000
# $1 = 0x1d400000
# (gdb) p/x 0xffffffff81800000 + 0x1d400000
# $2 = 0xffffffff9ec00000
# (gdb) add-symbol-file /home/xvirgov/iso/debian/pom/vmlinux-4.9.0-9-amd64 0xffffffff9e400000 -s .rodata 0xffffffff9ec00000
# add symbol table from file "/home/xvirgov/iso/debian/pom/vmlinux-4.9.0-9-amd64" at
# 	.text_addr = 0xffffffff9e400000
# 	.rodata_addr = 0xffffffff9ec00000
# (y or n) y
# Reading symbols from /home/xvirgov/iso/debian/pom/vmlinux-4.9.0-9-amd64...done.
# (gdb) p printk
# $3 = {int (const char *, ...)} 0xffffffff9e58162c <printk>
# (gdb) 


import os
import gdb
import re # regular expression module
from binascii import hexlify
import struct
from elftools.elf import elffile

def get_linux_banner(vm_file):
	f = open(vm_file,"rb")
	e = elffile.ELFFile(f)
	ro = e.get_section_by_name('.rodata')
	f.seek(ro.header['sh_offset'] + 0xe0)
	return f.read(20).decode("utf-8") 

def vmlinux_pos(vm_file):
	f = open(vm_file,"rb")
	e = elffile.ELFFile(f)
	s = e.get_section_by_name(".symtab")
	banner = s.get_symbol_by_name("linux_banner")
	banner = banner[0]
	return banner.entry['st_value']

def search_page(from_to, needle):
	from_addr = from_to.split('-')[0] 

	# check the allignment and skip if not alligned to 0x200000 ~ 2MB
	if int(from_addr, 16) % 2097152 != 0:
		return -1

	to_addr = from_to.split('-')[1]
	mem_len = int(to_addr, 16) - int(from_addr, 16)

	# extract the memory specified by offset and length

	# print('monitor x/{}xb 0x{}'.format(mem_len, from_addr))
	try:
		hex_memory_o = gdb.execute('x/{}xb 0x{}'.format(mem_len, from_addr), to_string=True, from_tty=False) # used wo monitor
	except:
		return -1
		pass

	matches = re.findall(r'0x\w+', hex_memory_o) # ignore first element (might be address)
	bytes_matches = []
	for match in matches:
		bytes_matches.append(match.split('x')[1])
	hex_memory = ''.join(bytes_matches)

	# print(hex_memory)

	# format needle from ascii string to hex string
	hex_needle = hexlify(str.encode(needle))
	# print('p')
	# search for hex needle in hex memory
	# print('Looking for -- {} :: {}'.format(needle, hex_needle.decode()))
	pos = hex_memory.find(hex_needle.decode())
	
	if pos > 0:
		print('monitor x/{}xb 0x{}'.format(mem_len, from_addr))
		# print(hex_memory)
		# print(pos)
		# print(bytes.fromhex(hex_memory).decode('utf-8'))
		print('Looking for -- {} :: {}'.format(needle, hex_needle.decode()))
		return pos+int(from_addr, 16)

	return -1

def main():
	vmlinux_file = '/home/xvirgov/iso/debian/pom/vmlinux-4.9.0-9-amd64'
	needle = get_linux_banner(vmlinux_file)
	print(needle)
	# registers = gdb.execute('monitor info registers', to_string=True)
	# match = re.search(r'CR3=\w+', registers)
	# if match:
	# 	cr3 = match.group(0).split("=")[1]
	# 	print(cr3)

	mappings = gdb.execute('monitor info mem', to_string=True).split()
	pages = mappings[::3]

	# search_page(pages[0], 'init_task')

	kaslr_pos = -1
	for page in pages:
		kaslr_pos = search_page(page, needle)
		if kaslr_pos != -1:
			break

	print('kaslr_pos : {} - {}'.format(kaslr_pos, hex(kaslr_pos)))

	# rel_pos = vmlinux_pos(vmlinux_file, needle)

	# print('rel_pos : {} - {}'.format(rel_pos, hex(rel_pos)))

	# print('kaslr_offset : {}'.format(hex(kaslr_pos - rel_pos)))

main()