#!/usr/bin/env python3

# Virtual machine introspection program to
# be executed from gdb in order to bypass
# KASLR in the kernel.

import os
import gdb
import re # regular expression module

#search_string = "Linux\x20version\x204.9.0-9-amd64\x20(debian-kernel@lists.debian.org)\x20(gcc\x20version\x206.3.0\x2020170516\x20(Debian\x206.3.0-18+deb9u1)\x20)\x20#1\x20SMP\x20Debian\x204.9.168-1\x20(2019-04-12)"
search_string = "4.9.0-9-amd64"
#search_string = "Linux"
#search_string = "swapper"

def get_kernel_position_without_kaslr():
    cmd = "readelf -S /home/xvirgov/iso/debian/pom/vmlinux-4.9.0-9-amd64"
    output = os.popen(cmd).read()
    i = 0
    for line in output.splitlines():
        if ("[ 1]" in line):
            # get kernel address
            #print(line.split()[4])
            return line.split()[4]

def main():
    kernel_addr = "0x" + get_kernel_position_without_kaslr()
    print("Kernel start before KASLR: " + kernel_addr)
    registers = gdb.execute('monitor info registers', to_string=True)
    match = re.search(r'CR3=\w+', registers)
    if match:
        cr3 = match.group(0).split("=")[1]
        #print(cr3)

    mem = gdb.execute('monitor info mem', to_string=True)
    for line in mem.splitlines():
        # get start address
        line = line.split("-")
        start_addr = "0x" + line[0]

        if ("0000ffff" in start_addr):
            # set the first four zeros to f
            start_addr = hex((int(start_addr, 16) + int("0xffff000000000000", 16)))
            print("Kernel start after KASLR: " + start_addr)
            kaslr_offset = hex((int(start_addr, 16) - int(kernel_addr, 16)))
            print("KASLR offset: " + kaslr_offset)
            rodata_offset = hex((int("0xffffffff81800000", 16) + int(kaslr_offset, 16)))
            gdb.execute('add-symbol-file /home/xvirgov/iso/debian/pom/vmlinux-4.9.0-9-amd64 %s -s .rodata %s' %(start_addr, rodata_offset))
            gdb.execute('x/s linux_banner')
            break

main()