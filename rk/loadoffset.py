#!/usr/bin/env python3

from elftools.elf import elffile
import gdb

def load_at_offset(name, elf, offs):
    addr = None
    adds = ""
    for s in elf.iter_sections():
        if s.name == '.text':
            addr = hex(s.header['sh_addr'] + offs)
        elif s.header['sh_addr'] != 0:
            adds += " -s {:s} {:#x}".format(s.name, s.header['sh_addr'] + offs)
    if addr == None:
        print("WARNING: elf file does not contain a .text section")
    gdb.execute("add-symbol-file {:#x}".format(adds),  from_tty = False)

class OffsetLoad(gdb.Command):
    "Load the kernel"
    def __init__(self):
        super(OffsetLoad, self).__init__("oload", gdb.COMMAND_FILES, gdb.COMPLETE_FILENAME, False)

    def invoke(self, argument, from_tty):
        # args = gdb.string_to_argv(argument)
        # if len(args) != 2:
        #     print("usage: oload <file> <addr>")
        #     return
        with open("/home/xvirgov/iso/debian/pom/vmlinux-4.9.0-9-amd64", "rb") as f:
            elf = elffile.ELFFile(f)
            offs = int("0x0000ffffa9a00000", 0)
            load_at_offset("/home/xvirgov/iso/debian/pom/vmlinux-4.9.0-9-amd64", elf, offs)

OffsetLoad()