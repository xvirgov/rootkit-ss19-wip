# Report on Syscall Hooking by overwriting MSR register

## My steps (before I knew about PTI)

1) Studying how MSR works/how Syscalls are served in Linux
2) Reading/Writing values from/to MSR -- commented code in read_hook.c
    * Reading worked with no problem
    * Writing worked only when I wrote to MSR same value as it originally contained from this I assumed that MSR is writable (or I'm able to write to it), the issue should be with the value written - I figured that there are some wrong permissions
    * I found an article about exploiting MSR register [here](https://ruinedsec.wordpress.com/2013/04/04/modifying-system-calls-dispatching-linux/), but after I implemented the code, kernel still panicked.
    * I didn't know how to proceed from this point and I still thought that it might be an permissions issue, so I tried mapping my syscall_sipatcher function semewhere else in memory like they did [here](https://is.muni.cz/el/1433/jaro2011/PV204/um/LinuxRootkits/sys_call_table_complete.htm). I didn't really expected this to work and so kernel panick was still present.
    * I tried diffrent ways how to write to MSR register (assembly command, wrmsr, and also tried [this code](https://github.com/intel/msr-tools/blob/master/wrmsr.c))

## My steps (after I found out about PTI) -- code in syscall_msr_hook.c
1) Reading about PTI/paging in Linux, how syscalls work
2) "kaiser_add_mapping()" approach
    * My first try was to copy code from kernel (so I wouldn't use kallsyms_lookup_name) and try to make it work (didn't work because of some null pointer reference)
    * On second try, I used kallsyms_lookup_name, then I used the syscall dispatcher function from [this article](https://ruinedsec.wordpress.com/2013/04/04/modifying-system-calls-dispatching-linux/) but I was still getting the same kernelpanic
    * I turned off pti (I checked that it was turned off in dmesg) and tried my implementation again but I still got kernel panic
    * I figured that the implementation of the dispatcher was not correct so I tried to copy the assembly code of the original entry_SYSCALL_64 
    * Still did not worked...
3) "pti_clone_user_shared()" approach
    * I read about how paging in linux kernel works, the four-level page tables, etc.
    * I was browsing pti.c file in kernel and I was trying to figure out how my rootkit could disable pti
    * In the description of the function pti_clone_user_shared is written:
```
/*
 * On 32 bit PAE systems with 1GB of Kernel address space there is only
 * one pgd/p4d for the whole kernel. Cloning that would map the whole
 * address space into the user page-tables, making PTI useless. So clone
 * the page-table on the PMD level to prevent that.
 */
```
4)
    * I tried to modify the code of that function so I would be able to clone on pgd/p4d level..
    * I wasn't able to make this work either...