#!/usr/bin/env python3

import struct
from elftools.elf import elffile
import sys


SYSCALL_NUM = 100

def check_syscalls(vmlinux_file):
	# if len(sys.argv) != 3:
	# 	print("usage: ./foo <file> <kaslr_offset>")

	f = open(vmlinux_file, "rb")
	e = elffile.ELFFile(f)
	s = e.get_section_by_name(".symtab")
	sym = s.get_symbol_by_name("sys_call_table")[0]

	ls = e.get_section(sym.entry['st_shndx'])
	offset = sym.entry['st_value'] - ls.header['sh_addr']
	base = ls.header['sh_offset']

	expected_addresses = []
	numBytesPerEntry = struct.calcsize("<Q")
	print("Addresses in vmlinux")
	for ii in range(SYSCALL_NUM):
		e.stream.seek(base + offset + ii * numBytesPerEntry)
		data = e.stream.read(numBytesPerEntry)
		# print("Value :: {:#x}".format(str(struct.unpack("<Q", data)[0])))
		expected_addresses.append(hex(struct.unpack("<Q", data)[0]))

	# first one: 0xffffffff8120ce10
	# kaslr offset: 0x28a00000

	real_addresses = []
	syscalls_names = []
	print("Adresses in kernel")
	for i in range(SYSCALL_NUM):
		sym_gdb = gdb.lookup_global_symbol("sys_call_table")
		gdb_entry_data = sym_gdb.value()[i]
		# gdb_entry_data = gdb_entry_data
		real_addresses.append(str(gdb_entry_data).split()[0])
		syscalls_names.append(str(gdb_entry_data).split()[1])
		# print("Value :: {}".format(str(gdb_entry_data).split()[0]))

	for i in range(SYSCALL_NUM):
		kaslr_offset = '0x28a00000'
		# kaslr_offset = sys.argv[2]
		print("Checking syscall {}".format(syscalls_names[i]))
		expected_addresse_kaslr = hex(int(expected_addresses[i], 16) + int(kaslr_offset, 16))
		if expected_addresse_kaslr != real_addresses[i]:
			print("Expected: {}, Real: {}".format(expected_addresse_kaslr, real_addresses[i]))
			continue
		print("[OK]")

	# first one: 0xffffffffa9c0ce10

def check_functions(vmlinux_file):
	f = elffile.ELFFile(open(vmlinux_file, "rb"))
	s = f.get_section_by_name(".symtab")
	for i in s.iter_symbols():
		if i.entry["st_info"]["type"] == "STT_FUNC":
			try:
				gdb_val = gdb.parse_and_eval(i.name)
				print(gdb_val)
			except:
				pass
			# print(i.name, i.entry["st_info"]["type"])

if __name__ == "__main__":
	vmlinux_file = "/home/xvirgov/iso/debian/pom/vmlinux-4.9.0-9-amd64"
	# check_syscalls(vmlinux_file)
	check_functions(vmlinux_file)