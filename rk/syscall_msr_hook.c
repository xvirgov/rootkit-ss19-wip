#include "syscall_msr_hook.h"

struct mm_struct init_mm;

enum pti_clone_level {
	PTI_CLONE_PMD,
	PTI_CLONE_PTE,
};

#define CPU_ENTRY_AREA_BASE						\
	((FIXADDR_TOT_START - PAGE_SIZE * (CPU_ENTRY_AREA_PAGES + 1))   \
	 & PMD_MASK)

#define CPU_ENTRY_AREA_PAGES	(NR_CPUS * 40)

	#define FIXADDR_SIZE		(__end_of_permanent_fixed_addresses << PAGE_SHIFT)
#define FIXADDR_START		(FIXADDR_TOP - FIXADDR_SIZE)
#define FIXADDR_TOT_SIZE	(__end_of_fixed_addresses << PAGE_SHIFT)
#define FIXADDR_TOT_START	(FIXADDR_TOP - FIXADDR_TOT_SIZE)

// 	int kaiser_enabled __read_mostly = 1;
// EXPORT_SYMBOL(kaiser_enabled);	/* for inlined TLB flush functions */

__visible
DEFINE_PER_CPU_USER_MAPPED(unsigned long, unsafe_stack_register_backup);

/*
 * These can have bit 63 set, so we can not just use a plain "or"
 * instruction to get their value or'd into CR3.  It would take
 * another register.  So, we use a memory reference to these instead.
 *
 * This is also handy because systems that do not support PCIDs
 * just end up or'ing a 0 into their CR3, which does no harm.
 */
DEFINE_PER_CPU(unsigned long, x86_cr3_pcid_user);

/*
 * At runtime, the only things we map are some things for CPU
 * hotplug, and stacks for new processes.  No two CPUs will ever
 * be populating the same addresses, so we only need to ensure
 * that we protect between two CPUs trying to allocate and
 * populate the same page table page.
 *
 * Only take this lock when doing a set_p[4um]d(), but it is not
 * needed for doing a set_pte().  We assume that only the *owner*
 * of a given allocation will be doing this for _their_
 * allocation.
 *
 * This ensures that once a system has been running for a while
 * and there have been stacks all over and these page tables
 * are fully populated, there will be no further acquisitions of
 * this lock.
 */
static DEFINE_SPINLOCK(shadow_table_allocation_lock);


static inline unsigned long get_pa_from_mapping(unsigned long vaddr)
{
	pgd_t *pgd;
	pud_t *pud;
	pmd_t *pmd;
	pte_t *pte;

	pgd = pgd_offset_k(vaddr);
	/*
	 * We made all the kernel PGDs present in kaiser_init().
	 * We expect them to stay that way.
	 */
	BUG_ON(pgd_none(*pgd));
	/*
	 * PGDs are either 512GB or 128TB on all x86_64
	 * configurations.  We don't handle these.
	 */
	BUG_ON(pgd_large(*pgd));

	pud = pud_offset(pgd, vaddr);
	if (pud_none(*pud)) {
		WARN_ON_ONCE(1);
		return -1;
	}

	if (pud_large(*pud))
		return (pud_pfn(*pud) << PAGE_SHIFT) | (vaddr & ~PUD_PAGE_MASK);

	pmd = pmd_offset(pud, vaddr);
	if (pmd_none(*pmd)) {
		WARN_ON_ONCE(1);
		return -1;
	}

	if (pmd_large(*pmd))
		return (pmd_pfn(*pmd) << PAGE_SHIFT) | (vaddr & ~PMD_PAGE_MASK);

	pte = pte_offset_kernel(pmd, vaddr);
	if (pte_none(*pte)) {
		WARN_ON_ONCE(1);
		return -1;
	}

	return (pte_pfn(*pte) << PAGE_SHIFT) | (vaddr & ~PAGE_MASK);
}

/*
 * This is a relatively normal page table walk, except that it
 * also tries to allocate page tables pages along the way.
 *
 * Returns a pointer to a PTE on success, or NULL on failure.
 */
static pte_t *kaiser_pagetable_walk(unsigned long address, bool user)
{
	pmd_t *pmd;
	pud_t *pud;
	pgd_t *pgd = native_get_shadow_pgd(pgd_offset_k(address));
	gfp_t gfp = (GFP_KERNEL | __GFP_NOTRACK | __GFP_ZERO);
	unsigned long prot = _KERNPG_TABLE;

	if (pgd_none(*pgd)) {
		WARN_ONCE(1, "All shadow pgds should have been populated");
		return NULL;
	}
	BUILD_BUG_ON(pgd_large(*pgd) != 0);

	if (user) {
		/*
		 * The vsyscall page is the only page that will have
		 *  _PAGE_USER set. Catch everything else.
		 */
		BUG_ON(address != VSYSCALL_ADDR);

		set_pgd(pgd, __pgd(pgd_val(*pgd) | _PAGE_USER));
		prot = _PAGE_TABLE;
	}

	pud = pud_offset(pgd, address);
	/* The shadow page tables do not use large mappings: */
	if (pud_large(*pud)) {
		WARN_ON(1);
		return NULL;
	}
	if (pud_none(*pud)) {
		unsigned long new_pmd_page = __get_free_page(gfp);
		if (!new_pmd_page)
			return NULL;
		spin_lock(&shadow_table_allocation_lock);
		if (pud_none(*pud)) {
			set_pud(pud, __pud(prot | __pa(new_pmd_page)));
			__inc_zone_page_state(virt_to_page((void *)
						new_pmd_page), NR_KAISERTABLE);
		} else
			free_page(new_pmd_page);
		spin_unlock(&shadow_table_allocation_lock);
	}

	pmd = pmd_offset(pud, address);
	/* The shadow page tables do not use large mappings: */
	if (pmd_large(*pmd)) {
		WARN_ON(1);
		return NULL;
	}
	if (pmd_none(*pmd)) {
		unsigned long new_pte_page = __get_free_page(gfp);
		if (!new_pte_page)
			return NULL;
		spin_lock(&shadow_table_allocation_lock);
		if (pmd_none(*pmd)) {
			set_pmd(pmd, __pmd(prot | __pa(new_pte_page)));
			__inc_zone_page_state(virt_to_page((void *)
						new_pte_page), NR_KAISERTABLE);
		} else
			free_page(new_pte_page);
		spin_unlock(&shadow_table_allocation_lock);
	}

	return pte_offset_kernel(pmd, address);
}

static int kaiser_add_user_map(const void *__start_addr, unsigned long size,
			       unsigned long flags)
{
	int ret = 0;
	pte_t *pte;
	unsigned long start_addr = (unsigned long )__start_addr;
	unsigned long address = start_addr & PAGE_MASK;
	unsigned long end_addr = PAGE_ALIGN(start_addr + size);
	unsigned long target_address;

	/*
	 * It is convenient for callers to pass in __PAGE_KERNEL etc,
	 * and there is no actual harm from setting _PAGE_GLOBAL, so
	 * long as CR4.PGE is not set.  But it is nonetheless troubling
	 * to see Kaiser itself setting _PAGE_GLOBAL (now that "nokaiser"
	 * requires that not to be #defined to 0): so mask it off here.
	 */
	flags &= ~_PAGE_GLOBAL;
	if (!(__supported_pte_mask & _PAGE_NX))
		flags &= ~_PAGE_NX;

	for (; address < end_addr; address += PAGE_SIZE) {
		target_address = get_pa_from_mapping(address);
		if (target_address == -1) {
			ret = -EIO;
			break;
		}
		pte = kaiser_pagetable_walk(address, flags & _PAGE_USER);
		if (!pte) {
			ret = -ENOMEM;
			break;
		}
		if (pte_none(*pte)) {
			set_pte(pte, __pte(flags | target_address));
		} else {
			pte_t tmp;
			set_pte(&tmp, __pte(flags | target_address));
			WARN_ON_ONCE(!pte_same(*pte, tmp));
		}
	}
	return ret;
}

/* Add a mapping to the shadow mapping, and synchronize the mappings */
int kaiser_add_mapping(unsigned long addr, unsigned long size, unsigned long flags)
{
	if (!kaiser_enabled)
		return 0;
	return kaiser_add_user_map((const void *)addr, size, flags);
}

// void kaiser_remove_mapping(unsigned long start, unsigned long size)
// {
// 	extern void unmap_pud_range_nofree(pgd_t *pgd,
// 				unsigned long start, unsigned long end);
// 	unsigned long end = start + size;
// 	unsigned long addr, next;
// 	pgd_t *pgd;

// 	if (!kaiser_enabled)
// 		return;
// 	pgd = native_get_shadow_pgd(pgd_offset_k(start));
// 	for (addr = start; addr < end; pgd++, addr = next) {
// 		next = pgd_addr_end(addr, end);
// 		unmap_pud_range_nofree(pgd, addr, next);
// 	}
// }


// void fake_syscall_dispatcher(void){
//     /* steps:
//      *  1- reverse the function prolouge
//      *  2- store the GP-registers/FLAGS
//      *  3- do [Nice] things
//      *  4- restore GP-registers/FLAGS
//      *  5- call system call
//      */
//     __asm__ __volatile__ (
//         "mov %rbp,%rsp\n"
//         "pop %rbp\n");
 
//     __asm__ __volatile__ (
//         "push %rsp\n"
//         "push %rax\n"
//         "push %rbp\n"
//         "push %rdi\n"
//         "push %rsi\n"
//         "push %rdx\n"
//         "push %rcx\n"
//         "push %rbx\n"
//         "push %r8\n"
//         "push %r9\n"
//         "push %r10\n"
//         "push %r11\n"
//         "push %r12\n"
//         "push %r15\n"
//         );
//         // Hook Goes here.
//     __asm__ __volatile__(
 
//         "\tpop %%r15\n"
//         "\tpop %%r12\n"
//         "\tpop %%r11\n"
//         "\tpop %%r10\n"
//         "\tpop %%r9\n"
//         "\tpop %%r8\n"
//         "\tpop %%rbx\n"
//         "\tpop %%rcx\n"
//         "\tpop %%rdx\n"
//         "\tpop %%rsi\n"
//         "\tpop %%rdi\n"
//         "\tpop %%rbp\n"
//         "\tpop %%rax\n"
//         "\tpop %%rsp\n"
//         "\tjmp *%0\n"
//         :: "m"(syscall_handler));
 
// }


static inline void *ptr_set_bit(void *ptr, int bit)
{
	unsigned long __ptr = (unsigned long)ptr;

	__ptr |= BIT(bit);
	return (void *)__ptr;
}
#define PTI_PGTABLE_SWITCH_BIT	PAGE_SHIFT
static inline pgd_t *kernel_to_user_pgdp(pgd_t *pgdp)
{
	return ptr_set_bit(pgdp, PTI_PGTABLE_SWITCH_BIT);
}

static p4d_t *pti_user_pagetable_walk_p4d(unsigned long address)
{
	pgd_t *pgd = kernel_to_user_pgdp(pgd_offset_k(address));
	gfp_t gfp = (GFP_KERNEL | __GFP_NOTRACK | __GFP_ZERO);

	if (address < PAGE_OFFSET) {
		WARN_ONCE(1, "attempt to walk user address\n");
		return NULL;
	}

	if (pgd_none(*pgd)) {
		unsigned long new_p4d_page = __get_free_page(gfp);
		if (WARN_ON_ONCE(!new_p4d_page))
			return NULL;

		set_pgd(pgd, __pgd(_KERNPG_TABLE | __pa(new_p4d_page)));
	}
	// BUILD_BUG_ON(pgd_large(*pgd) != 0);

	return p4d_offset(pgd, address);
}

static pmd_t *pti_user_pagetable_walk_pmd(unsigned long address)
{
	gfp_t gfp = (GFP_KERNEL | __GFP_NOTRACK | __GFP_ZERO);
	p4d_t *p4d;
	pud_t *pud;

	p4d = pti_user_pagetable_walk_p4d(address);
	if (!p4d)
		return NULL;

	// BUILD_BUG_ON(p4d_large(*p4d) != 0);
	if (p4d_none(*p4d)) {
		unsigned long new_pud_page = __get_free_page(gfp);
		if (WARN_ON_ONCE(!new_pud_page))
			return NULL;

		set_p4d(p4d, __p4d(_KERNPG_TABLE | __pa(new_pud_page)));
	}

	pud = pud_offset(p4d, address);
	/* The user page tables do not use large mappings: */
	if (pud_large(*pud)) {
		WARN_ON(1);
		return NULL;
	}
	if (pud_none(*pud)) {
		unsigned long new_pmd_page = __get_free_page(gfp);
		if (WARN_ON_ONCE(!new_pmd_page))
			return NULL;

		set_pud(pud, __pud(_KERNPG_TABLE | __pa(new_pmd_page)));
	}

	return pmd_offset(pud, address);
}

static pte_t *pti_user_pagetable_walk_pte(unsigned long address)
{
	gfp_t gfp = (GFP_KERNEL | __GFP_NOTRACK | __GFP_ZERO);
	pmd_t *pmd;
	pte_t *pte;

	pmd = pti_user_pagetable_walk_pmd(address);
	if (!pmd)
		return NULL;

	/* We can't do anything sensible if we hit a large mapping. */
	if (pmd_large(*pmd)) {
		WARN_ON(1);
		return NULL;
	}

	if (pmd_none(*pmd)) {
		unsigned long new_pte_page = __get_free_page(gfp);
		if (!new_pte_page)
			return NULL;

		set_pmd(pmd, __pmd(_KERNPG_TABLE | __pa(new_pte_page)));
	}

	pte = pte_offset_kernel(pmd, address);
	if (pte_flags(*pte) & _PAGE_USER) {
		WARN_ONCE(1, "attempt to walk to user pte\n");
		return NULL;
	}
	return pte;
}

static void
pti_clone_pgtable(unsigned long start, unsigned long end,
		  enum pti_clone_level	 level)
{
	unsigned long addr;

	/*
	 * Clone the populated PMDs which cover start to end. These PMD areas
	 * can have holes.
	 */
	for (addr = start; addr < end;) {
		pte_t *pte, *target_pte;
		pmd_t *pmd, *target_pmd;
		pgd_t *pgd;
		p4d_t *p4d;
		pud_t *pud;

		/* Overflow check */
		if (addr < start)
			break;

		pgd = pgd_offset_k(addr);
		if (WARN_ON(pgd_none(*pgd)))
			return;
		p4d = p4d_offset(pgd, addr);
		if (WARN_ON(p4d_none(*p4d)))
			return;

		pud = pud_offset(p4d, addr);
		if (pud_none(*pud)) {
			addr += PUD_SIZE;
			continue;
		}

		pmd = pmd_offset(pud, addr);
		if (pmd_none(*pmd)) {
			addr += PMD_SIZE;
			continue;
		}

		// if (pmd_large(*pmd) || level == PTI_CLONE_PMD) {
		// 	target_pmd = pti_user_pagetable_walk_pmd(addr);
		// 	if (WARN_ON(!target_pmd))
		// 		return;

		// 	/*
		// 	 * Only clone present PMDs.  This ensures only setting
		// 	 * _PAGE_GLOBAL on present PMDs.  This should only be
		// 	 * called on well-known addresses anyway, so a non-
		// 	 * present PMD would be a surprise.
		// 	 */
		// 	if (WARN_ON(!(pmd_flags(*pmd) & _PAGE_PRESENT)))
		// 		return;

			
		// 	 * Setting 'target_pmd' below creates a mapping in both
		// 	 * the user and kernel page tables.  It is effectively
		// 	 * global, so set it as global in both copies.  Note:
		// 	 * the X86_FEATURE_PGE check is not _required_ because
		// 	 * the CPU ignores _PAGE_GLOBAL when PGE is not
		// 	 * supported.  The check keeps consistentency with
		// 	 * code that only set this bit when supported.
			 
		// 	if (boot_cpu_has(X86_FEATURE_PGE))
		// 		*pmd = pmd_set_flags(*pmd, _PAGE_GLOBAL);

		// 	/*
		// 	 * Copy the PMD.  That is, the kernelmode and usermode
		// 	 * tables will share the last-level page tables of this
		// 	 * address range
		// 	 */
		// 	*target_pmd = *pmd;

		// 	addr += PMD_SIZE;

		// } else if (level == PTI_CLONE_PTE) {

		// 	/* Walk the page-table down to the pte level */
		// 	pte = pte_offset_kernel(pmd, addr);
		// 	if (pte_none(*pte)) {
		// 		addr += PAGE_SIZE;
		// 		continue;
		// 	}

		// 	/* Only clone present PTEs */
		// 	if (WARN_ON(!(pte_flags(*pte) & _PAGE_PRESENT)))
		// 		return;

		// 	/* Allocate PTE in the user page-table */
		// 	target_pte = pti_user_pagetable_walk_pte(addr);
		// 	if (WARN_ON(!target_pte))
		// 		return;

		// 	/* Set GLOBAL bit in both PTEs */
		// 	if (boot_cpu_has(X86_FEATURE_PGE))
		// 		*pte = pte_set_flags(*pte, _PAGE_GLOBAL);

		// 	/* Clone the PTE */
		// 	*target_pte = *pte;

			addr += PAGE_SIZE;

		// } else {
		// 	BUG();
		// }
		// pr_info("AA");
	}
}

static void pti_clone_user_shared(void)
{
	unsigned long start, end;

	start = CPU_ENTRY_AREA_BASE;
	end   = start + (PAGE_SIZE * CPU_ENTRY_AREA_PAGES);

	pti_clone_pgtable(start, end, PTI_CLONE_PMD);
}

static struct file_operations chdir_ops;
asmlinkage long (*real_chdir)(const char __user *filename);
void (*syscall_handler)(void);
long unsigned int orig_reg;

void fake_syscall_dispatcher(void) {
  /* steps:
   *  1- reverse the function prolouge
   *  2- store the GP-registers/FLAGS
   *  3- do [Nice] things
   *  4- restore GP-registers/FLAGS
   *  5- call system call
   */
  // __asm__ __volatile__(
  //     "mov %rbp,%rsp\n"
  //     "pop %rbp\n");

  // __asm__ __volatile__(
  //     "push %rsp\n"
  //     "push %rax\n"
  //     "push %rbp\n"
  //     "push %rdi\n"
  //     "push %rsi\n"
  //     "push %rdx\n"
  //     "push %rcx\n"
  //     "push %rbx\n"
  //     "push %r8\n"
  //     "push %r9\n"
  //     "push %r10\n"
  //     "push %r11\n"
  //     "push %r12\n"
  //     "push %r15\n");
  // // Hook Goes here.
  // __asm__ __volatile__(

  //     "\tpop %%r15\n"
  //     "\tpop %%r12\n"
  //     "\tpop %%r11\n"
  //     "\tpop %%r10\n"
  //     "\tpop %%r9\n"
  //     "\tpop %%r8\n"
  //     "\tpop %%rbx\n"
  //     "\tpop %%rcx\n"
  //     "\tpop %%rdx\n"
  //     "\tpop %%rsi\n"
  //     "\tpop %%rdi\n"
  //     "\tpop %%rbp\n"
  //     "\tpop %%rax\n"
  //     "\tpop %%rsp\n"
  //     "\tjmp *%0\n" ::"m"(syscall_handler));
  
}

unsigned long get_msr(void) {
  pr_info("get_msr");

  uint32_t low, high;
  asm volatile("rdmsr" : "=a"(low), "=d"(high) : "c"(MSR_LSTAR));
  uint64_t _rdmsr = ((uint64_t)high << 32) | low;

  return _rdmsr;
  // pr_info("rdmsr:: %lx", _rdmsr);
}
void fake_entry_SYSCALL_64(void);

asmlinkage long (*ref_kaiser_add_mapping)(unsigned long addr, unsigned long size, unsigned long flags);

int init_msr_hook(void)
{
// 	wrmsr(MSR_STAR, 0, (__USER32_CS << 16) | __KERNEL_CS);
// 	wrmsrl(MSR_LSTAR, (unsigned long)entry_SYSCALL_64);

// #ifdef CONFIG_IA32_EMULATION
// 	wrmsrl(MSR_CSTAR, (unsigned long)entry_SYSCALL_compat);
// 	/*
// 	 * This only works on Intel CPUs.
// 	 * On AMD CPUs these MSRs are 32-bit, CPU truncates MSR_IA32_SYSENTER_EIP.
// 	 * This does not cause SYSENTER to jump to the wrong location, because
// 	 * AMD doesn't allow SYSENTER in long mode (either 32- or 64-bit).
// 	 */
// 	wrmsrl_safe(MSR_IA32_SYSENTER_CS, (u64)__KERNEL_CS);
// 	wrmsrl_safe(MSR_IA32_SYSENTER_ESP, 0ULL);
// 	wrmsrl_safe(MSR_IA32_SYSENTER_EIP, (u64)entry_SYSENTER_compat);
// #else
// 	wrmsrl(MSR_CSTAR, (unsigned long)ignore_sysret);
// 	wrmsrl_safe(MSR_IA32_SYSENTER_CS, (u64)GDT_ENTRY_INVALID_SEG);
// 	wrmsrl_safe(MSR_IA32_SYSENTER_ESP, 0ULL);
// 	wrmsrl_safe(MSR_IA32_SYSENTER_EIP, 0ULL);
// #endif

// 	/* Flags to clear on syscall */
// 	wrmsrl(MSR_SYSCALL_MASK,
// 	       X86_EFLAGS_TF|X86_EFLAGS_DF|X86_EFLAGS_IF|
// 	       X86_EFLAGS_IOPL|X86_EFLAGS_AC|X86_EFLAGS_NT);
	// pr_info("THIS_MODULE->init_layout.base :: %x", THIS_MODULE->init_layout.base);
	// pr_info("THIS_MODULE->init_layout.size :: %d", THIS_MODULE->init_layout.size);
	// pr_info("__PAGE_KERNEL :: %d", __PAGE_KERNEL);

	// // kaiser_add_mapping(THIS_MODULE->init_layout.base + THIS_MODULE->init_layout.size,
 // //             1,
 // //             __PAGE_KERNEL);

	// // kaiser_add_mapping(mem, 4096, __PAGE_KERNEL_RX);

	// kaiser_add_mapping(mem,1,__PAGE_KERNEL_RX);

	// asmlinkage long (*ref_sys_read)(int fd, void *buf, size_t count);

	// ref_sys_read = (void *)kallsyms_lookup_name("sys_read");

	// ref_sys_read(0, "a", 1);	

	// void *mem = kmalloc(4096, GFP_KERNEL);
	// ref_kaiser_add_mapping = (void *)kallsyms_lookup_name("kaiser_add_mapping");
	// pr_info("mem :: %lx", THIS_MODULE->init_layout.base);
	// (*ref_kaiser_add_mapping)( THIS_MODULE->core_layout.base,
 //             THIS_MODULE->core_layout.size,
 //             __PAGE_KERNEL_RX);	
		// (*ref_kaiser_add_mapping)( fake_syscall_dispatcher,
  //            4096,
  //            __PAGE_KERNEL_RX);	



	// pti_user_pagetable_walk_pmd(mem);
	// pti_clone_user_shared();

	unsigned int low = 0, high = 0, lo=0;
    long unsigned int address;
 
    rdmsr(0xC0000082,low,high);
    printk("Low:%x\tHigh:%x\n", low,high);
    address = 0;
    address |= high;
    address = address << 32;
    address |= low;
    orig_reg = address;
 
    printk("Syscall Handler: %lx\n", address);
    syscall_handler = (void (*)(void)) address;
 
    lo = (unsigned int) (((unsigned long)fake_syscall_dispatcher)
                 & 0xFFFFFFFF);
    printk("Lo: %x\tHi:%x\n", lo,high);

    // msleep(5000);
    asm volatile ("wrmsr" :: "c"(0xC0000082), "a"(lo),
                 "d"(high) : "memory");
    // msleep(5000);

    pr_info("THIS_MODULE->core_layout.base :: %lx", THIS_MODULE->core_layout.base);
    pr_info("THIS_MODULE->core_layout.size :: %ld", THIS_MODULE->core_layout.size);
    pr_info("fake_syscall_dispatcher :: %lx", fake_syscall_dispatcher);

    pr_info("entry_SYSCALL_64 :: %lx", kallsyms_lookup_name("entry_SYSCALL_64"));
    pr_info("LSTAR_MSR :: %lx", get_msr() );

    // asm volatile ("wrmsr" :: "c"(0xC0000082), "a"(kallsyms_lookup_name("entry_SYSCALL_64") & 0xFFFFFFFF),
    //              "d"(kallsyms_lookup_name("entry_SYSCALL_64") >> 32) : "memory");    

	return 0;
}
