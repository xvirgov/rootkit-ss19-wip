#ifndef _SYSCALL_MSR_HOOK_H
#define _SYSCALL_MSR_HOOK_H

#include <linux/linkage.h>
#include <asm/segment.h>
#include <asm/cache.h>
#include <asm/errno.h>
// #include "calling.h"
#include <asm/asm-offsets.h>
#include <asm/msr.h>
#include <asm/unistd.h>
#include <asm/thread_info.h>
#include <asm/hw_irq.h>
#include <asm/page_types.h>
#include <asm/irqflags.h>
#include <asm/paravirt.h>
#include <asm/percpu.h>
#include <asm/asm.h>
#include <asm/smap.h>
#include <asm/pgtable_types.h>
// #include <asm/export.h>
#include <linux/err.h>
#include <linux/elf-em.h>
#include <linux/bootmem.h>
#include <linux/linkage.h>
#include <linux/bitops.h>
#include <linux/kernel.h>
#include <linux/export.h>
#include <linux/percpu.h>
#include <linux/string.h>
#include <linux/ctype.h>
#include <linux/delay.h>
#include <linux/sched.h>
#include <linux/init.h>
#include <linux/kprobes.h>
#include <linux/kgdb.h>
#include <linux/smp.h>
#include <linux/io.h>
#include <linux/syscore_ops.h>

#include <asm/stackprotector.h>
#include <asm/perf_event.h>
#include <asm/mmu_context.h>
#include <asm/archrandom.h>
#include <asm/hypervisor.h>
#include <asm/processor.h>
#include <asm/tlbflush.h>
#include <asm/debugreg.h>
#include <asm/sections.h>
#include <asm/vsyscall.h>
#include <linux/topology.h>
#include <linux/cpumask.h>
#include <asm/pgtable.h>
#include <linux/atomic.h>
#include <asm/proto.h>
#include <asm/setup.h>
#include <asm/apic.h>
#include <asm/desc.h>
#include <asm/fpu/internal.h>
#include <asm/mtrr.h>
#include <linux/numa.h>
#include <asm/asm.h>
#include <asm/bugs.h>
#include <asm/cpu.h>
#include <asm/mce.h>
#include <asm/msr.h>
#include <asm/pat.h>
#include <asm/microcode.h>
#include <asm/microcode_intel.h>

#include <linux/bug.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/types.h>
#include <linux/bug.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/spinlock.h>
#include <linux/mm.h>
#include <linux/uaccess.h>
#include <linux/ftrace.h>
#include <xen/xen.h>

#include <asm/kaiser.h>
#include <asm/tlbflush.h>	/* to verify its kaiser declarations */
#include <asm/pgtable.h>
#include <asm/pgalloc.h>
#include <asm/desc.h>
#include <asm/vsyscall.h>
#include <asm/cmdline.h>
#include <linux/mm_types.h>

#include <linux/module.h>
#include <linux/kernel.h>

#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/types.h>
#include <linux/bug.h>
#include <linux/init.h>
#include <linux/spinlock.h>
#include <linux/mm.h>
#include <linux/uaccess.h>
#include <linux/cpu.h>

#include <asm/cpufeature.h>
#include <asm/hypervisor.h>
#include <asm/vsyscall.h>
#include <asm/cmdline.h>
// #include <asm/pti.h>
#include <asm/pgtable.h>
#include <asm/pgalloc.h>
#include <asm/tlbflush.h>
#include <asm/desc.h>
#include <asm/sections.h>



#define p4d_t				pgd_t

int init_msr_hook(void);

#endif //_SYSCALL_MSR_HOOK_H

#ifndef _5LEVEL_FIXUP_H
#define _5LEVEL_FIXUP_H

#define __ARCH_HAS_5LEVEL_HACK
#define __PAGETABLE_P4D_FOLDED 1

#define P4D_SHIFT			PGDIR_SHIFT
#define P4D_SIZE			PGDIR_SIZE
#define P4D_MASK			PGDIR_MASK
#define MAX_PTRS_PER_P4D		1
#define PTRS_PER_P4D			1

#define p4d_t				pgd_t

#define pud_alloc(mm, p4d, address) \
	((unlikely(pgd_none(*(p4d))) && __pud_alloc(mm, p4d, address)) ? \
		NULL : pud_offset(p4d, address))

#define p4d_alloc(mm, pgd, address)	(pgd)
#define p4d_offset(pgd, start)		(pgd)
#define p4d_none(p4d)			0
#define p4d_bad(p4d)			0
#define p4d_present(p4d)		1
#define p4d_ERROR(p4d)			do { } while (0)
#define p4d_clear(p4d)			pgd_clear(p4d)
#define p4d_val(p4d)			pgd_val(p4d)
#define p4d_populate(mm, p4d, pud)	pgd_populate(mm, p4d, pud)
#define p4d_populate_safe(mm, p4d, pud)	pgd_populate(mm, p4d, pud)
#define p4d_page(p4d)			pgd_page(p4d)
#define p4d_page_vaddr(p4d)		pgd_page_vaddr(p4d)

#define __p4d(x)			__pgd(x)
#define set_p4d(p4dp, p4d)		set_pgd(p4dp, p4d)

#undef p4d_free_tlb
#define p4d_free_tlb(tlb, x, addr)	do { } while (0)
#define p4d_free(mm, x)			do { } while (0)
#define __p4d_free_tlb(tlb, x, addr)	do { } while (0)

#undef  p4d_addr_end
#define p4d_addr_end(addr, end)		(end)

#endif