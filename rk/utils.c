#include "utils.h"

/* Utils - IP/ports */

int isIP(char *str) {
  int i;
  for (i = 0; i < strlen(str); i++)
    if ((!isdigit(str[i]) && (str[i] < 'a' && str[i] > 'f')) && (str[i] != '.') && (str[i] != ':')) return 0;
  return 1;
}

int isPort(char *str) {
  int i;
  for (i = 0; i < strlen(str); i++)
    if (!isdigit(str[i])) return 0;
  return 1;
}

/* critical section functions (mutexes) */

/* increment counter of a critical section */
void inc_critical(struct mutex *lock, int *counter) {
  /* lock access mutex */
  mutex_lock(lock);
  (*counter)++;

  /* unlock access mutex */
  mutex_unlock(lock);
}

/* decrement counter of a critical section */
void dec_critical(struct mutex *lock, int *counter) {
  /* lock access mutex */
  mutex_lock(lock);
  (*counter)--;

  /* unlock access mutex */
  mutex_unlock(lock);
}

void disable_page_protection(void) {
  unsigned long value;
  asm volatile("mov %%cr0, %0" : "=r"(value));

  if (!(value & 0x00010000)) return;

  asm volatile("mov %0, %%cr0" : : "r"(value & ~0x00010000));
}

void enable_page_protection(void) {
  unsigned long value;
  asm volatile("mov %%cr0, %0" : "=r"(value));

  if ((value & 0x00010000)) return;

  asm volatile("mov %0, %%cr0" : : "r"(value | 0x00010000));
}

/* strtoint: convert char *str containing a number into an integer */
int strtoint(char *str) {
  long res;

  /* using kstrtol with base 10 (decimal) */
  if (kstrtol(str, 10, &res) == 0) return (int)res;

  return -1;
}

/***
   * Hooking any linux function using ftrace
   * source - https://www.apriorit.com/dev-blog/546-hooking-linux-functions-2
   */
// static void notrace fh_ftrace_thunk(unsigned long ip, unsigned long parent_ip,
//                                     struct ftrace_ops *ops,
//                                     struct pt_regs *regs) {
//   struct ftrace_hook *hook = container_of(ops, struct ftrace_hook, ops);
//   /* Skip the function calls from the current module. */
//   if (!within_module(parent_ip, THIS_MODULE)) {
//     regs->ip = (unsigned long)hook->function;
//   }
// }

// static int resolve_hook_address(struct ftrace_hook *hook) {
//   hook->address = kallsyms_lookup_name(hook->name);

//   if (!hook->address) {
//     pr_info("unresolved symbol: %s\n", hook->name);
//     return -ENOENT;
//   }

//   *((unsigned long *)hook->original) = hook->address;

//   return 0;
// }

// int fh_install_hook(struct ftrace_hook *hook) {
//   int err;

//   err = resolve_hook_address(hook);
//   if (err) return err;

//   hook->ops.func = fh_ftrace_thunk;
//   hook->ops.flags = FTRACE_OPS_FL_SAVE_REGS | FTRACE_OPS_FL_IPMODIFY;

//   err = ftrace_set_filter_ip(&hook->ops, hook->address, 0, 0);
//   if (err) {
//     pr_info("ftrace_set_filter_ip() failed: %d\n", err);
//     return err;
//   }

//   err = register_ftrace_function(&hook->ops);
//   if (err) {
//     pr_info("register_ftrace_function() failed: %d\n", err);

//     /* Don’t forget to turn off ftrace in case of an error. */
//     ftrace_set_filter_ip(&hook->ops, hook->address, 1, 0);

//     return err;
//   }
//   pr_info("Set hook for %s", hook->name);
//   pr_info("fake hook for %x", hook->function);
//   pr_info("original hook for %x", hook->original);
//   return 0;
// }

// void fh_remove_hook(struct ftrace_hook *hook) {
//   int err;

//   err = unregister_ftrace_function(&hook->ops);
//   if (err) {
//     pr_info("unregister_ftrace_function() failed: %d\n", err);
//   }

//   err = ftrace_set_filter_ip(&hook->ops, hook->address, 1, 0);
//   if (err) {
//     pr_info("ftrace_set_filter_ip() failed: %d\n", err);
//   }
// }