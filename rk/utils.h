#ifndef _UTILS_H
#define _UTILS_H

#include <linux/ftrace.h>
#include <linux/kallsyms.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/types.h>
#include <linux/unistd.h>
#include <linux/ctype.h>

#define NIPQUAD(addr) \
    ((unsigned char *)&addr)[0], \
    ((unsigned char *)&addr)[1], \
    ((unsigned char *)&addr)[2], \
    ((unsigned char *)&addr)[3]

#define NIP6(addr) \
    ntohs(addr[0]) >> 8, \
    ntohs(addr[1]) >> 8, \
    ntohs(addr[2]) >> 8, \
    ntohs(addr[3]) >> 8, \
    ntohs(addr[4]) >> 8, \
    ntohs(addr[5]) >> 8, \
    ntohs(addr[6]) >> 8, \
    ntohs(addr[7]) >> 8, \
    ntohs(addr[8]) >> 8, \
    ntohs(addr[9]) >> 8, \
    ntohs(addr[10]) >> 8, \
    ntohs(addr[11]) >> 8, \
    ntohs(addr[12]) >> 8, \
    ntohs(addr[13]) >> 8, \
    ntohs(addr[14]) >> 8, \
    ntohs(addr[15]) >> 8
    
int isIP(char *str);
int isPort(char *str);

/* increment counter of a critical section */
void inc_critical(struct mutex *lock, int *counter);
/* decrement counter of a critical section */
void dec_critical(struct mutex *lock, int *counter);

void enable_page_protection(void);
void disable_page_protection(void);

int strtoint(char *str);

/* hooking using ftrace */
// struct ftrace_hook {
//   const char *name;
//   void *function;
//   void *original;

//   unsigned long address;
//   struct ftrace_ops ops;
// };

// #define HOOK(_name, _function, _original) \
//   { .name = (_name), .function = (_function), .original = (_original), }

// int fh_install_hook(struct ftrace_hook *hook);
// void fh_remove_hook(struct ftrace_hook *hook);

#endif  //_UTILS_H